<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTblDeliverymensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('tbl_deliverymens', function (Blueprint $table) {
            $table->increments('deliveryman_id');
            $table->string('delivery_man_name');
            $table->string('delivery_man_image');
            $table->string('delivery_man_phone');
            $table->string('deliveryman_status');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('tbl_deliverymens');
    }
}
