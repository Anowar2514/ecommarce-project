<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Session;
session_start();
class SliderController extends Controller
{
    public function index(){
        $this->AdminAuthCheck();
            return view('admin.slider.add_slider');
    }
    public function save_slider(Request $request)
    {
        $data =array();
        //$data['product_id'] = $request->product_id;
        $data['slider_name'] = $request->slider_name;
        $data['slider_price'] = $request->slider_price;
        $data['publication_status'] = $request->publication_status;
        $image = $request->file('slider_image');
        if($image){
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'slider/';
            $image_url = $upload_path.$image_full_name;
            $success = $image->move($upload_path,$image_full_name);
            if ($success){
                $data['slider_image'] = $image_url;
                DB::table('sliders')->insert($data);
                return Redirect::to('/add-slider');
            }
        }
        $data['slider_image'] ='';
        DB::table('sliders')->insert($data);
        return Redirect::to('/add-slider');


    }
    public function all_slider(){
        $this->AdminAuthCheck();
        $all_slider_info = DB::table('sliders')
            ->paginate(6);
        $manage_slider = view('admin.slider.all_slider')
            ->with('all_slider_info',$all_slider_info);
        return view('admin_layout')
            ->with('admin.slider.all_slider',$manage_slider);
    }
    public function inactive_slider($slider_id){
        //dd($product_id);
        DB::table('sliders')
            ->where('slider_id',$slider_id)
            ->update(['publication_status' => 0]);
        return Redirect::to('/all-slider');
    }
    public function active_slider($slider_id){
        DB::table('sliders')
            ->where('slider_id',$slider_id)
            ->update(['publication_status' =>1]);
        return Redirect::to('/all-slider');
    }
    public function edit_slider($slider_id){
        $this->AdminAuthCheck();
        $slider_info = DB::table('sliders')
            ->where('slider_id',$slider_id)
            ->first();

        $slider_info = view('admin.slider.edit_slider')
            ->with('slider_info',$slider_info);
        return view('admin_layout')
            ->with('admin.slider.edit_slider',$slider_info);
        //return view('admin.edit_category');
    }
    public function update_slider(Request $request,$slider_id){
        $data = array();
        $data['slider_name'] = $request->slider_name;
        $data['slider_price'] = $request->slider_price;
//        $data['publication_status'] = $request->publication_status;
        $image = $request->file('slider_image');
        if($image){
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'slider/';
            $image_url = $upload_path.$image_full_name;
            $success = $image->move($upload_path,$image_full_name);
            if ($success){
                $data['slider_image'] = $image_url;
                DB::table('sliders')
                    ->where('slider_id',$slider_id)
                    ->save($data);
                return Redirect::to('/all-slider');
            }
        }
        $data['slider_image'] ='';
        DB::table('sliders')
            ->where('slider_id',$slider_id)
            ->update($data);
        return Redirect::to('/all-slider');
    }
    public function delete_slider($slider_id){
        DB::table('sliders')
            ->where('slider_id',$slider_id)
            ->delete();
        return Redirect::to('/all-slider');
    }
    public function AdminAuthCheck(){
        if(Session::get('admin_id')) {
            $admin_id = Session::get('admin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/admin')->send();
            }
        }elseif (Session::get('useradmin_id')){
            $admin_id = Session::get('useradmin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/user-admin')->send();
            }
        }
    }
}
