<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Session;
session_start();
class ProductController extends Controller
{
    public function index(){
        $this->AdminAuthCheck();
        return view('admin.product.add_product');
    }
    public function save_product(Request $request)
    {
        $data =array();
        //$data['product_id'] = $request->product_id;
        $data['category_id'] = $request->category_id;
        $data['sub_category_id'] = $request->sub_category_id;
        $data['brand_id'] = $request->brand_id;
        $data['product_name'] = $request->product_name;
        $data['product_short_description'] = $request->product_short_description;
        $data['product_long_description'] = $request->product_long_description;
        $data['product_price'] = $request->product_price;
        //$data['product_image'] = $request->product_image;
        $data['product_size'] = $request->product_size;
        $data['product_color'] = $request->product_color;
        $data['product_quantity'] = $request->product_quantity;
        $data['publication_status'] = $request->publication_status;
        $image = $request->file('product_image');
        if($image){
           $image_name = str_random(20);
           $ext = strtolower($image->getClientOriginalExtension());
           $image_full_name = $image_name.'.'.$ext;
           $upload_path = 'image/';
           $image_url = $upload_path.$image_full_name;
           $success = $image->move($upload_path,$image_full_name);
           if ($success){
               $data['product_image'] = $image_url;
               DB::table('products')->insert($data);
               return Redirect::to('/add-product');
           }
        }
        $data['product_image'] ='';
        DB::table('products')->insert($data);
        return Redirect::to('/add-product');


    }
    public function all_product(){
        $this->AdminAuthCheck();
        $all_product_info = DB::table('products')
            ->join('categories','products.category_id','=','categories.category_id')
            ->join('sub_categories','products.sub_category_id','=','sub_categories.sub_category_id')
            ->join('brands','products.brand_id','=','brands.brand_id')
            ->select('products.*','categories.category_name','brands.brand_name','sub_categories.sub_category_name')
            ->paginate(6);
        $manage_product = view('admin.product.all_product')
            ->with('all_product_info',$all_product_info);
        return view('admin_layout')
            ->with('admin.product.all_product',$manage_product);
    }
    public function inactive_product($product_id){
        //dd($product_id);
        DB::table('products')
            ->where('product_id',$product_id)
            ->update(['publication_status' => 0]);
        return Redirect::to('/all-product');
    }
    public function active_product($product_id){
        DB::table('products')
            ->where('product_id',$product_id)
            ->update(['publication_status' =>1]);
        return Redirect::to('/all-product');
    }
    public function edit_product($product_id){
        $this->AdminAuthCheck();
        $product_info = DB::table('products')
            ->where('product_id',$product_id)
            ->first();

        $product_info = view('admin.product.edit_product')
            ->with('product_info',$product_info);
        return view('admin_layout')
            ->with('admin.product.edit_product',$product_info);
        //return view('admin.edit_category');
    }
    public function update_product(Request $request,$product_id){
        $data = array();
        $data['category_id'] = $request->category_id;
        $data['sub_category_id'] = $request->sub_category_id;
        $data['brand_id'] = $request->brand_id;
        //$data['product_name'] = $request->product_name;
        $data['product_short_description'] = $request->product_short_description;
        $data['product_long_description'] = $request->product_long_description;
        $data['product_price'] = $request->product_price;
        //$data['product_image'] = $request->product_image;
        $data['product_size'] = $request->product_size;
        $data['product_color'] = $request->product_color;
        $data['product_quantity'] = $request->product_quantity;
//        $image = $request->file('product_image');
//        if($image){
//            $image_name = str_random(20);
//            $ext = strtolower($image->getClientOriginalExtension());
//            $image_full_name = $image_name.'.'.$ext;
//            $upload_path = 'image/';
//            $image_url = $upload_path.$image_full_name;
//            $success = $image->move($upload_path,$image_full_name);
//            if ($success){
//                $data['product_image'] = $image_url;
//                DB::table('products')
//                    ->where('product_id',$product_id)
//                    ->update($data);
//                return Redirect::to('/all-product');
//            }
//        }
//        $data = DB::table('products')->find($product_id);
//        if($request->hasFile('product_image')){
//            $image = $request->file('product_image');
//            $file_name = time().'.'.$image->getClientOriginalExtension();
//            $old_file = $data->image;
//            $request->file->move('image/',$file_name);
//            $data->image=$file_name;
//            $image_path = ("image/$old_file");
//            unlink($image_path);
//        }
//        $data->category_id = $request->category_id;
//        $data->sub_category_id = $request->sub_category_id;
//        $data->brand_id = $request->brand_id;
//        $data->product_name = $request->product_name;
//        $data->product_short_description = $request->product_short_description;
//        $data->product_long_description = $request->product_long_description;
//        $data->product_price = $request->product_price;
//        //$data['product_image'] = $request->product_image;
//        $data->product_size = $request->product_size;
//        $data->product_color = $request->product_color;
//        $data->product_quantity = $request->product_quantity;
//        dd($data);
//        //$data->save();

        //$data['product_image'] ='';
        DB::table('products')
            ->where('product_id',$product_id)
            ->update($data);
        return Redirect::to('/all-product');
    }
    public function delete_product($product_id){
        DB::table('products')
            ->where('product_id',$product_id)
            ->delete();
        return Redirect::to('/all-product');
    }
    public function AdminAuthCheck(){
        if(Session::get('admin_id')) {
            $admin_id = Session::get('admin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/admin')->send();
            }
        }elseif (Session::get('useradmin_id')){
            $admin_id = Session::get('useradmin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/user-admin')->send();
            }
        }
    }
}
