<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Session;
use Cart;
session_start();

class CheckoutController extends Controller
{
    public function login_check(){
        return view('pages.login');
    }
    public function customer_registration(Request $request){
        $data = array();
        $data['customer_name'] = $request->customer_name;
        $data['customer_email'] = $request->customer_email;
        $data['password'] = md5($request->password);
        $data['mobile_number'] = $request->mobile_number;

        $customer_id = DB::table('customers')
            ->insertGetId($data);

        Session::put('customer_id',$customer_id);
        Session::put('customer_name',$request->customer_name);
        return Redirect::to('/checkout');
    }
    public function checkout(){
        return view('pages.checkout');

    }
    public function save_shipping_details(Request $request){
        $data = array();
        $data['shipping_email'] = $request->shipping_email;
        $data['shipping_first_name'] = $request->shipping_first_name;
        $data['shipping_last_name'] = $request->shipping_last_name;
        $data['shipping_address'] = $request->shipping_address;
        $data['shipping_mobile_number'] = $request->shipping_mobile_number;
        $data['shipping_city'] = $request->shipping_city;

        $shipping_id = DB::table('shippings')
            ->insertGetId($data);
        Session::put('shipping_id',$shipping_id);
        return Redirect::to('/payment');
    }
    public function customer_login(Request $request){
        $customer_email = $request->customer_email;
        $password = md5($request->password);
        $result = DB::table('customers')
            ->where('customer_email',$customer_email)
            ->where('password',$password)
            ->first();
        if($result){
            Session::put('customer_id',$result->customer_id);
            Session::put('customer_name',$result->customer_name);
            return Redirect::to('/checkout');
        }else{
            return Redirect::to('/login-check');
        }
    }
    public function payment(){
        return view('pages.payment');
    }
    public function order_place(Request $request){
        $payment_method = $request->payment_method;
        $payment_data = array();
        $payment_data['payment_method'] = $payment_method;
        $payment_data['payment_status'] = 'pending';
        $payment_id = DB::table('payments')
            ->insertGetId($payment_data);
        $order_data = array();
        $order_data['customer_id'] = Session::get('customer_id');
        $order_data['shipping_id'] = Session::get('shipping_id');
        $order_data['payment_id'] = $payment_id;
        $order_data['order_total'] = Cart::total();
        $order_data['order_status'] = 'pending';
        $order_id = DB::table('orders')
            ->insertGetId($order_data);
        $contents = Cart::content();
        $order_details_data = array();
        foreach ($contents as $v_content){
            $order_details_data['order_id'] = $order_id;
            $order_details_data['product_id'] = $v_content->id;
            $order_details_data['product_name'] = $v_content->name;
            $order_details_data['product_price'] = $v_content->price;
            $order_details_data['product_sales_quantity'] = $v_content->qty;

            DB::table('order_details')
                ->insert($order_details_data);
        }
        if($payment_method == 'handcash'){
            Cart::destroy();
            return view('pages.handcash');


        }elseif ($payment_method == 'bkash'){
            Cart::destroy();
            return view('pages.bkash');

        }elseif ($payment_method == 'paypal'){
            return Redirect::to('/payment');

        }elseif ($payment_method == 'debitcard'){
            return Redirect::to('/payment');

        }else{
            return Redirect::to('/payment');
        }
    }
    private function sslapi(){

    }

    public function customer_logout(){
        Session::flush();
        return Redirect::to('/login-check');
    }
}
