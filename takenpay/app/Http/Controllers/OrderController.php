<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Session;
use Cart;
session_start();
class OrderController extends Controller
{
    public function manage_order(){
        $this->AdminAuthCheck();
        $all_order_info = DB::table('orders')
            ->join('customers','orders.customer_id','=','customers.customer_id')
            ->select('orders.*','customers.customer_name')
            ->paginate(10);
        $manage_order = view('admin.order.all_orders')
            ->with('all_order_info',$all_order_info);
        return view('admin_layout')
            ->with('admin.order.all_orders',$manage_order);
    }
    public function view_order($order_id){
        $this->AdminAuthCheck();
        $order_view_by_id = DB::table('orders')
            ->join('customers','orders.customer_id','=','customers.customer_id')
            ->join('order_details','orders.order_id','=','order_details.order_id')
            ->join('shippings','orders.shipping_id','=','shippings.shipping_id')
            ->join('products','order_details.product_id','=','products.product_id')
            ->join('payments','orders.payment_id','=','payments.payment_id')
            ->select('orders.*','order_details.*','shippings.*','customers.*','products.*','payments.*')
            ->where('orders.order_id',$order_id)
            ->get();

        $view_order = view('admin.order.view_order')
            ->with('order_view_by_id',$order_view_by_id);
        return view('admin_layout')
            ->with('admin.order.view_order',$view_order);

        //return view('admin.order.view_order');
    }
    public function inactive_order($order_id){
        //dd($product_id);
        DB::table('orders')
            ->where('order_id',$order_id)
            ->update(['order_status' => 'completed']);
        return Redirect::to('/manage-order');
    }
    public function active_order($order_id){
        DB::table('orders')
            ->where('order_id',$order_id)
            ->update(['order_status' => 'pending']);
        return Redirect::to('/manage-order');
    }
    public function delete_order($order_id){
        DB::table('orders')
            ->where('order_id',$order_id)
            ->delete();
        return Redirect::to('/manage-order');
    }
    public function AdminAuthCheck(){
        if(Session::get('admin_id')) {
            $admin_id = Session::get('admin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/admin')->send();
            }
        }elseif (Session::get('useradmin_id')){
            $admin_id = Session::get('useradmin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/user-admin')->send();
            }
        }
    }
}
