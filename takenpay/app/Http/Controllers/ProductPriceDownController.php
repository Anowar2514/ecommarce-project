<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Session;
session_start();
class ProductPriceDownController extends Controller
{
    public function price_down_index(){
        $this->AdminAuthCheck();
        return view('admin.product_price_up&down.add_product_price_down');
    }
    public function save_price_down(Request $request)
    {
        $data =array();
        //$data['product_id'] = $request->product_id;
        $data['category_id'] = $request->category_id;
        $data['brand_id'] = $request->brand_id;
        $data['product_name'] = $request->product_name;
        $data['product_short_description'] = $request->product_short_description;
        $data['product_long_description'] = $request->product_long_description;
        $data['product_price'] = $request->product_price;
        //$data['product_image'] = $request->product_image;
        $data['product_size'] = $request->product_size;
        $data['product_color'] = $request->product_color;
        $data['publication_status'] = $request->publication_status;
        $image = $request->file('product_image');
        if($image){
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'price_down_image/';
            $image_url = $upload_path.$image_full_name;
            $success = $image->move($upload_path,$image_full_name);
            if ($success){
                $data['product_image'] = $image_url;
                DB::table('price_downs')->insert($data);
                return Redirect::to('/add-price-down');
            }
        }
        $data['product_image'] ='';
        DB::table('price_downs')->insert($data);
        return Redirect::to('/add-price-down');


    }
    public function all_price_down(){
        $this->AdminAuthCheck();
        $all_product_info = DB::table('price_downs')
            ->join('categories','price_downs.category_id','=','categories.category_id')
            ->join('brands','price_downs.brand_id','=','brands.brand_id')
            ->select('price_downs.*','categories.category_name','brands.brand_name')
            ->paginate(6);
        $manage_product = view('admin.product_price_up&down.all_product_price_down')
            ->with('all_product_info',$all_product_info);
        return view('admin_layout')
            ->with('admin.product_price_up&down.all_product_price_down',$manage_product);
    }
    public function inactive_price_up($price_down_id){
        //dd($product_id);
        DB::table('price_downs')
            ->where('price_down_id',$price_down_id)
            ->update(['publication_status' => 0]);
        return Redirect::to('/all-price-down');
    }
    public function active_price_down($price_down_id){
        DB::table('price_downs')
            ->where('price_down_id',$price_down_id)
            ->update(['publication_status' =>1]);
        return Redirect::to('/all-price-down');
    }
    public function edit_price_down($price_down_id){
        $this->AdminAuthCheck();
        $product_info = DB::table('price_downs')
            ->where('price_down_id',$price_down_id)
            ->first();

        $product_info = view('admin.product_price_up&down.edit_product_price_down')
            ->with('product_info',$product_info);
        return view('admin_layout')
            ->with('admin.product_price_up&down.edit_product_price_down',$product_info);
        //return view('admin.edit_category');
    }
    public function update_price_down(Request $request,$price_down_id){
        $data = array();
        $data['category_id'] = $request->category_id;
        $data['brand_id'] = $request->brand_id;
        $data['product_short_description'] = $request->product_short_description;
        $data['product_long_description'] = $request->product_long_description;
        $data['product_price'] = $request->product_price;
        //$data['product_image'] = $request->product_image;
        $data['product_size'] = $request->product_size;
        $data['product_color'] = $request->product_color;
//        $data['publication_status'] = $request->publication_status;
//        $image = $request->file('product_image');
//        if($image){
//            $image_name = str_random(20);
//            $ext = strtolower($image->getClientOriginalExtension());
//            $image_full_name = $image_name.'.'.$ext;
//            $upload_path = 'price_down_image/';
//            $image_url = $upload_path.$image_full_name;
//            $success = $image->move($upload_path,$image_full_name);
//            if ($success){
//                $data['product_image'] = $image_url;
//                DB::table('price_downs')
//                    ->where('price_down_id',$price_down_id)
//                    ->update($data);
//                return Redirect::to('/all-price-down');
//            }
//        }
//        $data['product_image'] ='';
        DB::table('price_downs')
            ->where('price_down_id',$price_down_id)
            ->update($data);
        return Redirect::to('/all-price-down');
    }
    public function delete_price_down($price_down_id){
        DB::table('price_downs')
            ->where('price_down_id',$price_down_id)
            ->delete();
        return Redirect::to('/all-price-down');
    }
    public function AdminAuthCheck(){
        if(Session::get('admin_id')) {
            $admin_id = Session::get('admin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/admin')->send();
            }
        }elseif (Session::get('useradmin_id')){
            $admin_id = Session::get('useradmin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/user-admin')->send();
            }
        }
    }
}
