<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Session;
session_start();
class BrandController extends Controller
{
    public function index(){
        $this->AdminAuthCheck();
        return view('admin.brand.add_brand');
    }
    public function save_brand(Request $request)
    {
        $data =array();
        $data['brand_id'] = $request->brand_id;
        $data['brand_name'] = $request->brand_name;
        $data['brand_description'] = $request->brand_description;
        $data['publication_status'] = $request->publication_status;
        DB::table('brands')->insert($data);
        return Redirect::to('/add-brand');
    }
    public function all_brand(){
        $this->AdminAuthCheck();
        $all_brand_info = DB::table('brands')->paginate(6);
        $manage_brand = view('admin.brand.all_brand')
            ->with('all_brand_info',$all_brand_info);
        return view('admin_layout')
            ->with('admin.brand.all_brand',$manage_brand);
    }
    public function inactive_brand($brand_id){
        DB::table('brands')
            ->where('brand_id',$brand_id)
            ->update(['publication_status' =>0]);
        return Redirect::to('/all-brand');
    }
    public function active_brand($brand_id){
        DB::table('brands')
            ->where('brand_id',$brand_id)
            ->update(['publication_status' =>1]);
        return Redirect::to('/all-brand');
    }
    public function edit_brand($brand_id){
        $this->AdminAuthCheck();
        $brand_info = DB::table('brands')
            ->where('brand_id',$brand_id)
            ->first();

        $brand_info = view('admin.brand.edit_brand')
            ->with('brand_info',$brand_info);
        return view('admin_layout')
            ->with('admin.brand.edit_brand',$brand_info);
        //return view('admin.edit_category');
    }
    public function update_brand(Request $request,$brand_id){
        $data = array();
        $data['brand_name'] = $request->brand_name;
        $data['brand_description'] = $request->brand_description;
        DB::table('brands')
            ->where('brand_id',$brand_id)
            ->update($data);
        return Redirect::to('/all-brand');
    }
    public function delete_brand($brand_id){
        DB::table('brands')
            ->where('brand_id',$brand_id)
            ->delete();
        return Redirect::to('/all-brand');
    }
    public function AdminAuthCheck(){
        if(Session::get('admin_id')) {
            $admin_id = Session::get('admin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/admin')->send();
            }
        }elseif (Session::get('useradmin_id')){
            $admin_id = Session::get('useradmin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/user-admin')->send();
            }
        }
    }
}
