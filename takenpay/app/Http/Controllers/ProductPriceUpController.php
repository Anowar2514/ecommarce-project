<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Session;
session_start();
class ProductPriceUpController extends Controller
{
    public function price_up_index(){
        $this->AdminAuthCheck();
        return view('admin.product_price_up&down.add_product_price_up');
    }
    public function save_price_up(Request $request)
    {
        $data =array();
        //$data['product_id'] = $request->product_id;
        $data['category_id'] = $request->category_id;
        $data['brand_id'] = $request->brand_id;
        $data['product_name'] = $request->product_name;
        $data['product_short_description'] = $request->product_short_description;
        $data['product_long_description'] = $request->product_long_description;
        $data['product_price'] = $request->product_price;
        //$data['product_image'] = $request->product_image;
        $data['product_size'] = $request->product_size;
        $data['product_color'] = $request->product_color;
        $data['publication_status'] = $request->publication_status;
        $image = $request->file('product_image');
        if($image){
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'price_up_image/';
            $image_url = $upload_path.$image_full_name;
            $success = $image->move($upload_path,$image_full_name);
            if ($success){
                $data['product_image'] = $image_url;
                DB::table('price_ups')->insert($data);
                return Redirect::to('/add-price-up');
            }
        }
        $data['product_image'] ='';
        DB::table('price_ups')->insert($data);
        return Redirect::to('/add-price-up');


    }
    public function all_price_up(){
        $this->AdminAuthCheck();
        $all_product_info = DB::table('price_ups')
            ->join('categories','price_ups.category_id','=','categories.category_id')
            ->join('brands','price_ups.brand_id','=','brands.brand_id')
            ->select('price_ups.*','categories.category_name','brands.brand_name')
            ->paginate(6);
        $manage_product = view('admin.product_price_up&down.all_product_price_up')
            ->with('all_product_info',$all_product_info);
        return view('admin_layout')
            ->with('admin.product_price_up&down.all_product_price_up',$manage_product);
    }
    public function inactive_price_up($price_up_id){
        //dd($product_id);
        DB::table('price_ups')
            ->where('price_up_id',$price_up_id)
            ->update(['publication_status' => 0]);
        return Redirect::to('/all-price-up');
    }
    public function active_price_up($price_up_id){
        DB::table('price_ups')
            ->where('price_up_id',$price_up_id)
            ->update(['publication_status' =>1]);
        return Redirect::to('/all-price-up');
    }
    public function edit_price_up($price_up_id){
        $this->AdminAuthCheck();
        $product_info = DB::table('price_ups')
            ->where('price_up_id',$price_up_id)
            ->first();

        $product_info = view('admin.product_price_up&down.edit_product_price_up')
            ->with('product_info',$product_info);
        return view('admin_layout')
            ->with('admin.product_price_up&down.edit_product_price_up',$product_info);
        //return view('admin.edit_category');
    }
    public function update_price_up(Request $request,$price_up_id){
        $data = array();
        $data['category_id'] = $request->category_id;
        $data['brand_id'] = $request->brand_id;
        //$data['product_name'] = $request->product_name;
        $data['product_short_description'] = $request->product_short_description;
        $data['product_long_description'] = $request->product_long_description;
        $data['product_price'] = $request->product_price;
        //$data['product_image'] = $request->product_image;
        $data['product_size'] = $request->product_size;
        $data['product_color'] = $request->product_color;
//        $data['publication_status'] = $request->publication_status;
//        $image = $request->file('product_image');
//        if($image){
//            $image_name = str_random(20);
//            $ext = strtolower($image->getClientOriginalExtension());
//            $image_full_name = $image_name.'.'.$ext;
//            $upload_path = 'price_up_image/';
//            $image_url = $upload_path.$image_full_name;
//            $success = $image->move($upload_path,$image_full_name);
//            if ($success){
//                $data['product_image'] = $image_url;
//                DB::table('price_ups')
//                    ->where('price_up_id',$price_up_id)
//                    ->update($data);
//                return Redirect::to('/all-price-up');
//            }
//        }
//        $data['product_image'] ='';
        DB::table('price_ups')
            ->where('price_up_id',$price_up_id)
            ->update($data);
        return Redirect::to('/all-price-up');
    }
    public function delete_price_up($price_up_id){
        DB::table('price_ups')
            ->where('price_up_id',$price_up_id)
            ->delete();
        return Redirect::to('/all-price-up');
    }
    public function AdminAuthCheck(){
        if(Session::get('admin_id')) {
            $admin_id = Session::get('admin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/admin')->send();
            }
        }elseif (Session::get('useradmin_id')){
            $admin_id = Session::get('useradmin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/user-admin')->send();
            }
        }
    }
}
