<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Session;
session_start();
class UserAdminController extends Controller
{
    public function add_admin(){
        $this->AdminAuthCheck();
        return view('admin.admin-user.add_admin');
    }
    public function all_admin(){
        $this->AdminAuthCheck();
        $all_admin_info = DB::table('user_admins')->paginate(6);
        $manage_admin = view('admin.admin-user.all_admin')
            ->with('all_admin_info',$all_admin_info);
        return view('admin_layout')
            ->with('admin.admin-user.all_admin',$manage_admin);
    }
    public function save_admin(Request $request){
        $data =array();
        $data['admin_name'] = $request->admin_name;
        $data['admin_email'] = $request->admin_email;
        $data['admin_phone'] = $request->admin_phone;
        $data['admin_password'] = md5($request->admin_password);
        $data['publication_status'] = $request->publication_status;
        DB::table('user_admins')->insert($data);
        //Session::put('message','Category added successfully added !!!');
        return Redirect::to('/add-admins');
    }
    public function block_admin($useradmin_id){
        DB::table('user_admins')
            ->where('useradmin_id',$useradmin_id)
            ->update(['publication_status' =>0]);
        return Redirect::to('/all-admins');
    }
    public function active_admin($useradmin_id){
        DB::table('user_admins')
            ->where('useradmin_id',$useradmin_id)
            ->update(['publication_status' =>1]);
        return Redirect::to('/all-admins');
    }
    public function delete_admin($useradmin_id){
        DB::table('user_admins')
            ->where('useradmin_id',$useradmin_id)
            ->delete();
        return Redirect::to('/all-admins');
    }
    public function index()
    {
        return view('useradmin_login');
    }
    public function dashboard(Request $request){
        $admin_email = $request->admin_email;
        $admin_password = md5($request->admin_password);
        $result = DB::table('user_admins')
            ->where('admin_email',$admin_email)
            ->where('admin_password',$admin_password)
            ->where('publication_status',1)
            ->first();
        if ($result){
            Session::put('admin_name',$result->admin_name);
            Session::put('useradmin_id',$result->useradmin_id);
            Session::put('admin_email',$result->admin_email);
            return Redirect::to('/user-dashboard');
        }else{
            Session::put('message','Email or Password Invalid');
            return Redirect::to('/user-admin');
        }
    }
    public function user_index(){
        $this->AdminAuthCheck();
        if(Session::has('useradmin_id')){

        }else{
            return Redirect::to('/user-admin')->with('flash_message_error','Please login to access');
        }
        return view('admin.dashboard');
    }
    public function logout(){
        Session::flush();
        return Redirect::to('/user-admin');
    }


    public function AdminAuthCheck(){
        if(Session::get('admin_id')) {
            $admin_id = Session::get('admin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/admin')->send();
            }
        }elseif (Session::get('useradmin_id')){
            $admin_id = Session::get('useradmin_id');
            if($admin_id){
                return;
            }else{
                return Redirect::to('/user-admin')->send();
            }
        }

    }
}
