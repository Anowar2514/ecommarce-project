<?php

namespace App\Http\Controllers;

use App\Mail\SendMail;
use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Session;
session_start();
use Mail;
class MessageController extends Controller
{
    public function send_message(Request $request){
        $data = array();
        $data['customer_name']=$request->customer_name;
        $data['customer_email']=$request->customer_email;
        $data['message_subject']=$request->message_subject;
        $data['customer_message']=$request->customer_message;
        DB::table('messages')->insert($data);
        return Redirect::to('/contact');
    }
    public function all_message(){
        $this->AdminAuthCheck();
        $all_messages_info = DB::table('messages')->paginate(3);
        $manage_messages = view('admin.messages.all_messages')
            ->with('all_messages_info',$all_messages_info);
        return view('admin_layout')
            ->with('admin.messages.all_messages',$manage_messages);
    }
    public function inactive_message($message_id){
        DB::table('messages')
            ->where('message_id',$message_id)
            ->update(['publication_status' =>0]);
        return Redirect::to('/all-messages');
    }
    public function active_message($message_id){
        DB::table('messages')
            ->where('message_id',$message_id)
            ->update(['publication_status' =>1]);
        return Redirect::to('/all-messages');
    }
    public function view_message($message_id){
        $this->AdminAuthCheck();
        $view_messages_info = DB::table('messages')
                         ->where('message_id',$message_id)
                         ->get();
        $view_manage_messages = view('admin.messages.view_message')
            ->with('view_messages_info',$view_messages_info);
        return view('admin_layout')
            ->with('admin.messages.view_message',$view_manage_messages);
    }
    public function delete_message($message_id){
        DB::table('messages')
            ->where('message_id',$message_id)
            ->delete();
        return Redirect::to('/all-messages');
    }
    public function send_email(){
        Mail::send(new SendMail());
        return Redirect::to('/all-messages');
    }

    public function AdminAuthCheck(){
        if(Session::get('admin_id')) {
            $admin_id = Session::get('admin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/admin')->send();
            }
        }elseif (Session::get('useradmin_id')){
            $admin_id = Session::get('useradmin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/user-admin')->send();
            }
        }
    }
}
