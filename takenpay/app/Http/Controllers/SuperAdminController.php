<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;
use Session;
session_start();

class SuperAdminController extends Controller
{
    public function index(){
        $this->AdminAuthCheck();
        if(Session::has('admin_id')){

        }else{
            return Redirect::to('/admin')->with('flash_message_error','Please login to access');
        }
        return view('admin.dashboard');
    }
    public function logout(){
        if(Session::get('admin_id')) {
            Session::flush();
            return Redirect::to('/admin');
        }elseif (Session::get('useradmin_id')){
            Session::flush();
            return Redirect::to('/user-admin');
        }
    }
    public function AdminAuthCheck(){
        if(Session::get('admin_id')) {
            $admin_id = Session::get('admin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/admin')->send();
            }
        }elseif (Session::get('useradmin_id')){
            $admin_id = Session::get('useradmin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/user-admin')->send();
            }
        }
    }
}
