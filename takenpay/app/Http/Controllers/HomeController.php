<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Redirect;
use Session;
session_start();
class HomeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $all_published_product = DB::table('products')
            ->join('categories','products.category_id','=','categories.category_id')
            ->join('brands','products.brand_id','=','brands.brand_id')
            ->select('products.*','categories.category_name','brands.brand_name')
            ->where('products.publication_status',1)
            ->paginate(30);
        $manage_published_product = view('pages.home_content')
            ->with('all_published_product',$all_published_product);
        return view('layout')
            ->with('pages.home_content',$manage_published_product);
        //return view('pages.home_content');
    }
    public function about(){
        return view('pages.about');
    }
    public function contact(){
        return view('pages.contact');
    }
    public function category_show($category_id){
        $product_by_category = DB::table('products')
            ->join('categories','products.category_id','=','categories.category_id')
            ->join('sub_categories','products.sub_category_id','=','sub_categories.sub_category_id')
            ->select('products.*','categories.category_name')
            ->where('categories.category_id',$category_id)
            ->where('products.publication_status',1)
            ->paginate(9);
        $manage_product_by_category = view('pages.product_by_category')
            ->with('product_by_category',$product_by_category);
        return view('layout')
            ->with('pages.product_by_category',$manage_product_by_category);
    }
    public function brand_show($brand_id){
        $product_by_brand = DB::table('products')
            ->join('brands','products.brand_id','=','brands.brand_id')
            ->select('products.*','brands.brand_name')
            ->where('brands.brand_id',$brand_id)
            ->where('products.publication_status',1)
            ->paginate(9);
        $manage_product_by_brand = view('pages.product_by_brand')
            ->with('product_by_brand',$product_by_brand);
        return view('layout')
            ->with('pages.product_by_brand',$manage_product_by_brand);
    }
    public function product_details_by_id($product_id){
        $product_by_details = DB::table('products')
            ->join('categories','products.category_id','=','categories.category_id')
            ->join('brands','products.brand_id','=','brands.brand_id')
            ->select('products.*','categories.category_name','brands.brand_name')
            ->where('products.product_id',$product_id)
            ->where('products.publication_status',1)
            ->first();
        $manage_product_by_details = view('pages.product_details')
            ->with('product_by_details',$product_by_details);
        return view('layout')
            ->with('pages.product_details',$manage_product_by_details);
    }
    public function price_up_details_by_id($price_up_id){
        $price_by_details = DB::table('price_ups')
            ->join('categories','price_ups.category_id','=','categories.category_id')
            ->join('brands','price_ups.brand_id','=','brands.brand_id')
            ->select('price_ups.*','categories.category_name','brands.brand_name')
            ->where('price_ups.price_up_id',$price_up_id)
            ->where('price_ups.publication_status',1)
            ->first();
        $manage_price_by_details = view('pages.price_details')
            ->with('price_by_details',$price_by_details);
        return view('layout')
            ->with('pages.price_details',$manage_price_by_details);
    }

    public function price_down_details_by_id($price_down_id){
        $price_down_by_details = DB::table('price_downs')
            ->join('categories','price_downs.category_id','=','categories.category_id')
            ->join('brands','price_downs.brand_id','=','brands.brand_id')
            ->select('price_downs.*','categories.category_name','brands.brand_name')
            ->where('price_downs.price_down_id',$price_down_id)
            ->where('price_downs.publication_status',1)
            ->first();
        $manage_price_by_details = view('pages.price_down_details')
            ->with('price_down_by_details',$price_down_by_details);
        return view('layout')
            ->with('pages.price_down_details',$manage_price_by_details);
    }

    public function search(Request $request)
    {
        $request->validate([
            'search' =>'required|min:3',
        ]);
        $search = $request->input('search');
        $products = DB::table('products')
            ->where('product_name','like',"%$search%")
            ->paginate(15);
        return view('pages.search')->with('products',$products);
    }
    public function related_price_product()
    {
        $all_published_price_up = DB::table('price_ups')
            ->join('categories','price_ups.category_id','=','categories.category_id')
            ->join('brands','price_ups.brand_id','=','brands.brand_id')
            ->select('price_ups.*','categories.category_name','brands.brand_name')
            ->where('price_ups.publication_status',1)
            ->paginate(2);
        $manage_published_price_up = view('pages.price_details')
            ->with('all_published_price_up',$all_published_price_up);
        return view('layout')
            ->with('pages.price_details',$manage_published_price_up);
        //return view('pages.home_content');
    }
    public function sub_category($sub_category_id){
        $product_by_sub_category = DB::table('products')
            ->join('sub_categories','products.sub_category_id','=','sub_categories.sub_category_id')
            ->select('products.*','sub_categories.sub_category_name')
            ->where('sub_categories.sub_category_id',$sub_category_id)
            ->where('products.publication_status',1)
            ->get();
        $manage_product_by_sub_category = view('pages.mans_clothing')
            ->with('product_by_sub_category',$product_by_sub_category);
        return view('layout')
            ->with('pages.mans_clothing',$manage_product_by_sub_category);
    }
    public function price_range(Request $request){
        $this->validate($request,[
            'price_range' => 'required'
            ]);
        $minprice = DB::table('products')
            ->min('product_price');
        $maxprice = DB::table('products')
            ->max('product_price');
        $price_range = $request->price_range;
        if(!empty($price_range)){
            $priceRangeArr =explode(",",$price_range);
            $start = $priceRangeArr[0];
            $end = $priceRangeArr[1];
            if($minprice <= $start && $maxprice >= $end){
                $all_published_product = DB::table('products')
                    ->join('categories','products.category_id','=','categories.category_id')
                    ->join('brands','products.brand_id','=','brands.brand_id')
                    ->select('products.*','categories.category_name','brands.brand_name')
                    ->where('products.publication_status',1)
                    ->where('products.product_price','>=',$start)
                    ->where('products.product_price','<=',$end)
                    ->orderBy('product_id', 'desc')
                    ->paginate(9);
                $manage_published_product = view('pages.price_range_wise_product')
                    ->with('all_published_product',$all_published_product);
                return view('layout')
                    ->with('pages.price_range_wise_product',$manage_published_product);
            }else{
                return Redirect::to('/')->send();
            }
        }else{
            return Redirect::to('/')->send();
        }
    }
    public function pagenotfound(){
        return view('admin.error.404');
    }
}
