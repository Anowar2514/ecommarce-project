<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Session;
session_start();
class SubCategoryController extends Controller
{
    public function index(){
        $this->AdminAuthCheck();
        return view('admin.sub_category.add_sub_category');
    }
    public function all_sub_category(){
        $this->AdminAuthCheck();
        $all_sub_category_info = DB::table('sub_categories')
            ->join('categories','sub_categories.category_id','=','categories.category_id')
            ->paginate(6);
        $manage_sub_category = view('admin.sub_category.all_sub_category')
            ->with('all_sub_category_info',$all_sub_category_info);
        return view('admin_layout')
            ->with('admin.sub_category.all_sub_category',$manage_sub_category);
    }
    public function save_sub_category(Request $request){
        $data =array();
        $data['category_id'] = $request->category_id;
        $data['sub_category_name'] = $request->sub_category_name;
        $data['sub_category_description'] = $request->sub_category_description;
        $data['publication_status'] = $request->publication_status;
        DB::table('sub_categories')->insert($data);
        //Session::put('message','Category added successfully added !!!');
        return Redirect::to('/add-sub-category');
    }
    public function inactive_sub_category($sub_category_id){
        DB::table('sub_categories')
            ->where('sub_category_id',$sub_category_id)
            ->update(['publication_status' =>0]);
        return Redirect::to('/all-sub-category');
    }
    public function active_sub_category($sub_category_id){
        DB::table('sub_categories')
            ->where('sub_category_id',$sub_category_id)
            ->update(['publication_status' =>1]);
        return Redirect::to('/all-sub-category');
    }
    public function edit_sub_category($sub_category_id){
        $this->AdminAuthCheck();
        $sub_category_info = DB::table('sub_categories')
            ->where('sub_category_id',$sub_category_id)
            ->first();

        $sub_category_info = view('admin.sub_category.edit_sub_category')
            ->with('sub_category_info',$sub_category_info);
        return view('admin_layout')
            ->with('admin.sub_category.edit_sub_category',$sub_category_info);
        //return view('admin.edit_category');
    }
    public function update_sub_category(Request $request,$sub_category_id){
        $data = array();
        $data['category_id'] = $request->category_id;
        $data['sub_category_name'] = $request->sub_category_name;
        $data['sub_category_description'] = $request->sub_category_description;
        DB::table('sub_categories')
            ->where('sub_category_id',$sub_category_id)
            ->update($data);
        return Redirect::to('/all-sub-category');
    }
    public function delete_sub_category($sub_category_id){
        DB::table('sub_categories')
            ->where('sub_category_id',$sub_category_id)
            ->delete();
        return Redirect::to('/all-sub-category');
    }
    public function AdminAuthCheck(){
        if(Session::get('admin_id')) {
            $admin_id = Session::get('admin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/admin')->send();
            }
        }elseif (Session::get('useradmin_id')){
            $admin_id = Session::get('useradmin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/user-admin')->send();
            }
        }
    }
}
