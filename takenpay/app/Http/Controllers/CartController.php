<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use DB;
use Cart;
use Illuminate\Support\Facades\Redirect;

class CartController extends Controller
{
    public function add_to_cart(Request $request){
        $qty = $request->qty;
        $product_id = $request->product_id;
        $product_info = DB::table('products')
            ->where('product_id',$product_id)
            ->first();
        $data['qty'] = $qty;
        $data['id'] = $product_info->product_id;
        $data['name'] = $product_info->product_name;
        $data['price'] = $product_info->product_price;
        $data['options']['image'] = $product_info->product_image;

        Cart::add($data);
        return Redirect::to('/show-cart');

    }
    public function show_cart(){
        $all_published_category = DB::table('categories')
            ->where('publication_status',1)
            ->get();
        $manage_published_category = view('pages.add_to_cart')
            ->with('all_published_category',$all_published_category);
        return view('layout')
            ->with('pages.add_to_cart',$manage_published_category);
    }
    public function delete_to_cart($rowId){
        Cart::update($rowId,0);
        return Redirect::to('/show-cart');
    }
    public function update_cart(Request $request){
        $qty = $request->qty;
        $rowId = $request->rowId;
        Cart::update($rowId,$qty);
        return Redirect::to('/show-cart');
    }
    public function update_cart_payment(Request $request){
        $qty = $request->qty;
        $rowId = $request->rowId;
        Cart::update($rowId,$qty);
        return Redirect::to('/payment');
    }
    public function price_up_add_to_cart(Request $request){
        $qty = $request->qty;
        $price_up_id = $request->price_up_id;
        $product_info = DB::table('price_ups')
            ->where('price_up_id',$price_up_id)
            ->first();
        $data['qty'] = $qty;
        $data['id'] = $product_info->price_up_id;
        $data['name'] = $product_info->product_name;
        $data['price'] = $product_info->product_price;
        $data['options']['image'] = $product_info->product_image;

        Cart::add($data);
        return Redirect::to('/show-cart');
    }
    public function price_down_add_to_cart(Request $request){
        $qty = $request->qty;
        $price_down_id = $request->price_down_id;
        $product_info = DB::table('price_downs')
            ->where('price_down_id',$price_down_id)
            ->first();
        $data['qty'] = $qty;
        $data['id'] = $product_info->price_down_id;
        $data['name'] = $product_info->product_name;
        $data['price'] = $product_info->product_price;
        $data['options']['image'] = $product_info->product_image;

        Cart::add($data);
        return Redirect::to('/show-cart');
    }
}
