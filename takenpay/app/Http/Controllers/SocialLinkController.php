<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Session;
session_start();
class SocialLinkController extends Controller
{
    public function index(){
        $this->AdminAuthCheck();
        return view('admin.social-link.add_social_link');
    }
    public function save_social_link(Request $request)
    {
        $data =array();
        $data['social_logo'] = $request->social_logo;
        $data['social_name'] = $request->social_name;
        $data['social_link'] = $request->social_link;
        $data['social_status'] = $request->social_status;
        DB::table('tbl_sociallinks')->insert($data);
        return Redirect::to('/add-social-link');


    }
    public function all_social_link(){
        $this->AdminAuthCheck();
        $all_social_link_info = DB::table('tbl_sociallinks')->paginate(10);
        $manage_social_link = view('admin.social-link.all_social_link')
            ->with('all_social_link_info',$all_social_link_info);
        return view('admin_layout')
            ->with('admin.social-link.all_social_link',$manage_social_link);
    }
    public function inactive_social_link($social_id){
        DB::table('tbl_sociallinks')
            ->where('social_id',$social_id)
            ->update(['social_status' => 'inactive']);
        return Redirect::to('/all-social-link');
    }
    public function active_social_link($social_id){
        DB::table('tbl_sociallinks')
            ->where('social_id',$social_id)
            ->update(['social_status' =>'active']);
        return Redirect::to('/all-social-link');
    }
    public function edit_social_link($social_id){
        $this->AdminAuthCheck();
        $social_link_info = DB::table('tbl_sociallinks')
            ->where('social_id',$social_id)
            ->first();

        $social_link_info = view('admin.social-link.edit_social_link')
            ->with('social_link_info',$social_link_info);
        return view('admin_layout')
            ->with('admin.social-link.edit_social_link',$social_link_info);
    }
    public function update_social_link(Request $request,$social_id){
        $data = array();
        $data['social_logo'] = $request->social_logo;
        $data['social_name'] = $request->social_name;
        $data['social_link'] = $request->social_link;
        DB::table('tbl_sociallinks')
            ->where('social_id',$social_id)
            ->update($data);
        return Redirect::to('/all-social-link');
    }
    public function delete_social_link($social_id){
        DB::table('tbl_sociallinks')
            ->where('social_id',$social_id)
            ->delete();
        return Redirect::to('/all-social-link');
    }
    public function AdminAuthCheck(){
        if(Session::get('admin_id')) {
            $admin_id = Session::get('admin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/admin')->send();
            }
        }elseif (Session::get('useradmin_id')){
            $admin_id = Session::get('useradmin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/user-admin')->send();
            }
        }
    }
}
