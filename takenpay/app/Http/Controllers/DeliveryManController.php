<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\Http\Requests;
use Illuminate\Support\Facades\Redirect;
use Session;
session_start();

class DeliveryManController extends Controller
{
    public function index(){
        $this->AdminAuthCheck();
        return view('admin.delivery_man.add_delivery_man');
    }
    //delivery_man
    public function save_delivery_man(Request $request)
    {
        $data =array();
        $data['delivery_man_name'] = $request->delivery_man_name;
        $data['delivery_man_phone'] = $request->delivery_man_phone;
        $data['deliveryman_status'] = $request->deliveryman_status;
        $image = $request->file('delivery_man_image');
        if($image){
            $image_name = str_random(20);
            $ext = strtolower($image->getClientOriginalExtension());
            $image_full_name = $image_name.'.'.$ext;
            $upload_path = 'delivery_man/';
            $image_url = $upload_path.$image_full_name;
            $success = $image->move($upload_path,$image_full_name);
            if ($success){
                $data['delivery_man_image'] = $image_url;
                DB::table('tbl_deliverymens')->insert($data);
                return Redirect::to('/add-delivery-man');
            }
        }
        $data['delivery_man_image'] ='';
        DB::table('tbl_deliverymens')->insert($data);
        return Redirect::to('/add-delivery-man');
    }
    public function all_delivery_man(){
        $this->AdminAuthCheck();
        $all_delivery_man_info = DB::table('tbl_deliverymens')
                               ->paginate(10);
        $manage_delivery_man = view('admin.delivery_man.all_delivery_man')
            ->with('all_delivery_man_info',$all_delivery_man_info);
        return view('admin_layout')
            ->with('admin.delivery_man.all_delivery_man',$manage_delivery_man);
    }
    public function inactive_delivery_man($deliveryman_id){
        //dd($product_id);
        DB::table('tbl_deliverymens')
            ->where('deliveryman_id',$deliveryman_id)
            ->update(['deliveryman_status' => 'inactive']);
        return Redirect::to('/all-delivery-man');
    }
    public function active_delivery_man($deliveryman_id){
        DB::table('tbl_deliverymens')
            ->where('deliveryman_id',$deliveryman_id)
            ->update(['deliveryman_status' =>'active']);
        return Redirect::to('/all-delivery-man');
    }
    public function delete_delivery_man($deliveryman_id){
        DB::table('tbl_deliverymens')
            ->where('deliveryman_id',$deliveryman_id)
            ->delete();
        return Redirect::to('/all-delivery-man');
    }
    public function AdminAuthCheck(){
        if(Session::get('admin_id')) {
            $admin_id = Session::get('admin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/admin')->send();
            }
        }elseif (Session::get('useradmin_id')){
            $admin_id = Session::get('useradmin_id');
            if ($admin_id) {
                return;
            } else {
                return Redirect::to('/user-admin')->send();
            }
        }
    }
}
