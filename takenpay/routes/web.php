<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//----error page------------
Route::get('pagenotfound',['as'=>'notfound','uses'=>'HomeController@pagenotfound']);
//-----------------------------------------------Front-End-----------------------------------------------------------
//front-end section..............
Route::get('/','HomeController@index');
Route::get('/about','HomeController@about');
Route::get('/contact','HomeController@contact');

//sub category related route........
Route::get('/product_by_sub_category/{sub_category_id}','HomeController@sub_category');

//category wise product display
Route::get('/product_by_category/{category_id}','HomeController@category_show');
Route::get('/product_by_brand/{brand_id}','HomeController@brand_show');

//view product in cart
Route::get('/view_product/{product_id}','HomeController@product_details_by_id');
Route::get('/view_price/{price_up_id}','HomeController@price_up_details_by_id');

Route::get('/view-down/{price_down_id}','HomeController@price_down_details_by_id');

//cart related route
Route::post('/add-to-cart','CartController@add_to_cart');
Route::get('/show-cart','CartController@show_cart');
Route::get('/delete-to-cart/{rowId}','CartController@delete_to_cart');
Route::post('/update-cart','CartController@update_cart');
Route::post('/update-cart-payment','CartController@update_cart_payment');

//price up cart related route
Route::post('/price-up-add-to-cart','CartController@price_up_add_to_cart');

//price down cart related route
Route::post('/price-down-add-to-cart','CartController@price_down_add_to_cart');

//checkout & Shipping related route
Route::get('/login-check','CheckoutController@login_check');
Route::post('/customer-registration','CheckoutController@customer_registration');
Route::get('/checkout','CheckoutController@checkout');
Route::post('/save-shipping-details','CheckoutController@save_shipping_details');

//customer login & logout route
Route::post('/customer-login','CheckoutController@customer_login');
Route::get('/customer-logout','CheckoutController@customer_logout');

//customer payment method related route.......
Route::get('/payment','CheckoutController@payment');
Route::post('/order-place','CheckoutController@order_place');

//customer message related route
Route::post('/send-message','MessageController@send_message');

//search related route
Route::get('/search','HomeController@search')->name('search');

//price down product related route
Route::get('/price-down-product',function (){
    return view('pages.price_down_product');
});

//price up product related route
Route::get('/price-up-product',function (){
    return view('pages.price_up_product');
});

Route::get('/price-range','HomeController@price_range');

//------------------------------------------------------Back End---------------------------------------------------------------



//back-end section...............
Route::get('/logout','SuperAdminController@logout');
Route::get('/admin','AdminController@index');
Route::get('/dashboard','SuperAdminController@index');
Route::post('/admin-dashboard','AdminController@dashboard');

//new user admin related route..............
Route::get('/add-admins','UserAdminController@add_admin');
Route::post('/save-admin','UserAdminController@save_admin');
Route::get('/all-admins','UserAdminController@all_admin');
Route::get('/block-admin/{useradmin_id}','UserAdminController@block_admin');
Route::get('/active-admin/{useradmin_id}','UserAdminController@active_admin');
Route::get('/delete-admin/{useradmin_id}','UserAdminController@delete_admin');

//new user admin login logout related route............
Route::get('/user-logout','UserAdminController@logout');
Route::get('/user-admin','UserAdminController@index');
Route::get('/user-dashboard','UserAdminController@user_index');
Route::post('/useradmin-dashboard','UserAdminController@dashboard');

//delivery man related route..
Route::get('/add-delivery-man','DeliveryManController@index');
Route::post('/save-delivery-man','DeliveryManController@save_delivery_man');
Route::get('/all-delivery-man','DeliveryManController@all_delivery_man');
Route::get('/inactive-delivery-man/{deliveryman_id}','DeliveryManController@inactive_delivery_man');
Route::get('/active-delivery-man/{deliveryman_id}','DeliveryManController@active_delivery_man');
Route::get('/delete-delivery-man/{deliveryman_id}','DeliveryManController@delete_delivery_man');

//social link related route
Route::get('/add-social-link','SocialLinkController@index');
Route::post('/save-social-link','SocialLinkController@save_social_link');
Route::get('/all-social-link','SocialLinkController@all_social_link');
Route::get('/inactive-social-link/{social_id}','SocialLinkController@inactive_social_link');
Route::get('/active-social-link/{social_id}','SocialLinkController@active_social_link');
Route::get('/delete-social-link/{social_id}','SocialLinkController@delete_social_link');
Route::get('/edit-social-link/{social_id}','SocialLinkController@edit_social_link');
Route::get('/update-social-link/{social_id}','SocialLinkController@update_social_link');

//category related route
Route::get('/add-category','CategoryController@index');
Route::get('/all-category','CategoryController@all_category');
Route::post('/save-category','CategoryController@save_category');
Route::get('/edit-category/{category_id}','CategoryController@edit_category');
Route::post('/update-category/{category_id}','CategoryController@update_category');
Route::get('/delete-category/{category_id}','CategoryController@delete_category');

Route::get('/inactive_category/{category_id}','CategoryController@inactive_category');
Route::get('/active_category/{category_id}','CategoryController@active_category');

//sub category related route
Route::get('/add-sub-category','SubCategoryController@index');
Route::get('/all-sub-category','SubCategoryController@all_sub_category');
Route::post('/save-sub-category','SubCategoryController@save_sub_category');
Route::get('/edit-sub-category/{sub_category_id}','SubCategoryController@edit_sub_category');
Route::post('/update-sub-category/{sub_category_id}','SubCategoryController@update_sub_category');
Route::get('/delete-sub-category/{sub_category_id}','SubCategoryController@delete_sub_category');

Route::get('/inactive-sub-category/{sub_category_id}','SubCategoryController@inactive_sub_category');
Route::get('/active-sub-category/{sub_category_id}','SubCategoryController@active_sub_category');

//brands related route
Route::get('/add-brand','BrandController@index');
Route::post('/save-brand','BrandController@save_brand');
Route::get('/all-brand','BrandController@all_brand');

Route::get('/edit-brand/{brand_id}','BrandController@edit_brand');
Route::post('/update-brand/{brand_id}','BrandController@update_brand');
Route::get('/delete-brand/{brand_id}','BrandController@delete_brand');

Route::get('/inactive_brand/{brand_id}','BrandController@inactive_brand');
Route::get('/active_brand/{brand_id}','BrandController@active_brand');

//Product related route
Route::get('/add-product','ProductController@index');
Route::post('/save-product','ProductController@save_product');
Route::get('/all-product','ProductController@all_product');

Route::get('/inactive_product/{product_id}','ProductController@inactive_product');
Route::get('/active_product/{product_id}','ProductController@active_product');

Route::get('/edit-product/{product_id}','ProductController@edit_product');
Route::post('/update-product/{product_id}','ProductController@update_product');
Route::get('/delete-product/{product_id}','ProductController@delete_product');

//Product price up related route
Route::get('/add-price-up','ProductPriceUpController@price_up_index');
Route::post('/save-product-price-up','ProductPriceUpController@save_price_up');
Route::get('/all-price-up','ProductPriceUpController@all_price_up');

Route::get('/inactive_price/{price_up_id}','ProductPriceUpController@inactive_price_up');
Route::get('/active_price/{price_up_id}','ProductPriceUpController@active_price_up');

Route::get('/edit-price/{price_up_id}','ProductPriceUpController@edit_price_up');
Route::post('/update-price/{price_up_id}','ProductPriceUpController@update_price_up');
Route::get('/delete-price/{price_up_id}','ProductPriceUpController@delete_price_up');

//Product price down related route
Route::get('/add-price-down','ProductPriceDownController@price_down_index');
Route::post('/save-product-price-down','ProductPriceDownController@save_price_down');
Route::get('/all-price-down','ProductPriceDownController@all_price_down');

Route::get('/inactive-price-down/{price_down_id}','ProductPriceDownController@inactive_price_down');
Route::get('/active-price-down/{price_down_id}','ProductPriceDownController@active_price_down');

Route::get('/edit-price-down/{price_down_id}','ProductPriceDownController@edit_price_down');
Route::post('/update-price-down/{price_down_id}','ProductPriceDownController@update_price_down');
Route::get('/delete-price-down/{price_down_id}','ProductPriceDownController@delete_price_down');

//Slider related route
Route::get('/add-slider','SliderController@index');
Route::post('/save-slider','SliderController@save_slider');
Route::get('/all-slider','SliderController@all_slider');

Route::get('/inactive_slider/{slider_id}','SliderController@inactive_slider');
Route::get('/active_slider/{slider_id}','SliderController@active_slider');

Route::get('/edit-slider/{slider_id}','SliderController@edit_slider');
Route::post('/update-slider/{slider_id}','SliderController@update_slider');
Route::get('/delete-slider/{slider_id}','SliderController@delete_slider');

//Order related route
Route::get('/manage-order','OrderController@manage_order');
Route::get('/view-order/{order_id}','OrderController@view_order');
Route::get('/inactive-order/{order_id}','OrderController@inactive_order');
Route::get('/active-order/{order_id}','OrderController@active_order');
Route::get('/delete-order/{order_id}','OrderController@delete_order');

//customer messages related route...
Route::get('/all-messages','MessageController@all_message');
Route::get('/inactive-message/{message_id}','MessageController@inactive_message');
Route::get('/active-message/{message_id}','MessageController@active_message');
Route::get('/view-message/{message_id}','MessageController@view_message');
Route::get('/delete-message/{message_id}','MessageController@delete_message');
Route::post('/send-email','MessageController@send_email');