<?php
session_start();
error_reporting(E_ERROR | E_PARSE);
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dealer PANEL</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link rel="icon" href="assets/images/dash/4.png" width="16px">
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 550px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #000000;
      height: 111%;
    }

    .sidenav a{
      background-color: #00334d;
      color: #ffffff;
      cursor: pointer;
      border-left: 4px solid #00ccff;
      margin-bottom: 5%;
    }
    ul li:hover a {
      color: #000000;
    }

    /* On small screens, set height to 'auto' for the grid */
    @media screen and (max-width: 767px) {
      .row.content {height: auto;} 
    }
  </style>
</head>
<body style=" background-color: #000000;">


<div class="col-md-12 col-lg-12  hidden-xs hidden-sm" style="background-color: #001a1a">
<img src="assets/images/l2.png" style="width:11%;margin-left: 2%;margin-right: 4%;">

<h7 style="margin-top: 1%;background-color: transparent;margin-left: 0%;text-align: left;color:white">Account Type : <button class="btn btn-info btn-xs">Admin</button></h7> &nbsp &nbsp &nbsp &nbsp
    <h7 style="margin-top: 1%;background-color: transparent;margin-left: 0%;text-align: left;color:white">E-Wallet : <button class="btn btn-success btn-xs">404.20938</button></h7> &nbsp &nbsp &nbsp &nbsp
    <h7 style="margin-top: 1%;background-color: transparent;margin-left: 0%;text-align: left;color:white">P-Wallet : <button class="btn btn-success btn-xs">30.409</button></h7>

    <a href="logout.php" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
</div>


<nav class="navbar navbar-inverse visible-xs">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
         <li><a id="dashboard" href="adminPanel.php"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
        <li> <a href="logout.php" class="btn btn-xs btn-danger" style="color:white">Logout</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 col-md-2 col-lg-2  sidenav hidden-xs" style="padding: 0px;background-color: #002233">
      <img src="assets/images/user.png" style="width:45%;margin:5%;margin-left: 25%;">
      <ul class="nav nav-stacked">
         <li><a id="dashboard" href="adminPanel.php"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
      </ul><br>
    </div>
    <br>
    
    <div class="col-sm-9 col-md-10 col-lg-10" style="margin-top: 2%">
      <?php include('register_member.php');?>
    </div>
  </div>
</div>

</body>
</html>
