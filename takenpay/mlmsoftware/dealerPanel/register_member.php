<?php
session_start();
error_reporting(E_ERROR | E_PARSE);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/images/dash/1.png" width="16px">

    <title>Take N Pay</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/signin.css" rel="stylesheet">
        <script src='https://code.jquery.com/jquery-2.1.3.min.js'></script>
        


  </head>

  <body style="background-image: url(assets/images/bg.png);">



      <form class="form-signin" action="register_member.php" method="POST" style="width:100%;border:1px solid gray;border-radius: 5px;background-color: white">
       
        <input type="text" name="fname" class="form-control" style="border-radius: 0px" placeholder="First Name" required autofocus>

        <input type="text" name="lname" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Last Name" required autofocus>
        
        <input type="text" name="fathername" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Father's Name" required autofocus>
        
        <input type="text" name="mothername" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Mother's Name" required autofocus>

        <input type="email" name="email" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Email Address" required>

        <input type="text" name="phone" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Phone Number" required autofocus>

         <select class="form-control" name="district" style="border-radius: 0px;width: 100%;margin-top: 2%">
            <option value="">Select Division</option>
            <option value="Dhaka">Dhaka</option>
            <option value="Chittagong">Chittagong</option>
            <option value="Rajshahi">Rajshahi</option>
            <option value="Khulna">Khulna</option>
            <option value="Barisal">Barisal</option>
            <option value="Sylhet">Sylhet</option>
            <option value="Mymensingh">Mymensingh</option>
          </select> 

        
        <select class="form-control" name="type" style="border-radius: 0px;width: 100%;margin-top: 2%">
            <option value="">Select Member Type</option>
            <option value="General">General</option>
            <option value="Diamond">Diamond</option>
        </select> 


        <input type="text" name="referId" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Referal ID" required>
        
         <input type="text" class="form-control" name="dealerId" id="dealer_code" style="border-radius: 0px;margin-top: 5px" placeholder="Dealer Code" required>
        
        <label style="width: 100%;margin-top: 3%">Select User ID from here </label>
        <select class="form-control" name="username"  id='user_code' style="border-radius: 0px;width: 100%;margin-top: 1%">
        </select>

        <input type="password" name="dealer_password" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Dealer Password" required>

        <input type="password" name="password" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Password" required>

        <input type="password" name="pin" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="PIN" required>
        <!--label> Select Date : </label>//include('date_selector.php'); -->


        <!--label for="inputPassword" class="sr-only">User Type</label>
          
          <select name="type" style="width: 100%">
            <option value="general_manager">General Member</option>
            <option value="dealer">Dealer</option>
            <option value="silver_member">Silver Member</option>
            <option value="golden_member">Golden Member</option>
          </select--> 

        <!--button class="btn btn-md btn-success btn-block" type="submit" style="border-radius: 0px;margin-top: 5px" onclick="window.location.href='dash'">Sign up</button-->
        <button class="btn btn-md btn-success btn-block" type="submit" name="submit" style="border-radius: 0px;margin-top: 5px">Sign up</button>
        <button class="btn btn-md btn-primary btn-block" style="border-radius: 0px;margin-top: 5px" onclick="window.location.href='landingPage.php'">Back</button>
      </form>


    <?php

    include('config.php');

    if(isset($_POST["submit"]))
    {
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $fathername = $_POST['fathername'];
    $mothername = $_POST['mothername'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $district = $_POST['district'];
    $type = $_POST['type'];
    $referId = $_POST['referId'];
    $dealerId = $_POST['dealerId'];
    $dealer_password = $_POST['dealer_password'];
    $username = $_POST['username'];
    $password = $_POST['password'];
    $pin = $_POST['pin'];
    $date = $_POST['date'];
    $joiningDateShow = "";//date("d/m/Y", strtotime($date));
    $joiningDate =date('d-m-Y');// date("Ymd", strtotime($date));
    $expireDate = date('d-m-Y', strtotime('+4 months'));
    $status = "Active";
    
    
    
    $sess_deal_pass = $_SESSION['dealer_pass'];

    if($sess_deal_pass == $dealer_password)
    {
            $query101 ="INSERT INTO credential_registration
                      (fname,lname,fthrname,mthrname,email,phone,district,memberType,userId, referId,dealerId,joiningDateShow,joiningDate,status) 
                VALUES('$fname','$lname','$fathername','$mothername','$email','$phone','$district','$type','$username','$referId','$dealerId' ,'$joiningDateShow','$joiningDate','$status')";
                    $result101 = mysqli_query($dbcon,$query101) or die ('error 101');


                $query102 ="INSERT INTO credential_login
                      (userId,password,pin,joiningDate,expireDate,memberType,status) 
                VALUES('$username','$password','$pin','$joiningDate' ,'$expireDate','$type','$status')";
                    $result102 = mysqli_query($dbcon,$query102) or die ('error 102');

                $query103 ="INSERT INTO `commission_log` (`userId`, `directRfrl`, `globalRfrl`, `thanaDealerCom`, `distDealerCom`, `wardDealerCom`, `masterDealerCom`, `goldenDealerCom`, `silverDealerCom`, `district`, `lastModDate`) VALUES ('$username', '0', '0', '0', '0', '0', '0', '0', '0', '0', '$joiningDate')";
                    $result103 = mysqli_query($dbcon,$query103) or die ('error 103');

                $query104 ="INSERT INTO `member_table_general` (`userId`, `refrId`, `dealerId`, `district`, `memberType`, `status`, `entryDate`) VALUES ('$username', '$referId', '$dealerId', '$district', '$type', 'active', '$joiningDate')";
                    $result104 = mysqli_query($dbcon,$query104) or die ('error 104');


                $query105 ="UPDATE generate_user_for_dealer SET status='taken' WHERE dealerId='$dealerId' and userId='$username'";
                  $result105 = mysqli_query($dbcon,$query105) or die ('error 105');
                    

                if(!$result103){
                  echo "query failed";
                
                }

                else {

                  echo "<script>location='package_wise_money_dist.php'</script>";
                }
    }
    else
    {
            echo '<script>alert("Dealer password does not match record.");</script>';
            echo "<script>location='register_member.php'</script>";
    }

    }

    

    ?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
            <script src='assets/js/fetchUserId.js'></script>
  </body>
</html>
