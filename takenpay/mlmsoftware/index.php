<?php
include('config.php');
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Take N Pay</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Remove the navbar's default margin-bottom and rounded borders */ 
    .navbar {
      margin-bottom: 0;
      border-radius: 0;
    }
    
    /* Add a gray background color and some padding to the footer */
    footer {
      background-color: #f2f2f2;
      padding: 25px;
    }
    
  .carousel-inner img {
      width: 100%; /* Set width to 100% */
      margin: auto;
      min-height:200px;
  }

  /* Hide the carousel text when the screen is less than 600 pixels wide */
  @media (max-width: 600px) {
    .carousel-caption {
      display: none; 
    }
  }
  </style>
</head>
<body>

<nav class="navbar navbar-inverse" style="background-color: #202046">
  <div class="col-md-12" style="background-color: #262640;padding-top:0.3%;padding-bottom:0%;">
  <marquee class="col-md-6 col-sm-12" behavior="" style="color:white">Welcome to our website TakeNpay.org</marquee>
  <p class="col-md-6 col-sm-12" style="color:white;text-align: right;font-size:20px"><span class="glyphicon glyphicon-earphone"></span> +880 1862471494</p>
  </div>
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="" href="#"><img src="assets/images/l2.png" style="width: 73px;margin-top:0px !important"></a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav navbar-right">
        <li><a href="userPanel"><span class="glyphicon glyphicon-log-in"></span> My Account</a></li>
      </ul>
      <ul class="nav navbar-nav navbar-right">
        <li class="active"><a href="#">Home</a></li>
        <li><a href="#">About</a></li>
        <li><a href="#">Contact</a></li>
      </ul>
      
    </div>
  </div>
</nav>

<div id="myCarousel" class="carousel slide" data-ride="carousel">
    <!-- Indicators >
    <ol class="carousel-indicators">
                  <?php  
                  /*$a = 0; 
                  $query = "SELECT image,place FROM home_image WHERE `place` LIKE 'Slider%'"; 
                  $sql = mysqli_query($dbcon,$query); 
                  while($row = mysqli_fetch_array($sql)) 
                  { 
                  $a = $a + 1;
                  echo '<li data-target="#myCarousel" data-slide-to="'.$a.'"></li>';
                  } */
                  ?> 
    </ol>

    < Wrapper for slides -->
    <div class="carousel-inner" role="listbox">

      <?php
      $i = 0;
      $query ="SELECT image,place FROM home_image WHERE `place` LIKE 'Slider%'";
      $sqldata = mysqli_query($dbcon,$query) or die ('error 404');
              while($rows = mysqli_fetch_assoc($sqldata)){
                        $place = $rows['place'];
                        $image = $rows['image'];
                        $image = $place.$image;

                        if($i==0){
                        echo'
                          <div class="item active">
                            <img src="adminPanel/assets/images/Homepage_Image/'.$image.'" alt="Image">   
                          </div>
                        ';
                        $i = $i+1;
                        }else{  
                        echo'
                          <div class="item">
                            <img src="adminPanel/assets/images/Homepage_Image/'.$image.'" alt="Image">    
                          </div>
                        ';
                        }
                        
                      }
      ?>
      

      <!--div class="item">
        <img src="https://placehold.it/1200x400?text=Another Image Maybe" alt="Image">
        <div class="carousel-caption">
          <h3>More Sell $</h3>
          <p>Lorem ipsum...</p>
        </div>      
      </div-->

    <!-- Left and right controls -->
    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
      <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
      <span class="sr-only">Previous</span>
    </a>
    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
      <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
      <span class="sr-only">Next</span>
    </a>
</div>
</div>
  
<div class="container text-center">    
  <h3 style="text-align:left;border-bottom: 2px solid black">Our Products</h3><br>
  <div class="row">
    <?php
      $query ="SELECT image,place FROM home_image WHERE `place` LIKE 'Product%'";
      $sqldata = mysqli_query($dbcon,$query) or die ('error 404');
              while($rows = mysqli_fetch_assoc($sqldata)){
                        $title = $rows['place'];
                        $image = $rows['image'];
                        $image = $title.$image;
    echo '<div class="col-sm-4" style="margin-top: 1%">
      <img src="adminPanel/assets/images/Homepage_Image/'.$image.'" class="img-responsive" style="width:100%" alt="Image">
    </div>';
    }
    ?>

  </div>
</div><br>

<footer class="container-fluid text-center">
  <p>&copy TakeNpaY.org</p>
</footer>

</body>
</html>
