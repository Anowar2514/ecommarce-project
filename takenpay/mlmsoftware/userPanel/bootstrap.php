<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 550px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #000000;
      height: 111%;
    }

    .sidenav a{
      background-color: #00334d;
      color: #ffffff;
      cursor: pointer;
      border-left: 4px solid #00ccff;
      margin-bottom: 5%;
    }
    ul li:hover a {
      color: #000000;
    }

    /* On small screens, set height to 'auto' for the grid */
    @media screen and (max-width: 767px) {
      .row.content {height: auto;} 
    }
  </style>
</head>
<body style=" background-color: #000000;">


<div class="col-md-12 col-lg-12  hidden-xs hidden-sm" style="background-color: #001a1a">
<img src="assets/images/only_logo.png" style="width:7%;margin-left: 5%;margin-right: 5%;">

<h7 style="margin-top: 1%;background-color: transparent;margin-left: 0%;text-align: left;color:white">Account Type : <button class="btn btn-info btn-xs">Global</button></h7> &nbsp &nbsp &nbsp &nbsp
    <h7 style="margin-top: 1%;background-color: transparent;margin-left: 0%;text-align: left;color:white">E-Wallet : <button class="btn btn-success btn-xs">404.20938</button></h7> &nbsp &nbsp &nbsp &nbsp
    <h7 style="margin-top: 1%;background-color: transparent;margin-left: 0%;text-align: left;color:white">P-Wallet : <button class="btn btn-success btn-xs">30.409</button></h7>
</div>


<nav class="navbar navbar-inverse visible-xs">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li class="active"><a href="#">Dashboard</a></li>
        <li><a href="#">Age</a></li>
        <li><a href="#">Gender</a></li>
        <!--li class="dropdown">
          <a class="dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Tutorials
              <span class="caret"></span></a>
              <ul class="dropdown-menu" role="menu" aria-labelledby="menu1">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="bootstrap.php">HTML</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="bootstrap.php">CSS</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="bootstrap.php">JavaScript</a></li>
                <li role="presentation" class="divider"></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="bootstrap.php">About Us</a></li>
              </ul>
        </li-->
        <li role="presentation"><a role="menuitem" tabindex="-1" href="bootstrap.php">HTML</a></li>
        <li role="presentation"><a role="menuitem" tabindex="-1" href="bootstrap.php">CSS</a></li>
        <li role="presentation"><a role="menuitem" tabindex="-1" href="bootstrap.php">JavaScript</a></li>
        <li role="presentation"><a role="menuitem" tabindex="-1" href="bootstrap.php">About Us</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 col-md-2 col-lg-2  sidenav hidden-xs" style="padding: 0px;background-color: #002233">
      <img src="assets/images/user.png" style="width:45%;margin:5%;margin-left: 25%;">
      <ul class="nav nav-stacked">
        <li class="active"><a href="bootstrap.php">Dashboard</a></li>
        <li><a href="bootstrap.php">Age</a></li>
        <!--li class="dropdown">
          <a class="dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Tutorials
              <span class="caret"></span></a>
              <ul class="dropdown-menu sidenav" role="menu" aria-labelledby="menu1" style="">
                <li role="presentation"><a role="menuitem" tabindex="-1" href="bootstrap.php">HTML</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="bootstrap.php">CSS</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="bootstrap.php">JavaScript</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1" href="bootstrap.php">About Us</a></li>
              </ul>
        </li-->
        <li role="presentation"><a role="menuitem" tabindex="-1" href="bootstrap.php">HTML</a></li>
        <li role="presentation"><a role="menuitem" tabindex="-1" href="bootstrap.php">CSS</a></li>
        <li role="presentation"><a role="menuitem" tabindex="-1" href="bootstrap.php">JavaScript</a></li>
        <li role="presentation"><a role="menuitem" tabindex="-1" href="bootstrap.php">About Us</a></li>
        <li><a href="bootstrap.php">Gender</a></li>
      </ul><br>
    </div>
    <br>
    
    <div class="col-sm-9 col-md-10 col-lg-10 ">
      <div class="row">
        <div class="col-sm-3">
          <div class="well">
            <h4>Users</h4>
            <p>1 Million</p> 
          </div>
        </div>
        <div class="col-sm-3">
          <div class="well">
            <h4>Pages</h4>
            <p>100 Million</p> 
          </div>
        </div>
        <div class="col-sm-3">
          <div class="well">
            <h4>Sessions</h4>
            <p>10 Million</p> 
          </div>
        </div>
        <div class="col-sm-3">
          <div class="well">
            <h4>Bounce</h4>
            <p>30%</p> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-4">
          <div class="well">
            <p>Text</p> 
            <p>Text</p> 
            <p>Text</p> 
          </div>
        </div>
        <div class="col-sm-4">
          <div class="well">
            <p>Text</p> 
            <p>Text</p> 
            <p>Text</p> 
          </div>
        </div>
        <div class="col-sm-4">
          <div class="well">
            <p>Text</p> 
            <p>Text</p> 
            <p>Text</p> 
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-8">
          <div class="well">
            <p>Text</p> 
          </div>
        </div>
        <div class="col-sm-4">
          <div class="well">
            <p>Text</p> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
