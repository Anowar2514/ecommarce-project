<?php
session_start();
error_reporting(E_ERROR | E_PARSE);
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="assets/images/dash/1.png" width="16px">

    <title>Take N Pay</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/signin.css" rel="stylesheet">
    
        

  <script src="https://code.jquery.com/jquery-1.12.4.js"></script>
  <script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>

  </head>

  <body style="background-image: url(assets/images/bg.png);">

    <div class="container">

      <form class="form-signin" action="register_member.php" method="POST" style="width:100%;border:1px solid gray;border-radius: 5px;background-color: white">
        <img class="ing-circle" style="width: 100%;margin-bottom: 1%" src="assets/images/l2.png">
       <h2 style="width: 100%;color:  #08883b;text-align: center;">Create General Account</h2>
       
        <input type="text" name="fname" class="form-control" style="border-radius: 0px" placeholder="First Name"  autofocus>

        <input type="text" name="lname" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Last Name"  autofocus>
        
        <input type="text" name="fathername" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Father's Name"  autofocus>
        
        <input type="text" name="mothername" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Mother's Name"  autofocus>

        <input type="email" name="email" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Email Address" >

        <input type="text" name="phone" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Phone Number"  autofocus>

         <select class="form-control" name="district" style="border-radius: 0px;width: 100%;margin-top: 2%">
            <option value="">Select Division</option>
            <option value="DHA">Dhaka</option>
            <option value="CTG">Chittagong</option>
            <option value="RAJ">Rajshahi</option>
            <option value="KHU">Khulna</option>
            <option value="BAR">Barisal</option>
            <option value="SYL">Sylhet</option>
            <option value="MYM">Mymensingh</option>
          </select> 

        <input type="hidden" name="type" class="form-control" style="border-radius: 0px;margin-top: 5px" value="general">

        <!--select name="type" style="width: 100%">
            <option value="general">General Member</option>
        </select-->
        


        <input type="text" name="referId" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Referal ID" >
        
        <input type="text" class="form-control" name="dist_dealerId" id="dist_dealerId" style="border-radius: 0px;margin-top: 5px" placeholder="District Dealer Code">
        <input type="text" class="form-control" name="thana_dealerId" id="thana_dealerId" style="border-radius: 0px;margin-top: 5px" placeholder="Thana Dealer Code">
        <input type="text" class="form-control" name="ward_dealerId" id="ward_dealerId" style="border-radius: 0px;margin-top: 5px" placeholder="Ward Dealer Code">
        
        <!--label style="width: 100%;margin-top: 3%">Select User ID from here </label-->
        <!--input class="form-control" name="username"  id="user_code" style="border-radius: 0px;width: 100%;margin-top: 1%" placeholder="User ID"-->

        <input type="password" name="password" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Password" >

        <input type="password" name="pin" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="PIN" >
        <!--label> Select Date : </label>//include('date_selector.php'); -->


        <!--label for="inputPassword" class="sr-only">User Type</label>
          
          <select name="type" style="width: 100%">
            <option value="general_manager">General Member</option>
            <option value="dealer">Dealer</option>
            <option value="silver_member">Silver Member</option>
            <option value="golden_member">Golden Member</option>
          </select--> 

        <!--button class="btn btn-md btn-success btn-block" type="submit" style="border-radius: 0px;margin-top: 5px" onclick="window.location.href='dash'">Sign up</button-->
        <button class="btn btn-md btn-success btn-block" type="submit" name="submit" style="border-radius: 0px;margin-top: 5px">Sign up</button>
        <a class="btn btn-md btn-primary btn-block" style="border-radius: 0px;margin-top: 5px"  href="landingPage.php">Back</a>
      </form>

    </div> <!-- /container -->

    <?php

    include('config.php');

    if(isset($_POST["submit"]))
    {
    $fname = $_POST['fname'];
    $lname = $_POST['lname'];
    $fathername = $_POST['fathername'];
    $mothername = $_POST['mothername'];
    $email = $_POST['email'];
    $phone = $_POST['phone'];
    $district = $_POST['district'];
    $type = $_POST['type'];
    $referId = $_POST['referId'];
    $district_dealerId = $_POST['dist_dealerId'];
    $thana_dealerId = $_POST['thana_dealerId'];
    $ward_dealerId = $_POST['ward_dealerId'];
    
    $password = $_POST['password'];
    $pin = $_POST['pin'];
    $joiningDateShow = "";//date("d/m/Y", strtotime($date));
    $joiningDate =date('d-m-Y');// date("Ymd", strtotime($date));
    $expireDate = date('d-m-Y', strtotime('+4 months'));
    $status = "Inactive";

    $confirmation_status = "Pending";
    
    include('fetchUserId.php');
     $your_id;
    $userid  = $your_id + 1;
    $n2 = str_pad($userid, 8, 0, STR_PAD_LEFT);
     $genId = $district.$n2;  

     $_SESSION['genId'] = $genId;


           $query101 ="INSERT INTO credential_registration
                      (`fname`, `lname`, `fthrName`, `mthrName`, `email`, `phone`, `district`, `memberType`, `member_level_general`, `member_level_silver`, `member_level_gold`, `userId`, `referId`, `district_dealerId`, `thana_dealerId`, `ward_dealerId`, `joiningDateShow`, `joiningDate`, `status`, `activation_status`, `activatedBy`) 
                      VALUES ('$fname', '$lname', '$fathername', '$mothername', '$email', '$phone', '$district', '$type', '0', '0', '0', '$genId', '$referId', '$district_dealerId', '$thana_dealerId', '$ward_dealerId', '$joiningDateShow', '$joiningDate', '$status', '$confirmation_status', '')";
            $result101 = mysqli_query($dbcon,$query101) or die ('error 101');   


                $query102 ="INSERT INTO credential_login
                      (userId,password,pin,joiningDate,expireDate,memberType,status) 
                VALUES('$genId','$password','$pin','$joiningDate' ,'$expireDate','$type','$status')";
                    $result102 = mysqli_query($dbcon,$query102) or die ('error 102');

                $query103 ="INSERT INTO `commission_log` (`userId`, `directRfrl`, `allGeneralComm`, `allSilverComm`, `allGoldComm`, `allDiamondComm`, `allDealerComm`, `specificDealerComm`, `generationComm`, `district`, `lastModDate`) VALUES ('$genId', '0', '0', '0', '0', '0', '0', '0', '0', '0', '$joiningDate')";
                    $result103 = mysqli_query($dbcon,$query103) or die ('error 103');

                $query104 ="INSERT INTO `member_table_general` (`userId`, `refrId`, `dealerId`, `district`, `memberType`, `status`, `entryDate`) VALUES ('$genId', '$referId', '$dealerId', '$district', '$type', 'Inactive', '$joiningDate')";
                    $result104 = mysqli_query($dbcon,$query104) or die ('error 104');

                    /*
                $query105 ="UPDATE generate_user_for_dealer SET status='taken' WHERE dealerId='$dealerId' and userId='$username'";
                  $result105 = mysqli_query($dbcon,$query105) or die ('error 105');
                    */

                if(!$result104){
                  echo "query failed";
                
                }

                else {
                 $_SESSION['genId'] = $genId;
                 echo "<script>location='login.php'</script>";
                }
    }
   

    ?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
  </body>
</html>
