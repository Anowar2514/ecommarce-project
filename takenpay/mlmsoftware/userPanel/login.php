<?php
session_start();
error_reporting(E_ERROR | E_PARSE);
?>

<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="favicon.ico">

    <title>Take N Pay</title>

    <!-- Bootstrap core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="assets/css/signin.css" rel="stylesheet">
  </head>

  <body style="background-image: url(assets/images/bg.png);">

    <div class="container">

      <form class="form-signin" action="login.php" method="POST" style="width:100%;border:1px solid gray;border-radius: 5px;background-color: white">
        <img style="width: 100%" src="assets/images/l2.png">

        <?php
        if(isset($_SESSION['genId']))
          {
            $genId = $_SESSION['genId'];
            echo "<div class='alert alert-success'>
            <a href='logout.php' class='close' data-dismiss='alert' aria-label='close'>&times;</a>
            <strong>Congratulations!</strong> Your Id is : ".$genId.".
            </div>";
          };
          ?>

        <h2 style="width: 100%;color: #0a51a1;text-align: center;">MEMBER LOGIN</h2>
        <label class="sr-only">Username</label>
        <input type="text" name="username" class="form-control" style="border-radius: 5px;margin-top: 15%" placeholder="Username" required autofocus>
        <label class="sr-only">Password</label>
        <input type="password" name="password" class="form-control" style="border-radius: 5px;margin-top: 3%" placeholder="Password" required>


        <!--label for="inputPassword" class="sr-only">User Type</label>
          
          <select name="type" style="width: 100%">
            <option value="general_manager">General Member</option>
            <option value="dealer">Dealer</option>
            <option value="silver_member">Silver Member</option>
            <option value="golden_member">Golden Member</option>
          </select> 

        <div class="checkbox">
          <label>
            <input type="checkbox" value="remember-me"> Remember me
          </label>
        </div-->

        <button class="btn btn-md btn-success btn-block" type="submit" name="submit" style="border-radius: 0px;margin-top: 15%;margin-bottom: 1%">Login</button>
        <a class="btn btn-md btn-primary btn-block" style="border-radius: 0px;margin-top: 5px"  href="landingPage.php">Back</a>
      </form>
      
    </div> <!-- /container -->
    <div class="container" style="text-align: center;">
    <h7 style="text-align: center;">Don't have any account ? <a href="register_member.php">Create an account.</a></h7><br>
    <h7 style="text-align: center;">Back to <a href="../">TakeNpaY.org</a></h7>
    </div>

    <?php

    include ('config.php');

    if(isset($_POST["submit"]))
          {                  // checking if the query runs or not 
          
          //echo "button clicked";
          //global $connection;
         
          $username = $_POST['username'];
          $password = $_POST['password'];
          // PREVIOUS LOGIN LOGIC
          /*$query = "SELECT * FROM credential_login WHERE userId = '$username' AND password = '$password' AND status='Active'";
          $result = mysqli_query($dbcon,$query) or die ('error');*/
          
          //NEW LOGIC
          $queryACT_STS = "SELECT activation_status FROM credential_registration WHERE userId = '$username'";
          $resultACT_STS = mysqli_query($dbcon,$queryACT_STS) or die ('error');
          while($rowsACT_STS = mysqli_fetch_assoc($resultACT_STS)){
          
            $activation_status = $rowsACT_STS['activation_status'];
          }

          if($activation_status != 'Pending'){
          $query = "SELECT * FROM credential_login WHERE userId = '$username' AND password = '$password' AND status='Active'";
          $result = mysqli_query($dbcon,$query) or die ('error');
          }
          else{
          $query = "SELECT * FROM credential_login WHERE userId = '$username' AND password = '$password'";
          $result = mysqli_query($dbcon,$query) or die ('error');
          }
          
          
          if(!$result){
            echo "query failed";
          
          }
          
          while($rows = mysqli_fetch_assoc($result)){
          
            $uname = $rows['userId'];
            $pass = $rows['password'];
            }
          
          if($username == $uname && $password == $pass){
            
            //$new_location = "index.php";
            
             
              $_SESSION['user'] = $uname;
              $_SESSION['pass'] = $pass;
                //$_SESSION['user_id'] = $uid;
               echo "<script>location='pinConfirm.php'</script>";
               //echo $uname."+".$pass ;
                                  exit();
              
              
           }
          
          else{
              $_SESSION['user'] = $_POST['username'];
              $_SESSION['pass'] = $_POST['password'];
            echo "<script>location='../adminPanel/login.php'</script>";
                        
          }
          }

    ?>

    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <script src="assets/js/ie10-viewport-bug-workaround.js"></script>
  </body>
</html>
