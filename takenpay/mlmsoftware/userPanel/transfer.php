<?php
session_start();
error_reporting(E_ERROR | E_PARSE);
?>

<?php
if(!isset($_SESSION['user']))
    {
        echo "<script>location='landingPage.php'</script>";
    }

?>

  <?php include('sidepanel.php');?> 

  <div class="col-sm-9 col-md-10 col-lg-10" style="margin-top: 1%">
    <div class="row">
        <div class="col-sm-6">
          <div class="well">
            <h4>Money Transfer</h4>
            <form class="form-signin" action="transfer.php" method="POST" style="width:100%;background-color: white">
				
				<?php echo'
				<input type="hidden" name="sender" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Sender ID" value="'.$_SESSION['user'].'">';
				?>

				<h5 style="color: red">**Receiver :</h5>
				<input type="text" name="receiver" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Receiver ID">
        <input type="checkbox" name="receiver_com" value="company">Company</input>


				<h5 style="color: red">**Amount :</h5>
				<input type="text" name="amount" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Amount" required> 
        

				<?php 
    			$dateToday = date('d-m-Y');
    			echo'
				<input type="hidden" name="today" class="form-control" style="border-radius: 0px;margin-top: 5px" placeholder="Sender ID" value="'.$dateToday.'">';
				?>

				<button class="btn btn-md btn-success btn-block" type="submit" name="submit" style="border-radius: 0px;margin-top: 15px"><span class="glyphicon glyphicon-share"></span>Transfer Money</button>

<p style="margin-top: 3%;text-align: justify;">
	
    <table class="table table-bordered">
                <thead>
                 <p class="btn btn-md btn-danger btn-block" style="border-radius: 0px">Transection Costs</p>
                </thead>
                <tbody>
                  <tr class="danger">
                    <th>Receiver Type</th>
                    <th style="text-align: center;">Cost</th>
                    <th>Will be added to</th>
                  </tr>
                
                
                  <tr class="info">
                    <th>Member : General/ Silver/ Gold/ Diamond</th>
                    <th>10 Tk per transection</th>
                    <th>Admin Account</th>
                  </tr>
                  <tr class="info">
                    <th>Dealer</th>
                    <th>15 Tk per transection</th>
                    <th style="color:red">**10 Tk to the admin <br> **5 Tk to the dealer account</th>
                  </tr>
                  <tr class="info">
                    <th>Company</th>
                    <th>10% of total amount</th>
                    <th>Admin Account</th>
                  </tr>
              </tbody>
    </table>
</p>
</form>
<?php
	include('config.php');

    if(isset($_POST["submit"]) && (isset($_POST["receiver_com"]) || $_POST["receiver"] != "") && $_POST['amount'] <= $total70)
    {
    $sender = $_POST['sender'];
    $receiver = $_POST['receiver'];
    $amount = $_POST['amount'];
    $today = $_POST['today'];

    
    if($receiver != ""){
    $query_get_type = "SELECT memberType FROM credential_registration WHERE userId = '$receiver'";
            $result_get_type = mysqli_query($dbcon,$query_get_type) or die ('error');
            while($rows_get_type = mysqli_fetch_assoc($result_get_type)){
          
            $memberType_member = $rows_get_type['memberType'];
            
            /*check if dealer*/
            $query_get_dealer = "SELECT COUNT(id) as memberType FROM member_table_dealer WHERE dealerName = '$receiver'";
            $result_get_dealer = mysqli_query($dbcon,$query_get_dealer) or die ('error');
            while($rows_get_dealer = mysqli_fetch_assoc($result_get_dealer)){
            $memberType_dealer = $rows_get_dealer['memberType'];
                
            }
            
            if($memberType_dealer > 0)
            {
               $memberType = "Dealer";
            }
            else
            {
                $memberType = $memberType_member;
            }
            /*check if dealer*/
        }
    

    if($memberType == "general" || $memberType == "silver" || $memberType == "gold" || $memberType == "diamond")
    {
      $amount = $_POST['amount']-10;
      $company_comm = 10;

          $query101 ="INSERT INTO `money_transfer`
          (`sender`, `receiver`, `amount`,`receiverType`, `company_comm`, `entryDate`)
          VALUES
          ('$sender','$receiver','$amount','$memberType','$company_comm','$today')";
              $result101= mysqli_query($dbcon,$query101);

          if(!$result101){
            echo "query failed";
          
          }

          else {


          //company profit on transection
          $action = "moneyTrans";
          $total_profit = 10;
          $query_company_income ="INSERT INTO `commission_admin_log` (`total_income`,`total_distribution`, `total_incentive`, `total_profit`,`total_capital`,`action`,`entryDate`) 
          VALUES ('0','0','0','$total_profit','0','$action','$dateToday')";
          $result_company_income = mysqli_query($dbcon,$query_company_income) or die ('error_company_income');
          //company profit on transection


            echo'<script>alert("Transection Successful.")</script>';
            echo "<script>location='transfer.php'</script>";
          }
    }

    elseif($memberType == "Dealer")
    {
      
      $company_comm = 10;
      $dealer_comm = 5;
      $total_comm = $company_comm + $dealer_comm;

      $grossAmount = $_POST['amount']-$total_comm;

          $query101 ="INSERT INTO `money_transfer`
          (`sender`, `receiver`, `amount`,`receiverType`, `company_comm`, `entryDate`)
          VALUES
          ('$sender','$receiver','$grossAmount','$memberType','$company_comm','$today')";
              $result101= mysqli_query($dbcon,$query101);


          if(!$result101){
            echo "query failed";
          
          }

          else {
          $query102 ="INSERT INTO `money_transfer`
          (`sender`, `receiver`, `amount`,`receiverType`, `company_comm`, `entryDate`)
          VALUES
          ('$sender','$receiver','$dealer_comm','$memberType','0','$today')";
              $result102= mysqli_query($dbcon,$query102);

          if(!$result102){
            echo "query failed";
          
          }

          else {

          //company profit on transection
          $action = "moneyTrans";
          $total_profit = 10;
          $query_company_income ="INSERT INTO `commission_admin_log` (`total_income`,`total_distribution`, `total_incentive`, `total_profit`,`total_capital`,`action`,`entryDate`) 
          VALUES ('0','0','0','$total_profit','0','$action','$dateToday')";
          $result_company_income = mysqli_query($dbcon,$query_company_income) or die ('error_company_income');
          //company profit on transection
            


            echo'<script>alert("Transection Successful.")</script>';
            echo "<script>location='transfer.php'</script>";
          }
          }
      
    }
    }
    else
    {
      $company_comm = $_POST['amount']*0.1;

      $grossAmount = $_POST['amount']-$company_comm;

          $query101 ="INSERT INTO `money_transfer`
          (`sender`, `receiver`, `amount`,`receiverType`, `company_comm`, `entryDate`)
          VALUES
          ('$sender','Company','$grossAmount','Company','$company_comm','$today')";
              $result101= mysqli_query($dbcon,$query101);


          if(!$result101){
            echo "query failed";
          
          }

          else {

          //company profit on transection
          $action = "moneyTrans";
          $total_profit = $company_comm;
          $query_company_income ="INSERT INTO `commission_admin_log` (`total_income`,`total_distribution`, `total_incentive`, `total_profit`,`total_capital`,`action`,`entryDate`) 
          VALUES ('0','0','0','$total_profit','0','$action','$dateToday')";
          $result_company_income = mysqli_query($dbcon,$query_company_income) or die ('error_company_income');
          //company profit on transection

          //transection VIA company 
          $total_profit = $grossAmount;
          $query_company_income ="INSERT INTO `admin_receive_pay` (`userId`, `amount`, `date`, `paidBy`, `paymentDate`) 
          VALUES ('$sender', '$total_profit', '$dateToday', '', '');";
          $result_company_income = mysqli_query($dbcon,$query_company_income) or die ('error_company_trans');
          //transection VIA company 

          
            echo'<script>alert("Transection Successful.")</script>';
            echo "<script>location='transfer.php'</script>";
          }
      
    }



	
    /*$query102 ="INSERT INTO credential_login
          (userId,password,pin,joiningDate,expireDate,memberType,status) 
    VALUES('$username','$password','$pin','$joiningDate' ,'$joiningDate','$type','$status')";
        $result102 = mysqli_query($dbcon,$query102) or die ('error 102');


    $query103 ="UPDATE generate_user_for_dealer SET status='taken' WHERE dealerId='$dealerId' and userId='$username'";
      $result103 = mysqli_query($dbcon,$query103) or die ('error 103');
        
	*/

}
?>
          </div>
        </div>
    </div>
   
   

  


  </div>
</div><?php include('footer.php');?>
</div>




