<?php
session_start();
error_reporting(E_ERROR | E_PARSE);
?>

<?php
if(!isset($_SESSION['user']))
    {
        echo "<script>location='landingPage.php'</script>";
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title></title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link rel="icon" href="assets/images/dash/1.png" width="16px">
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 550px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #000000;
      height: 111%;
    }

    .sidenav a{
      background-color: #00334d;
      color: #ffffff;
      cursor: pointer;
      border-left: 4px solid #00ccff;
      margin-bottom: 3%;
    }
    ul li:hover a {
      color: #000000;
    }

    /* On small screens, set height to 'auto' for the grid */
    @media screen and (max-width: 767px) {
      .row.content {height: auto;} 
    }
  </style>
</head>
<body style=" background-color: #000000;">


<div class="col-md-12 col-lg-12  hidden-xs hidden-sm" style="background-color: #001a1a">
<img src="assets/images/l2.png" style="width:8%;margin-left: 2%;margin-right: 4%;">


                  <?php
                  include('config.php');
                  $user = $_SESSION['user'];
                  

                  //Withdrawal
                  $query_withdraw = "SELECT SUM(`amount`) as sumAmount,SUM(`company_comm`) as sumCommission  FROM `money_transfer` WHERE  sender = '$user'";
                  $result_withdraw = mysqli_query($dbcon,$query_withdraw) or die ('error1');
                                  while($rows_withdraw = mysqli_fetch_assoc($result_withdraw)){
                                    $total_withdrawal = number_format($rows_withdraw['sumAmount'] + $rows_withdraw['sumCommission'], 2, '.', '');
                                    
                                  }
                  //received from other 
                  $query_receive = "SELECT SUM(`amount`) as sumAmount FROM `money_transfer` WHERE  receiver = '$user'";
                  $result_receive = mysqli_query($dbcon,$query_receive) or die ('error2');
                                  while($rows_receive = mysqli_fetch_assoc($result_receive)){
                                    $total_receive = $rows_receive['sumAmount'] + $rows_receive['sumCommission'];
                                    
                                  }







                  $query_tbl_info = "SELECT memberType FROM credential_registration WHERE userId = '$user'";
                  $result_tbl_info = mysqli_query($dbcon,$query_tbl_info) or die ('error201');
                  while($rows_tbl_info = mysqli_fetch_assoc($result_tbl_info))
                  {
                  $memberType = $rows_tbl_info['memberType'];
                  $memberType = strtoupper($memberType);
                  $_SESSION['memType'] = $memberType;
                  $query_income = "SELECT SUM(`directRfrl`) as directRfrl, SUM(`allGeneralComm`) as allGeneralComm, SUM(`allSilverComm`) as allSilverComm, SUM(`allGoldComm`) as allGoldComm, SUM(`allDiamondComm`) as allDiamondComm, SUM(`allDealerComm`) as allDealerComm, SUM(`specificDealerComm`) as specificDealerComm, SUM(`generationComm`) as generationComm FROM commission_log WHERE userId = '$user'";
                        $result_income = mysqli_query($dbcon,$query_income) or die ('error201');
                        while($rows_income = mysqli_fetch_assoc($result_income))
                        {
                        $total_income = number_format($rows_income['directRfrl'] + $rows_income['allGeneralComm'] + $rows_income['allSilverComm'] + 
                        $rows_income['allGoldComm'] + $rows_income['allDiamondComm'] + $rows_income['allDealerComm'] + $rows_income['specificDealerComm'] + $rows_income['generationComm'] , 2, '.', '');

                        
                        }

                  //SELECT SUM(`amount`) as sumAmount,SUM(`company_comm`) as sumCommission  FROM `money_transfer` WHERE `id` = 
                        $total_income = number_format($total_income + $total_receive , 2, '.', '');
                        $total70 = $total_income * 0.7;
                        $total30 = $total_income * 0.3;

                        $total70 = number_format($total70 - $total_withdrawal, 2, '.', '');


                  echo'
                  <h7 style="margin-top: 1%;background-color: transparent;margin-left: 0%;text-align: left;color:white">Member Type : <button class="btn btn-info btn-xs">'.$memberType.'</button></h7> &nbsp &nbsp &nbsp &nbsp
                  <h7 style="margin-top: 1%;background-color: transparent;margin-left: 0%;text-align: left;color:white">All Commission : <button class="btn btn-success btn-xs">'.$total_income.'</button></h7> &nbsp &nbsp &nbsp &nbsp
                  <h7 style="margin-top: 1%;background-color: transparent;margin-left: 0%;text-align: left;color:white">Withdrawn Amount : <button class="btn btn-success btn-xs">'.$total_withdrawal.'</button></h7> &nbsp &nbsp &nbsp &nbsp
                  <h7 style="margin-top: 1%;background-color: transparent;margin-left: 0%;text-align: left;color:white">E-Wallet : <button class="btn btn-success btn-xs">'.$total70.'</button></h7> &nbsp &nbsp &nbsp &nbsp
                  <h7 style="margin-top: 1%;background-color: transparent;margin-left: 0%;text-align: left;color:white">P-Wallet : <button class="btn btn-success btn-xs">'.$total30.'</button></h7>';
                  }
                  
                  ?>







    <a href="logout.php" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
</div>


<nav class="navbar navbar-inverse visible-xs visible-sm">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
<img src="assets/images/l2.png" style="width:50%;margin-left: 2%;margin-right: 4%;">
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
        <li><a id="dashboard" href="index.php"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
        <li> <a id="profile" href="userProfile.php#menu1"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
        <li><a id="package" href="packages.php"><span class="glyphicon glyphicon-shopping-cart"></span> Package List</a></li>
        <!--li class="dropdown">
          <a class="dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Tutorials
              <span class="caret"></span></a>
              <ul class="dropdown-menu sidenav" role="menu" aria-labelledby="menu1" style="">
                <li role="presentation"><a role="menuitem" tabindex="-1">HTML</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1">CSS</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1">JavaScript</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1">About Us</a></li>
              </ul>
        </li-->
        <li> <a id="transfer" href="transfer.php"><span class="glyphicon glyphicon-random"></span> Transfer</a></li>
        <li> <a id="send_history" href="send_history.php"><span class="glyphicon glyphicon-align-justify"></span> Send History</a></li>
        <li> <a id="receive_history" href="receive_history.php"><span class="glyphicon glyphicon-align-justify"></span> Receive History</a></li>
        <li> <a id="comm_history" href="commission_history.php"><span class="glyphicon glyphicon-align-justify"></span> Commission History</a></li>
        <li> <a id="bank_withdraw" href="withdrawal.php"><span class="glyphicon glyphicon-download"></span> Bank Withdraw</a></li>
        <li> <a id="bank_withdraw_history" href="bank_withdraw_history.php"><span class="glyphicon glyphicon-align-justify"></span> Withdraw History</a></li>
        <li> <a id="bank_withdraw_history" href="genealogy.php"><span class="glyphicon glyphicon-globe"></span> Genealogy</a></li>
        <?php 
        $user = $_SESSION['user'];
        
        /*check if dealer*/
            $query_get_dealer = "SELECT COUNT(id) as memberTypeD FROM member_table_dealer WHERE dealerName = '$user'";
            $result_get_dealer = mysqli_query($dbcon,$query_get_dealer) or die ('error');
            while($rows_get_dealer = mysqli_fetch_assoc($result_get_dealer)){
            $memberTypeD = $rows_get_dealer['memberTypeD'];
                
            }
            
            if($memberTypeD > 0)
            {
                echo '<li> <a id="bank_withdraw_history" href="open_member_dealer.php"><span class="glyphicon glyphicon-lock"></span> Account Open</a></li>';
                
            }
            /*check if dealer*/
        ?>
        <li> <a href="logout.php" class="btn btn-xs btn-danger" style="color:white">Logout</a></li>
      </ul>
    </div>
  </div>
</nav>

<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 col-md-2 col-lg-2  sidenav hidden-xs" style="padding: 0px;background-color: #002233">
      <img src="assets/images/user.png" style="width:50%;margin:5%;margin-left: 25%;">
      <ul class="nav nav-stacked">
        <li><a id="dashboard" href="index.php"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
        <li> <a id="profile" href="userProfile.php"><span class="glyphicon glyphicon-user"></span> Profile</a></li>
        <li><a id="package" href="packages.php"><span class="glyphicon glyphicon-shopping-cart"></span> Package List</a></li>
        <!--li class="dropdown">
          <a class="dropdown-toggle" type="button" id="menu1" data-toggle="dropdown">Tutorials
              <span class="caret"></span></a>
              <ul class="dropdown-menu sidenav" role="menu" aria-labelledby="menu1" style="">
                <li role="presentation"><a role="menuitem" tabindex="-1">HTML</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1">CSS</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1">JavaScript</a></li>
                <li role="presentation"><a role="menuitem" tabindex="-1">About Us</a></li>
              </ul>
        </li-->
        <li> <a id="transfer" href="transfer.php"><span class="glyphicon glyphicon-random"></span> Transfer</a></li>
        <li> <a id="send_history" href="send_history.php"><span class="glyphicon glyphicon-align-justify"></span> Send History</a></li>
        <li> <a id="receive_history" href="receive_history.php"><span class="glyphicon glyphicon-align-justify"></span> Receive History</a></li>
        <li> <a id="comm_history" href="commission_history.php"><span class="glyphicon glyphicon-align-justify"></span> Commission History</a></li>
        <li> <a id="bank_withdraw" href="under_cons.php"><span class="glyphicon glyphicon-download"></span> Bank Withdraw</a></li>
        <li> <a id="bank_withdraw_history" href="under_cons.php"><span class="glyphicon glyphicon-align-justify"></span> Withdraw History</a>
        <li> <a id="bank_withdraw_history" href="genealogy.php"><span class="glyphicon glyphicon-globe"></span> Genealogy</a></li>
        <?php 
        $user = $_SESSION['user'];
        
        /*check if dealer*/
            $query_get_dealer = "SELECT COUNT(id) as memberTypeD FROM member_table_dealer WHERE dealerName = '$user'";
            $result_get_dealer = mysqli_query($dbcon,$query_get_dealer) or die ('error');
            while($rows_get_dealer = mysqli_fetch_assoc($result_get_dealer)){
            $memberTypeD = $rows_get_dealer['memberTypeD'];
                
            }
            
            if($memberTypeD > 0)
            {
                echo '<li> <a id="bank_withdraw_history" href="open_member_dealer.php"><span class="glyphicon glyphicon-lock"></span> Account Open</a></li>';
                $_SESSION['dealer'] = 'true';
                
            }
            /*check if dealer*/
        ?>
      </ul>
    </div>

    
<div class="well hidden-lg hidden-md" style="background-color: #001a1a;margin-bottom: 15%">
<?php
                  $user = $_SESSION['user'];
                  

                  //Withdrawal
                  $query_withdraw = "SELECT SUM(`amount`) as sumAmount,SUM(`company_comm`) as sumCommission  FROM `money_transfer` WHERE  sender = '$user'";
                  $result_withdraw = mysqli_query($dbcon,$query_withdraw) or die ('error1');
                                  while($rows_withdraw = mysqli_fetch_assoc($result_withdraw)){
                                    $total_withdrawal = number_format($rows_withdraw['sumAmount'] + $rows_withdraw['sumCommission'], 2, '.', '');
                                    
                                  }
                  //received from other 
                  $query_receive = "SELECT SUM(`amount`) as sumAmount FROM `money_transfer` WHERE  receiver = '$user'";
                  $result_receive = mysqli_query($dbcon,$query_receive) or die ('error2');
                                  while($rows_receive = mysqli_fetch_assoc($result_receive)){
                                    $total_receive = $rows_receive['sumAmount'] + $rows_receive['sumCommission'];
                                    
                                  }

                  $query_tbl_info = "SELECT memberType FROM credential_registration WHERE userId = '$user'";
                  $result_tbl_info = mysqli_query($dbcon,$query_tbl_info) or die ('error201');
                  while($rows_tbl_info = mysqli_fetch_assoc($result_tbl_info))
                  {
                  $memberType = $rows_tbl_info['memberType'];
                  $memberType = strtoupper($memberType);
                  $_SESSION['memType'] = $memberType;
                  $query_income = "SELECT SUM(`directRfrl`) as directRfrl, SUM(`allGeneralComm`) as allGeneralComm, SUM(`allSilverComm`) as allSilverComm, SUM(`allGoldComm`) as allGoldComm, SUM(`allDiamondComm`) as allDiamondComm, SUM(`allDealerComm`) as allDealerComm, SUM(`specificDealerComm`) as specificDealerComm, SUM(`generationComm`) as generationComm FROM commission_log WHERE userId = '$user'";
                        $result_income = mysqli_query($dbcon,$query_income) or die ('error201');
                        while($rows_income = mysqli_fetch_assoc($result_income))
                        {
                        $total_income = number_format($rows_income['directRfrl'] + $rows_income['allGeneralComm'] + $rows_income['allSilverComm'] + 
                        $rows_income['allGoldComm'] + $rows_income['allDiamondComm'] + $rows_income['allDealerComm'] + $rows_income['specificDealerComm'] + $rows_income['generationComm'] , 2, '.', '');

                        
                        }

                  //SELECT SUM(`amount`) as sumAmount,SUM(`company_comm`) as sumCommission  FROM `money_transfer` WHERE `id` = 
                        $total_income = number_format($total_income + $total_receive , 2, '.', '');
                        $total70 = $total_income * 0.7;
                        $total30 = $total_income * 0.3;

                        $total70 = number_format($total70 - $total_withdrawal, 2, '.', '');


echo'<table style="color:white">
<tr><td>Member Type :</td><td><button class="btn btn-info btn-xs"style="width:200%;margin-bottom:5%">'.$memberType.'</button></td></tr>
<tr><td>All Commission :</td><td><button class="btn btn-success btn-xs"style="width:200%;margin-bottom:5%">'.$total_income.'</button></td></tr>
<tr><td>Withdrawn Amount : </td><td><button class="btn btn-success btn-xs"style="width:200%;margin-bottom:5%">'.$total_withdrawal.'</button></td></tr>
<tr><td>E-Wallet : </td><td><button class="btn btn-success btn-xs"style="width:200%;margin-bottom:5%">'.$total70.'</button></td></tr>
<tr><td>P-Wallet : </td><td><button class="btn btn-success btn-xs"style="width:200%;margin-bottom:5%">'.$total30.'</button></td></tr>
</table>';
                 
}
                  
?>


</div>

    

</body>
</html>
