<?php
session_start();
error_reporting(E_ERROR | E_PARSE);
?>

<?php
if(!isset($_SESSION['user']))
    {
        echo "<script>location='landingPage.php'</script>";
    }

?>

  <?php include('sidepanel.php');?> 

  <div class="col-sm-9 col-md-10 col-lg-10 ">
    <div class="col-sm-12 col-md-12 col-lg-12">
          <div class="well" style="color: white;border: 0px solid black;background-color: black;padding: 1%;height: 60px">
                <h3><b>Profile Management</b></h3><br><br><br><br>
          </div>
    </div>
   
   <ul class="nav nav-tabs">
          <li><a data-toggle="tab" href="#menu1">Personal Information</a></li>
          <li><a data-toggle="tab" href="#menu2">Change Password</a></li>
          <li><a data-toggle="tab" href="#menu3">Change PIN</a></li>
          <li><a data-toggle="tab" href="#menu4">Nominee</a></li>
   </ul>
  <div class="tab-content">
    <div id="menu1" class="tab-pane fade in active">
      <?php //include('under_cons2.php');?>
      <?php include('basicUserInfo.php');?>
    </div>
    <div id="menu2" class="tab-pane fade">
      <?php include('changePassword.php');?>
    </div>
    <div id="menu3" class="tab-pane fade">
      <?php include('changePin.php');?>
    </div>
    <div id="menu4" class="tab-pane fade">
     <?php include('nominee.php');?>
    </div>
  </div>

  


</div>
</div><?php include('footer.php');?>
</div>




