<?php
session_start();
error_reporting(E_ERROR | E_PARSE);
?>

<?php
if(!isset($_SESSION['admin_user']))
    {
        echo "<script>location='../landingPage.php'</script>";
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>ADMIN PANEL</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link rel="icon" href="assets/images/dash/4.png" width="16px">
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 550px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #000000;
      height: 111%;
    }

    .sidenav a{
      background-color: #00334d;
      color: #ffffff;
      cursor: pointer;
      border-left: 4px solid #00ccff;
      margin-bottom: 5%;
    }
    ul li:hover a {
      color: #000000;
    }

    /* On small screens, set height to 'auto' for the grid */
    @media screen and (max-width: 767px) {
      .row.content {height: auto;} 
    }
  </style>
</head>
<body style=" background-color: #000000;">


<?php include('menu.php');?>
<?php
                  include('config.php');
                  
                  $query_income = "SELECT SUM(total_income) as total_income,SUM(total_incentive) as total_incentive,SUM(total_profit) as total_profit,SUM(total_distribution) as total_distribution , SUM(total_capital) as total_capital FROM commission_admin_log";
                  $result_income = mysqli_query($dbcon,$query_income) or die ('error203');
                        while($rows_income = mysqli_fetch_assoc($result_income)){
                        
                        $total_capital = number_format($rows_income['total_capital'], 2, '.', '');
                        
                        $total_income = number_format($rows_income['total_income'], 2, '.', '');
                        $total_incentive = number_format($rows_income['total_incentive'], 2, '.', '');
                        $total_profit = number_format($rows_income['total_profit'], 2, '.', '');
                        $total_distribution = number_format($rows_income['total_distribution'], 2, '.', '');
                        

    
   echo '<div class="col-sm-9 col-md-10 col-lg-10" style="margin-top: 2%">
      <div class="row"> 
        <div class="col-sm-3">       
          <div class="well">
            <img class="img-responsive img-circle" src="assets/images/dash/6.png" style="width: 80px;">
            <p style="float: right;width:100px;margin-top: -70px"> Total Income<br><br><b>৳ '.$total_income.'</b></p>
          </div>
        </div>
        <div class="col-sm-3">       
          <div class="well">
            <img class="img-responsive img-circle" src="assets/images/dash/6.png" style="width: 80px;">
            <p style="float: right;width:100px;margin-top: -70px"> Total Incentive<br><br><b>৳ '.$total_incentive.'</b></p>
          </div>
        </div> 
        <div class="col-sm-3">       
          <div class="well">
            <img class="img-responsive img-circle" src="assets/images/dash/6.png" style="width: 80px;">
            <p style="float: right;width:100px;margin-top: -70px"> Total Distribution<br><b>৳ '.$total_distribution.'</b></p>
          </div>
        </div>
        <div class="col-sm-3">       
          <div class="well">
            <img class="img-responsive img-circle" src="assets/images/dash/6.png" style="width: 80px;">
            <p style="float: right;width:100px;margin-top: -70px"> Total Profit<br><br><b>৳ '.$total_profit.'</b></p>
          </div>
        </div>      
        <div class="col-sm-3">       
          <div class="well">
            <img class="img-responsive img-circle" src="assets/images/dash/6.png" style="width: 80px;">
            <p style="float: right;width:100px;margin-top: -70px"> Total Capital<br><br><b>৳ '.$total_capital.'</b></p>
          </div>
        </div>';

      $queryadmin_receive_pay = "SELECT SUM(amount) as total_distribution  FROM admin_receive_pay where paidBy = ''";
      $resultadmin_receive_pay = mysqli_query($dbcon,$queryadmin_receive_pay) or die ('error201');
      while($rowsadmin_receive_pay = mysqli_fetch_assoc($resultadmin_receive_pay)){
           $total_received = number_format($rowsadmin_receive_pay['total_distribution'], 2, '.', '');
         }
        echo'<div class="col-sm-3">       
          <div class="well">
            <img class="img-responsive img-circle" src="assets/images/dash/6.png" style="width: 80px;">
            <p style="float: right;width:100px;margin-top: -70px"> Total Payable<br><br><b>৳ '.$total_received.'</b></p>
          </div>
        </div>
      </div>';

      }
?>
      <div class="row">        
        <div class="col-sm-6">
          <div class="well">
            <h5>Disable Member Inactive For 4 months</h5>
            <?php include('disableInactiveMember.php');?> 
            <br><br>
          </div>
        </div>
         <div class="col-sm-6">
          <div class="well">
            <h5>Modify Member</h5>
            <?php include('disableAnyMember.php');?> 
          </div>
        </div>       
      </div>
    </div>
  </div>
</div>

</body>
</html>