<?php session_start();
error_reporting(E_ERROR | E_PARSE);?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link rel="icon" href="assets/images/dash/4.png" width="16px">


    <title>TakeNpaY.org</title>

    <!-- Bootstrap Core CSS -->
    <link href="assets/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/blog-post.css" rel="stylesheet">

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
        <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
        <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
    <![endif]-->

</head>

<body style="background-color: #001a1a">


<?php 
include('menu.php');
include ('config.php');
?>
    <!-- Page Content -->
        

        <?php
        if (isset($_POST['submit'])){

          $action = $_POST['submit'];
          if($action == 'save')
          {
             /*iamge upload*/
              if(isset($_FILES['image'])){
                  $errors= array();
                  $file_name = $_POST["title"].$_FILES['image']['name'];
                  $file_size =$_FILES['image']['size'];
                  $file_tmp =$_FILES['image']['tmp_name'];
                  $file_type=$_FILES['image']['type'];
                  $file_ext=strtolower(end(explode('.',$_FILES['image']['name'])));
                  
                  $expensions= array("jpeg","jpg","png");
                  
                  if(in_array($file_ext,$expensions)=== false){
                     $errors[]="extension not allowed, please choose a JPEG or PNG file.";
                  }
                  
                  if($file_size > 2097152){
                     $errors[]='File size must be excately 2 MB';
                  }
                  
                  if(empty($errors)==true){
                     move_uploaded_file($file_tmp,"assets/images/Homepage_Image/".$file_name);
                     //echo "Success";
                  }else{
                     print_r($errors);
                  }
               }
            /*end*/


            $title = $_POST["title"];
            $image = addslashes($_FILES['image']['name']);
            //$time = $rows['comment'];
                 
            $queryDelete ="DELETE FROM home_image WHERE `place`='$title'";
            $resultDelete = mysqli_query($dbcon,$queryDelete) or die ('error 4043');

            $query ="INSERT INTO home_image (image,place) VALUES('".$image."','".$title."')";
            $result = mysqli_query($dbcon,$query) or die ('error 40423');
          }



          elseif ($action == 'delete') {
           //if (array_key_exists('delete_file', $_POST)) {
              $title = $_POST['title'];
              $queryDelete ="SELECT image,place FROM home_image WHERE `place`='$title'";
              $sqldata = mysqli_query($dbcon,$queryDelete) or die ('error 404');
              while($rows = mysqli_fetch_assoc($sqldata)){
                        $title = $rows['place'];
                        $image = $rows['image'];
                      }

              $resultDelete = mysqli_query($dbcon,$queryDelete) or die ('error 4043');
              
              $path = "assets/images/Homepage_Image";
              $filename =  $path . "/" .  $title.$image;
              if (file_exists($filename)) {
                unlink($filename);
                //echo 'File '.$filename.' has been deleted';
              } else {
                //echo 'Could not delete '.$filename.', file does not exist';
              }
            }
          //}
        }


        ?>

       <!-- TITLE,IMAGE,SUBTITLE Form -->

                  <div class="col-md-6" style="background-color: white">
                    <h4>Home Page Image Slider</h4>
                     <form action="uploadImage.php" method="post"  enctype="multipart/form-data">
                        
                        <table>
                          <tr>
                           <td>
                              <select name="title" style="width: 100%">
                                <option value="Slider1">Slider 1</option>
                                <option value="Slider2">Slider 2</option>
                                <option value="Slider3">Slider 3</option>
                                <option value="Slider4">Slider 4</option>
                                <option value="Slider5">Slider 5</option>
                              </select>
                           </td>
                           <td>
                             <!--button type="submit" name="submit" value="delete" class="btn btn-sm btn-danger">Delete</button-->
                           </td>
                           <td>
                             <input type="file" name="image"/>
                           </td>
                           <td>
                             <button type="submit" name="submit" value="save" class="btn btn-sm btn-success">Upload</button>

                           </td> 
                          </tr>
                        </table>
                            
                        
                        <!--button type="submit" value="Submit" name="Submit" class="btn btn-primary">Submit</button-->
                    </form>
                </div>

                <div class="col-md-6" style="background-color: white">
                    <h4>Product Image</h4>
                     <form method="post" action="uploadImage.php" enctype="multipart/form-data">
                        
                        <table>
                          <tr>
                           <td>
                              <select name="title" style="width: 100%">
                                <option value="Product1">Product 1</option>
                                <option value="Product2">Product 2</option>
                                <option value="Product3">Product 3</option>
                                <option value="Product4">Product 4</option>
                                <option value="Product5">Product 5</option>
                                <option value="Product6">Product 6</option>
                                <option value="Product7">Product 7</option>
                                <option value="Product8">Product 8</option>
                                <option value="Product9">Product 9</option>
                                <option value="Product10">Product 10</option>
                                <option value="Product11">Product 11</option>
                                <option value="Product12">Product 12</option>
                                <option value="Product13">Product 13</option>
                                <option value="Product14">Product 14</option>
                                <option value="Product15">Product 15</option>
                                <option value="Product16">Product 16</option>
                                <option value="Product17">Product 17</option>
                                <option value="Product18">Product 18</option>
                              </select>
                           </td>
                           <td style="padding: 0%">
                               <input type="hidden" value="Slider11.jpg" name="delete_file" />
                            </td>
                           <td style="padding: 0%">
                             <!--button type="submit" name="submit" value="delete" class="btn btn-sm btn-danger">Delete</button-->
                           </td>
                           <td style="padding: 0%">
                              <input type="file" name="image"/>
                           </td> 
                           <td style="padding: 0%">
                             <button type="submit" name="submit" value="save" class="btn btn-sm btn-success">Upload</button>
                           </td>
                          </tr>
                        </table>
                            
                        
                        <!--button type="submit" value="Submit" name="Submit" class="btn btn-primary">Submit</button-->
                    </form>
                </div>



<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.5";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
    <!-- /.container -->

    <!-- jQuery -->
    <script src="js/jquery.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

</body>

</html>
