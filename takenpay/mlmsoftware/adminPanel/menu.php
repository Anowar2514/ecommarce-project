<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

<div class="col-md-12 col-lg-12  hidden-xs hidden-sm" style="background-color: #001a1a">
<img src="assets/images/l2.png" style="width:11%;margin-left: 2%;margin-right: 4%;">

                  <?php
                  include('config.php');
                  
                  $query_income = "SELECT SUM(total_income) as total_income,SUM(total_incentive) as total_incentive,SUM(total_profit) as total_profit FROM commission_admin_log";
                  $result_income = mysqli_query($dbcon,$query_income) or die ('error201');
                        while($rows_income = mysqli_fetch_assoc($result_income))
                        {
                        $total_income = number_format($rows_income['total_income'], 2, '.', '');
                        $total_incentive = number_format($rows_income['total_incentive'], 2, '.', '');
                        $total_profit = number_format($rows_income['total_profit'], 2, '.', '');
                        }


                  echo'
                  <h7 style="margin-top: 1%;background-color: transparent;margin-left: 0%;text-align: left;color:white">Member Type : <button class="btn btn-info btn-xs">ADMIN</button></h7> &nbsp &nbsp &nbsp &nbsp
                  <h7 style="margin-top: 1%;background-color: transparent;margin-left: 0%;text-align: left;color:white">Total Incentive : <button class="btn btn-success btn-xs">'.$total_incentive.'</button></h7> &nbsp &nbsp &nbsp &nbsp
                  <h7 style="margin-top: 1%;background-color: transparent;margin-left: 0%;text-align: left;color:white">Total Profit: <button class="btn btn-success btn-xs">'.$total_profit.'</button></h7>';
                  
                  
                  ?>

    <a href="logout.php" class="btn btn-xs btn-danger"><span class="glyphicon glyphicon-log-out"></span> Logout</a>
</div>


<nav class="navbar navbar-inverse visible-xs">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>                        
      </button>
      <a class="navbar-brand" href="#">Logo</a>
    </div>
    <div class="collapse navbar-collapse" id="myNavbar">
      <ul class="nav navbar-nav">
         <li><a id="dashboard" href="adminPanel.php"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
         <li><a id="dashboard" href="adminPanel_confirmation_user.php"><span class="glyphicon glyphicon-user"></span>Confirmation</a></li>
         <li><a id="dashboard" href="manage_package.php"><span class="glyphicon glyphicon-shopping-cart"></span>Package Management</a></li>
         <li><a id="dashboard" href="manage_dealer.php"><span class="glyphicon glyphicon-knight"></span> Dealer Management</a></li>
         <li><a id="dashboard" href="capital_transfer_distribution.php"><span class="glyphicon glyphicon-share"></span> Distribution</a></li>
        <li> <a href="logout.php" class="btn btn-xs btn-danger" style="color:white">Logout</a></li>
      </ul>
    </div>
  </div>
</nav>


<div class="container-fluid">
  <div class="row content">
    <div class="col-sm-3 col-md-2 col-lg-2  sidenav hidden-xs" style="padding: 0px;background-color: #002233">
      <img src="assets/images/user.png" style="width:45%;margin:5%;margin-left: 25%;">
      <ul class="nav nav-stacked">
         <li><a id="dashboard" href="adminPanel.php"><span class="glyphicon glyphicon-dashboard"></span> Dashboard</a></li>
      </ul>
      <ul class="nav nav-stacked">
         <li><a id="dashboard" href="adminPanel_confirmation_user.php"><span class="glyphicon glyphicon-user"></span> Confirmation</a></li>
      </ul>
      <ul class="nav nav-stacked">
         <li><a id="dashboard" href="manage_package.php"><span class="glyphicon glyphicon-shopping-cart"></span> Package Management</a></li>
      </ul>
      <ul class="nav nav-stacked">
         <li><a id="dashboard" href="manage_dealer.php"><span class="glyphicon glyphicon-knight"></span> Dealer Management</a></li>
      </ul>
      <ul class="nav nav-stacked">
         <li><a id="dashboard" href="capital_transfer_distribution.php"><span class="glyphicon glyphicon-share"></span> Distribution</a></li>
      </ul>
      <ul class="nav nav-stacked">
         <li><a id="dashboard" href="uploadImage.php"><span class="glyphicon glyphicon-upload"></span> Upload Image</a></li>
      </ul>
      <ul class="nav nav-stacked">
         <li><a id="dashboard" href="deleteImage.php"><span class="glyphicon glyphicon-download"></span> Delete Image</a></li>
      </ul>
      <br>

    </div>
    <br>