<?php
session_start();
error_reporting(E_ERROR | E_PARSE);
?>

<?php
if(!isset($_SESSION['admin_user']))
    {
        echo "<script>location='../landingPage.php'</script>";
    }

?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>ADMIN PANEL</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>

  <link rel="icon" href="assets/images/dash/4.png" width="16px">
  <style>
    /* Set height of the grid so .sidenav can be 100% (adjust as needed) */
    .row.content {height: 550px}
    
    /* Set gray background color and 100% height */
    .sidenav {
      background-color: #000000;
      height: 111%;
    }

    .sidenav a{
      background-color: #00334d;
      color: #ffffff;
      cursor: pointer;
      border-left: 4px solid #00ccff;
      margin-bottom: 5%;
    }
    ul li:hover a {
      color: #000000;
    }

    /* On small screens, set height to 'auto' for the grid */
    @media screen and (max-width: 767px) {
      .row.content {height: auto;} 
    }
  </style>
</head>
<body style=" background-color: #000000;">


<?php include('menu.php');?>


    
    <div class="col-sm-9 col-md-10 col-lg-10" style="margin-top: 2%">
      <div class="row">
        <div class="col-sm-6">
          <div class="well">
            <h4>Create Dealer</h4>
            <?php include('createDealer.php');?> 
          </div>
        </div>
        <div class="col-sm-6">
          <div class="well">
            <h4>Generate Code for Dealer</h4>
            <?php include('genCodeForDealer.php');?> 
            <br>
          </div>
        </div>
        <div class="col-sm-12">
          <div class="well">
            <h4>Dealer List</h4>
            <?php 
          $query = "SELECT * FROM member_table_dealer";
          $result = mysqli_query($dbcon,$query) or die ('error');
          
          if(!$result){
            echo "query failed";
          
          }
          $i = 0;
          echo '<table class="table table-bordered">
                <thead>
                  <tr class="info">
                    <th>Sl</th>
                    <th>Dealer Name</th>
                    <th>User ID</th>
                    <th>Dealer Type</th>
                    <th>Dealer District</th>
                    <th>Dealer Added On</th>
                    <th>Status</th>
                  </tr>
                </thead>
                <tbody>';

          while($rows = mysqli_fetch_assoc($result)){
            $i++;
            $dealerName = $rows['dealerName'];
            $dealerId = $rows['dealerId'];
            $dealerType = $rows['dealerType'];
            $district = $rows['district'];
            $updatedToDealer = $rows['updatedToDealer'];
            $status = $rows['status'];
          
            
            echo '<tr class="success">
                    <td>'.$i.'</td>
                    <td>'.$dealerName.'</td>
                    <td>'.$dealerId.'</td>
                    <td>'.$dealerType.'</td>
                    <td>'.$district.'</td>
                    <td>'.$updatedToDealer.'</td>
                    <td>'.$status.'</td>
                  </tr>';


          
          }

 echo '</tbody>
  </table>';
?> 
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

</body>
</html>
