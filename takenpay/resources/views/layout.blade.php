<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Take N Pay | @yield('title')</title>
    <link href="{{asset('front_end/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('front_end/css/font-awesome.min.css')}}" rel="stylesheet">
    <link href="{{asset('front_end/css/prettyPhoto.css')}}" rel="stylesheet">
    <link href="{{asset('front_end/css/price-range.css')}}" rel="stylesheet">
    <link href="{{asset('front_end/css/animate.css')}}" rel="stylesheet">
    <link href="{{asset('front_end/css/main.css')}}" rel="stylesheet">
    <link href="{{asset('front_end/css/responsive.css')}}" rel="stylesheet">
    <!--[if lt IE 9]>
    <script src="{{asset('front_end/js/html5shiv.js')}}"></script>
    <script src="{{asset('front_end/js/respond.min.js')}}"></script>
    <![endif]-->
    <link rel="shortcut icon" href="{{asset('front_end/images/ico/favicon.ico')}}">
    <link rel="apple-touch-icon-precomposed" sizes="144x144" href="{{asset('front_end/images/ico/apple-touch-icon-144-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="114x114" href="{{asset('front_end/images/ico/apple-touch-icon-114-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" sizes="72x72" href="{{asset('front_end/images/ico/apple-touch-icon-72-precomposed.png')}}">
    <link rel="apple-touch-icon-precomposed" href="{{asset('front_end/images/ico/apple-touch-icon-57-precomposed.png')}}">
</head><!--/head-->

<body>
<header id="header"><!--header-->
    <div class="header_top"><!--header_top-->
        <div class="container">
            <div class="row">
                <div class="col-sm-6">
                    <div class="contactinfo">
                        <ul class="nav nav-pills">
                            <li><b><a href="#" onclick="return confirm('Call Here For Order: +8801612150312');"><i class="fa fa-phone"></i> +8801612150312</a></b></li>
                            <li><b><a href="#" onclick="return confirm('Email Here For Order: takenpay24@gmail.com');"><i class="fa fa-envelope"></i>  takenpay24@gmail.com</a></b></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="social-icons pull-right">
                        <ul class="nav navbar-nav">
                            <?php
                            $all_published_social_link = DB::table('tbl_sociallinks')
                            ->where('social_status','active')
                            ->get();
                            foreach($all_published_social_link as $v_social_link){
                            ?>
                            <li><a href="{{URL::to($v_social_link->social_link)}}" target="_blank"><i class="{{$v_social_link->social_logo}}"></i></a></li>
                            <?php }?>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header_top-->

    <div class="header-middle"><!--header-middle-->
        <div class="container">
            <div class="row">
                <div class="col-sm-4">
                    <div class="logo pull-left">
                        <h4><img src="{{asset('front_end/images/home/head01.jpg')}}" alt="" hight="30px" width="30px" /><a href="{{URL::to('/')}}"><b><span style="color: orange">  Take </span>N <span style="color:orange">Pay</span></b></a></h4>
                    </div>
                </div>
                <div class="col-sm-8">
                    <div class="shop-menu pull-right">
                        <ul class="nav navbar-nav">
                            <li><a href="http://mlmsoftware.takenpay.org/" target="_blank" style="color:blue"><i class="fa fa-money"></i>MlM-Software</a></li>
                            <li><a href="{{URL::to('/about')}}" style="color:blue"><i class="fa fa-book"></i>About</a></li>
                            <li><a href="{{URL::to('/contact')}}" style="color:blue"><i class="fa fa-phone"></i>Contact</a></li>
                            <?php
                            $customer_id = Session::get('customer_id');
                            $shipping_id = Session::get('shipping_id');
                            ?>
                            @if($customer_id != NULL && $shipping_id == NULL)
                            <li><a href="{{URL::to('/checkout')}}" style="color:blue"><i class="fa fa-money"></i> Checkout</a></li>
                            @elseif($customer_id != NULL && $shipping_id != NULL)
                            <li><a href="{{URL::to('/payment')}}" style="color:blue"><i class="fa fa-money"></i> Checkout</a></li>
                                @else
                                <li><a href="{{URL::to('/login-check')}}" style="color:blue"><i class="fa fa-money"></i> Checkout</a></li>
                            @endif
                            <li><a href="{{URL::to('/show-cart')}}" style="color:blue"><i class="fa fa-shopping-cart"></i> Cart</a></li>
                            <?php
                            $customer_id = Session::get('customer_id');
                            ?>
                            @if($customer_id !=NULL)
                            <li><a href="{{URL::to('/customer-logout')}}" style="color:blue"><i class="fa fa-lock"></i> Logout</a></li>
                            @else
                            <li><a href="{{URL::to('/login-check')}}" style="color:blue"><i class="fa fa-lock"></i> Login</a></li>
                                @endif
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/header-middle-->

    <div class="header-bottom"><!--header-bottom-->
        <div class="container">
            <div class="row">
                <div class="col-sm-9">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                    </div>
                    <div class="mainmenu pull-left">
                        <ul class="nav navbar-nav collapse navbar-collapse">
                            <li style="color: green"><a href="#" style="color:orange">Man's<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <?php
                                    $all_published_sub_category = DB::table('categories')
                                        ->join('sub_categories','categories.category_id','=','sub_categories.category_id')
                                        ->where('categories.publication_status',1)
                                        ->where('categories.category_id',1)
                                        ->get();
                                    foreach($all_published_sub_category as $v_sub_category){
                                    ?>
                                    <li><a href="{{ URL::to('/product_by_sub_category/'.$v_sub_category->sub_category_id) }}">{{ $v_sub_category->sub_category_name }}</a></li>
                                    <?php }?>
                                </ul>
                            </li>
                            <li style="color: green"><a href="#" style="color: orange">Women's<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <?php
                                    $all_published_sub_category = DB::table('categories')
                                        ->join('sub_categories','categories.category_id','=','sub_categories.category_id')
                                        ->where('categories.publication_status',1)
                                        ->where('categories.category_id',2)
                                        ->get();
                                    foreach($all_published_sub_category as $v_sub_category){
                                    ?>
                                        <li><a href="{{ URL::to('/product_by_sub_category/'.$v_sub_category->sub_category_id) }}">{{ $v_sub_category->sub_category_name }}</a></li>
                                        <?php }?>
                                </ul>
                            </li>
                            <li style="color: green"><a href="#" style="color: orange">Electronics<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <?php
                                    $all_published_sub_category = DB::table('categories')
                                        ->join('sub_categories','categories.category_id','=','sub_categories.category_id')
                                        ->where('categories.publication_status',1)
                                        ->where('categories.category_id',7)
                                        ->get();
                                    foreach($all_published_sub_category as $v_sub_category){
                                    ?>
                                    <li><a href="{{ URL::to('/product_by_sub_category/'.$v_sub_category->sub_category_id) }}">{{ $v_sub_category->sub_category_name }}</a></li>
                                    <?php }?>
                                </ul>
                            </li>
                            <li style="color: green"><a href="#" style="color: orange">Computer<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <?php
                                    $all_published_sub_category = DB::table('categories')
                                        ->join('sub_categories','categories.category_id','=','sub_categories.category_id')
                                        ->where('categories.publication_status',1)
                                        ->where('categories.category_id',5)
                                        ->get();
                                    foreach($all_published_sub_category as $v_sub_category){
                                    ?>
                                    <li><a href="{{ URL::to('/product_by_sub_category/'.$v_sub_category->sub_category_id) }}">{{ $v_sub_category->sub_category_name }}</a></li>
                                    <?php }?>
                                </ul>
                            </li>
                            <li style="color: green"><a href="#" style="color: orange">Furnitures<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <?php
                                    $all_published_sub_category = DB::table('categories')
                                        ->join('sub_categories','categories.category_id','=','sub_categories.category_id')
                                        ->where('categories.publication_status',1)
                                        ->where('categories.category_id',6)
                                        ->get();
                                    foreach($all_published_sub_category as $v_sub_category){
                                    ?>
                                    <li><a href="{{ URL::to('/product_by_sub_category/'.$v_sub_category->sub_category_id) }}">{{ $v_sub_category->sub_category_name }}</a></li>
                                    <?php }?>
                                </ul>
                            </li>
                            <li style="color: green"><a href="#" style="color: orange">Others<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <?php
                                    $all_published_sub_category = DB::table('categories')
                                        ->join('sub_categories','categories.category_id','=','sub_categories.category_id')
                                        ->where('categories.publication_status',1)
                                        ->where('categories.category_id',12)
                                        ->get();
                                    foreach($all_published_sub_category as $v_sub_category){
                                    ?>
                                    <li><a href="{{ URL::to('/product_by_sub_category/'.$v_sub_category->sub_category_id) }}">{{ $v_sub_category->sub_category_name }}</a></li>
                                    <?php }?>
                                </ul>
                            </li>
                            <li style="color: green" class="dropdown"><a href="#" style="color: orange">Price Up&Down<i class="fa fa-angle-down"></i></a>
                                <ul role="menu" class="sub-menu">
                                    <li><a href="{{URL::to('/price-down-product')}}">Price Down Products</a></li>
                                    <li><a href="{{URL::to('/price-up-product')}}">Product Up Products</a></li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3">
                    <form action="{{url('/search')}}" method="get">
                    <div class="search_box pull-right">
                        <input type="text" name="search" value="{{ request()->input('search') }}" placeholder="Search"/>
                    </div>
                    </form>
                </div>
            </div>
        </div>
    </div><!--/header-bottom-->
</header><!--/header-->

<!--Slider-->
@yield('slider')
<!--/Slider-->

<section>
    <div class="container">
        <div class="row">
            <div class="col-sm-3">
                <div class="left-sidebar">
                    <h2>Categories</h2>
                    <div class="panel-group category-products" id="accordian"><!--category-productsr-->
                        <?php
                        $all_published_category = DB::table('categories')
                            //->join('sub_categories','categories.category_id','=','sub_categories.category_id')
                            ->where('categories.publication_status',1)
                            ->get();
                        foreach($all_published_category as $v_category){
                        ?>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a href="{{ URL::to('/product_by_category/'.$v_category->category_id) }}">
                                        {{ $v_category->category_name }}
                                        </a>
                                </h4>
                            </div>
                        </div>
                        <?php }?>
                    </div><!--/category-products-->

                    <div class="sidebar-category sidebar-category-visible"><!--brands_products-->
                        <h2>Brand</h2>
                        <div class="category-content no-padding">
                            <ul class="navigation navigation-main navigation-accordion">
                                <?php
                                $all_published_brand = DB::table('brands')
                                    ->where('publication_status',1)
                                    //->limit(10)
                                    ->get();
                                foreach($all_published_brand as $v_brand){
                                ?>
                                <li><a href="{{ URL::to('/product_by_brand/'.$v_brand->brand_id) }}">{{ $v_brand->brand_name }}</a>
                                    {{--<ul>--}}
                                        {{--<li><a href="{{ URL::to('/product_by_sub_category/'.$v_category->sub_category_id) }}">{{ $v_category->sub_category_name }}</a></li>--}}
                                    {{--</ul>--}}
                                </li>
                                <?php }?>
                            </ul>
                        </div>
                    </div><!--/brands_products-->

                    <div class="price-range"><!--price-range-->
                        <h2>Price Range</h2>
                        <?php
                        $minprice = DB::table('products')->min('product_price');
                        $minlevel = $minprice+10;
                        $maxprice = DB::table('products')->max('product_price');
                        $maxlevel = $maxprice+20;
                        ?>
                        <div class="well text-center">
                            <form action="{{ URL::to('/price-range') }}" method="get">
                                {{ csrf_field() }}
                            <input type="text" class="span2" value="0,10000" name="price_range"  data-slider-min="{{ $minprice }}" data-slider-max="{{ $maxprice }}" data-slider-step="5" data-slider-value="[{{ $minlevel }}, {{ $maxlevel }}]" id="sl2" ><br />
                            <b class="pull-left">BDT-{{ $minprice }}</b> <b class="pull-right">BDT-{{ $maxprice }}</b>
                                <input type="submit" value="FILTER" class="btn btn-success">
                            </form>
                        </div>
                    </div><!--/price-range-->

                    <div class="shipping text-center"><!--shipping-->
                        <img src="{{asset('front_end/images/home/shipping.jpg')}}" alt="" />
                    </div><!--/shipping-->
                </div>
            </div>

            <div class="col-sm-9 padding-right">
                <div class="features_items"><!--features_items-->
                    @yield('content')
            </div>
        </div>
    </div>
    </div>
</section>

<footer id="footer"><!--Footer-->
    <div class="footer-top">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="companyinfo">
                        <h3 style="color: blue"><span style="color: orange">Delivery</span>Men's</h3>
                        <p>The Delivery men's will deliver your product safely.........</p>
                    </div>
                </div>
                <div class="col-sm-6">
                    <?php
                    $all_published_deliverymens = DB::table('tbl_deliverymens')
                        ->where('deliveryman_status','active')
                        ->limit(4)
                        ->get();
                    foreach($all_published_deliverymens as $v_de_man){
                    ?>
                    <div class="col-sm-3">
                                <div class="video-gallery text-center">
                                    <a href="#">
                                        <div class="iframe-img">
                                            <img src="{{ URL::to($v_de_man->delivery_man_image) }}" height="80px" width="80px">
                                        </div>
                                        <div class="overlay-icon">
                                            <i class="fa fa-play-circle-o"></i>
                                        </div>
                                    </a>
                                    <p>{{ $v_de_man->delivery_man_name }}</p>
                                    <h2>{{ $v_de_man->delivery_man_phone }}</h2>
                                </div>
                    </div>
                        <?php }?>
                </div>
                <div class="col-sm-3">
                    <div class="address">
                        <img src="{{asset('front_end/images/home/map.png')}}" alt="" />
                        <p>Bosila, Mohammadpur, Dhaka-1207,Bangladesh</p>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="footer-widget">
        <div class="container">
            <div class="row">
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2 style="color: orange">Service</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="{{URL::to('/contact')}}">Online Help</a></li>
                            <li><a href="{{URL::to('/contact')}}">Contact Us</a></li>
                            <li><a href="{{URL::to('/show-cart')}}">Order Status</a></li>
                            <li><a href="{{URL::to('/contact')}}">Company Location</a></li>
                            <li><a href="#">FAQ’s</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2 style="color: orange">Quock Shop</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <?php
                            $all_published_category = DB::table('categories')
                                ->where('publication_status',1)
                                ->limit(5)
                                ->get();
                            foreach($all_published_category as $v_category){
                                ?>
                            <li><a href="{{ URL::to('/product_by_category/'.$v_category->category_id) }}">{{$v_category->category_name}}</a></li>
                            <?php }?>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2 style="color: orange">Policies</h2>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="#">Terms of Use</a></li>
                            <li><a href="#">Privecy Policy</a></li>
                            <li><a href="#">Refund Policy</a></li>
                            <li><a href="#">Billing System</a></li>
                            <li><a href="#">Ticket System</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-2">
                    <div class="single-widget">
                        <h2 style="color: green">About <span style="color: orange">Take <span style="color: blue">N</span> Pay</span></h2>
                        <ul class="nav nav-pills nav-stacked">
                            <li><a href="{{URL::to('/about')}}">Company Information</a></li>
                            <li><a href="{{URL::to('/contact')}}">Careers</a></li>
                            <li><a href="{{URL::to('/contact')}}">Store Location</a></li>
                            <li><a href="{{URL::to('/about')}}">Affillate Program</a></li>
                            <li><a href="{{ URL::to('/user-admin') }}" target="_blank">Login</a></li>
                        </ul>
                    </div>
                </div>
                <div class="col-sm-3 col-sm-offset-1">
                    <div class="single-widget">
                        <h2>About <b style="color: orange">Take<span style="color: blue"> N </span><span style="color: orange"> Pay</span></b></h2>
                        <form action="#" class="searchform">
                            <input type="text" placeholder="Your email address" />
                            <button type="submit" class="btn btn-default"><i class="fa fa-arrow-circle-o-right"></i></button>
                            <p>Get the most recent updates from <br />our site and be updated your self...</p>
                        </form>
                    </div>
                </div>

            </div>
        </div>
    </div>

    <div class="footer-bottom">
        <div class="container">
            <div class="row">
                <p class="pull-left" style="color: green">Copyright © 2018 Take N Pay Inc. All rights reserved.</p>
                <p class="pull-right"><a href="{{URL::to('/admin')}}" target="_blank"><i class="fa fa-apple"></i></a>  Developed by <span><a target="_blank" href="http://www.facebook.com/anwarahmed.saif">Md.Anowar Hossain</a></span></p>
            </div>
        </div>
    </div>

</footer><!--/Footer-->



<script src="{{asset('front_end/js/jquery.js')}}"></script>
<script src="{{asset('front_end/js/bootstrap.min.js')}}"></script>
<script src="{{asset('front_end/js/jquery.scrollUp.min.js')}}"></script>
<script src="{{asset('front_end/js/price-range.js')}}"></script>
<script src="{{asset('front_end/js/jquery.prettyPhoto.js')}}"></script>
<script src="{{asset('front_end/js/main.js')}}"></script>
</body>
</html>