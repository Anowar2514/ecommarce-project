@extends('admin_layout')
@section('title','Add Delivery Man')
@section('admin_content')
@section('class_delivery_man','active')
@section('page_name','Add Delivery Man')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
<div class="container">
    <div class="row">
        <div class="col-md-4  col-md-offset-3">
            <form action="{{url('/save-delivery-man')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Add Delivery Man</h6>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label>Delivery Man Name</label>
                            <input type="text" name="delivery_man_name" class="form-control" placeholder="Add Delivery Man Name" required>
                        </div>
                        <div class="form-group">
                            <label>Delivery Man Image</label>
                            <input type="file" name="delivery_man_image" class="file-input" required>
                        </div>
                        <div class="form-group">
                            <label>Phone Number</label>
                            <input type="text" name="delivery_man_phone" class="form-control" placeholder="Add Phone Number" required>
                        </div>
                        <div class="form-group">
                            <label>Publication Status</label>
                            <select class="form-control" name="deliveryman_status">
                                <option>---------- Select Status ----------</option>
                                <option value="active">Published</option>
                                <option value="inactive">Unpublished</option>
                            </select>
                        </div>
                        <div>
                            <button type="submit" class="btn bg-teal-400">Submit Delivery Man</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
<!-- /form actions -->
@endsection