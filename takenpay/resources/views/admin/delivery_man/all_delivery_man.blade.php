@extends('admin_layout')
@section('title','All Delivery Man')
@section('admin_content')
@section('class_delivery_man','active')
@section('page_name','All Delivery Man')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">All Delivery Mans</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <table class="table table-togglable table-hover">
        <thead>
        <tr>
            <th data-toggle="true">Serial No</th>
            <th data-hide="phone">Delivery Man Image</th>
            <th data-hide="phone">Delivery Man Name</th>
            <th data-hide="phone">Delivery Man Phone</th>
            <th data-hide="phone" data-ignore="true">Status</th>
            <th data-hide="phone,tablet" class="text-center">Action</th>
        </tr>
        </thead>
        <?php $sl=1; ?>
        @foreach($all_delivery_man_info as $v_de_man)
            <tbody>
            <tr>
                <td>{{ $sl++ }}</td>
                <td><a href="#"><img src="{{ URL::to($v_de_man->delivery_man_image) }}" height="120px" width="100px"></a></td>
                <td><a href="#">{{ $v_de_man->delivery_man_name }}</a></td>
                <td><a href="#">{{ $v_de_man->delivery_man_phone }}</a></td>
                <td>
                    @if($v_de_man->deliveryman_status =='active')
                        <span class="label label-success">
                    Active
                </span>
                    @else
                        <span class="label label-danger">
                        Inactive
                    </span>
                    @endif
                </td>
                <td class="text-center">
                    @if($v_de_man->deliveryman_status=='active')
                        <a class="btn btn-warning" href="{{URL::to('/inactive-delivery-man/'.$v_de_man->deliveryman_id)}}">
                            <i class=" icon-thumbs-down3"></i>
                        </a>
                    @else
                        <a class="btn btn-success" href="{{URL::to('/active-delivery-man/'.$v_de_man->deliveryman_id)}}">
                            <i class=" icon-thumbs-up3"></i>
                        </a>
                    @endif
                    <a class="btn btn-info" href="{{URL::to('/edit-delivery-man/'.$v_de_man->deliveryman_id)}}">
                        <i class="icon-pencil"></i>
                    </a>
                    <a class="btn btn-danger" href="{{URL::to('/delete-delivery-man/'.$v_de_man->deliveryman_id)}}" onclick="return confirm('Are You Sure To Delete This Profile !!!');" id="delete">
                        <i class="icon-trash"></i>
                    </a>

                </td>
            </tr>
            </tbody>
        @endforeach
    </table>
</div>
{{ $all_delivery_man_info->links() }}
<br><br>
<!-- /column names -->
@endif
@endsection