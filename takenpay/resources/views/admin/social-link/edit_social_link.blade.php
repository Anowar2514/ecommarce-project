@extends('admin_layout')
@section('title','Edit Social Link')
@section('admin_content')
@section('class_social_link','active')
@section('page_name','Edit Social Link')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
<div class="container">
    <div class="row">
        <div class="col-md-4  col-md-offset-3">
            <form action="{{url('/update-social-link',$social_link_info->social_id)}}" method="post">
                {{ csrf_field() }}
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Edit Social Link</h6>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label>Social Link Logo Class</label>
                            <input type="text" name="social_logo" class="form-control" value="{{ $social_link_info->social_logo }}">
                        </div>
                        <div class="form-group">
                            <label>Social Link Name</label>
                            <input type="text" name="social_name" class="form-control" value="{{ $social_link_info->social_name }}">
                        </div>
                        <div class="form-group">
                            <label>Social Link</label>
                            <input type="text" name="social_link" class="form-control" value="{{ $social_link_info->social_link }}">
                        </div>
                        <div>
                            <button type="submit" class="btn bg-teal-400">Update Social Link</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
<!-- /form actions -->
@endsection