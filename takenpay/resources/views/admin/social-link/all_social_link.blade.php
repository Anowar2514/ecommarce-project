@extends('admin_layout')
@section('title','All Social Link')
@section('admin_content')
  @section('class_social_link','active')
  @section('page_name','All Social Link')
  <?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">All Social Link</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <table class="table table-togglable table-hover">
        <thead>
        <tr>
            <th data-toggle="true">Serial No</th>
            <th data-hide="phone">Social Link Logo Class</th>
            <th data-hide="phone">Social Link Name</th>
            <th data-hide="phone">Social Link</th>
            <th data-hide="phone" data-ignore="true">Status</th>
            <th data-hide="phone,tablet" class="text-center">Action</th>
        </tr>
        </thead>
        <?php $sl=1; ?>
        @foreach($all_social_link_info as $v_social_link)
        <tbody>
        <tr>
            <td>{{ $sl++ }}</td>
            <td><a href="#">{{ $v_social_link->social_logo }}</a></td>
            <td><a href="#">{{ $v_social_link->social_name }}</a></td>
            <td><a href="#">{{ $v_social_link->social_link }}</a></td>
            <td>
                @if($v_social_link->social_status =='active')
                <span class="label label-success">
                    Active
                </span>
                    @else
                    <span class="label label-danger">
                        Inactive
                    </span>
                @endif
            </td>
            <td class="text-center">
                @if($v_social_link->social_status=='active')
                    <a class="btn btn-warning" href="{{URL::to('/inactive-social-link/'.$v_social_link->social_id)}}">
                        <i class=" icon-thumbs-down3"></i>
                    </a>
                @else
                    <a class="btn btn-success" href="{{URL::to('/active-social-link/'.$v_social_link->social_id)}}">
                        <i class=" icon-thumbs-up3"></i>
                    </a>
                @endif
                <a class="btn btn-info" href="{{URL::to('/edit-social-link/'.$v_social_link->social_id)}}">
                    <i class="icon-pencil"></i>
                </a>
                <a class="btn btn-danger" href="{{URL::to('/delete-social-link/'.$v_social_link->social_id)}}" onclick="return confirm('Are You Sure To Delete This Social Link !!!');" id="delete">
                    <i class="icon-trash"></i>
                </a>

            </td>
        </tr>
        </tbody>
            @endforeach
    </table>
</div>
{{ $all_social_link_info->links() }}
<br><br>
@endif
<!-- /column names -->
@endsection