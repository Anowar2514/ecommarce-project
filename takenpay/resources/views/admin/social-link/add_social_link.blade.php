@extends('admin_layout')
@section('title','Add Social Link')
@section('admin_content')
@section('class_social_link','active')
@section('page_name','Add Social Link')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
<div class="container">
    <div class="row">
        <div class="col-md-4  col-md-offset-3">
            <form action="{{url('/save-social-link')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Add Social Link</h6>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label>Social Link Name</label>
                            <input type="text" name="social_name" class="form-control" placeholder="Add Social Link Name" required>
                        </div>
                        <div class="form-group">
                            <label>Social Logo Class</label>
                            <input type="text" name="social_logo" class="form-control" placeholder="Add Social Logo Class" required>
                        </div>
                        <div class="form-group">
                            <label>Social Link</label>
                            <input type="text" name="social_link" class="form-control" placeholder="Add Social Link" required>
                        </div>
                        <div class="form-group">
                            <label>Social Link Status</label>
                            <select class="form-control" name="social_status">
                                <option>---------- Select Status ----------</option>
                                <option value="active">Published</option>
                                <option value="inactive">Unpublished</option>
                            </select>
                        </div>
                        <div>
                            <button type="submit" class="btn bg-teal-400">Submit Social Link</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
<!-- /form actions -->
@endsection