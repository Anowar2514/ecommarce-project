@extends('useradmin_layout')
@section('title','Dashboard')
@section('admin_content')
@section('class_dash','active')
@section('page_name','Dashboard')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
    <!-- Main charts -->
    <div class="row">
        <div class="col-lg-7">
            <h3><b style="color: green">Welcome to dashboard ' <span style="color: blue">{{Session::get('admin_name')}}</span> '</b></h3>
        </div>
    </div>
    <!-- /main charts -->
    @endif
    @endsection