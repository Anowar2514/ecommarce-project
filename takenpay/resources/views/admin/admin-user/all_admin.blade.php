@extends('admin_layout')
@section('title','All Admin')
@section('admin_content')
  @section('class_admin','active')
  @section('page_name','All Admin')
  <?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">All Admins</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <table class="table table-togglable table-hover">
        <thead>
        <tr>
            <th data-toggle="true">Admin ID</th>
            <th data-hide="phone">Admin Name</th>
            <th data-hide="phone">Admin Email</th>
            <th data-hide="phone">Admin Phone</th>
            <th data-hide="phone">Admin Password</th>
            <th data-hide="phone" data-ignore="true">Status</th>
            <th data-hide="phone,tablet" class="text-center">Action</th>
        </tr>
        </thead>
        <?php $sl=1; ?>
        @foreach($all_admin_info as $v_admin)
        <tbody>
        <tr>
            <td>{{ $sl++ }}</td>
            <td><a href="#">{{ $v_admin->admin_name }}</a></td>
            <td><a href="#">{{ $v_admin->admin_email }}</a></td>
            <td><a href="#">{{ $v_admin->admin_phone }}</a></td>
            <td><a href="#">{{ $v_admin->admin_password }}</a></td>
            <td>
                @if($v_admin->publication_status ==1)
                    <span class="label label-success">
                    Active
                </span>
                @else
                    <span class="label label-danger">
                        Block
                    </span>
                @endif
            </td>
            <td class="text-center">
                @if($v_admin->publication_status ==1)
                    <a class="btn btn-warning" href="{{URL::to('/block-admin/'.$v_admin->useradmin_id)}}">
                        <i class=" icon-thumbs-down3"></i>
                    </a>
                @else
                    <a class="btn btn-success" href="{{URL::to('/active-admin/'.$v_admin->useradmin_id)}}">
                        <i class=" icon-thumbs-up3"></i>
                    </a>
            @endif
                <a class="btn btn-danger" href="{{URL::to('/delete-admin/'.$v_admin->useradmin_id)}}" onclick="return confirm('Are You Sure To Delete This Admin !!!');" id="delete">
                    <i class="icon-trash"></i>
                </a>

            </td>
        </tr>
        </tbody>
            @endforeach
    </table>
</div>
{{ $all_admin_info->links() }}
<br><br>
<!-- /column names -->
@endif
@endsection