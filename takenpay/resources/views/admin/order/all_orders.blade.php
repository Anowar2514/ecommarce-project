@extends('admin_layout')
@section('title','All Pending Orders')
@section('admin_content')
@section('class_order','active')
@section('page_name','All Pending Orders')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">All Pending Orders</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <table class="table table-togglable table-hover">
        <thead>
        <tr>
            <th data-toggle="true">Order ID</th>
            {{--<th data-hide="phone">Order Image</th>--}}
             <th data-hide="phone">Customer Name</th>
            <th data-hide="phone">Total Order</th>
            <th data-hide="phone" data-ignore="true">Status</th>
            <th data-hide="phone,tablet" class="text-center">Action</th>
        </tr>
        </thead>
        <?php $sl=1; ?>
        @foreach($all_order_info as $v_order)
            <tbody>
            <tr>
                <td>{{ $sl++ }}</td>
                {{--<td><a href="#"><img src="{{ URL::to($v_order->product_image) }}" height="120px" width="100px"></a></td>--}}
                <td><a href="#">{{ $v_order->customer_name }}</a></td>
                <td><a href="#">{{ $v_order->order_total }}</a></td>
                {{--<td><a href="#">{{ $v_order->order_status }}</a></td>--}}
                <td>
                    @if($v_order->order_status =='pending')
                        <span class="label label-success">
                    pending
                </span>
                    @else
                        <span class="label label-danger">
                        completed
                    </span>
                    @endif
                </td>
                <td class="text-center">
                    @if($v_order->order_status=='pending')
                        <a class="btn btn-warning" href="{{URL::to('/inactive-order/'.$v_order->order_id)}}">
                            <i class=" icon-thumbs-down3"></i>
                        </a>
                    @else
                        <a class="btn btn-success" href="{{URL::to('/active-order/'.$v_order->order_id)}}">
                            <i class=" icon-thumbs-up3"></i>
                        </a>
                    @endif
                    <a class="btn btn-info" href="{{URL::to('/view-order/'.$v_order->order_id)}}">
                        <i class="icon-eye"></i>
                    </a>
                    <a class="btn btn-danger" href="{{URL::to('/delete-order/'.$v_order->order_id)}}" onclick="return confirm('Is the Order Complete Successfully !!!');" id="delete">
                        <i class="icon-trash"></i>
                    </a>

                </td>
            </tr>
            </tbody>
        @endforeach
    </table>
</div>
{{ $all_order_info->links() }}
<br><br>
@endif
@endsection