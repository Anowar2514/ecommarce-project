@extends('admin_layout')
@section('title','Order View')
@section('admin_content')
@section('class_order','active')
@section('page_name','Order View')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
<div class="content">

    <!-- Page length options -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Customer Details</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>
        <table class="table datatable-show-all">
            <thead>
            <tr>
                <th>Customer Name</th>
                <th>Mobile</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                @foreach($order_view_by_id as $v_order)
                @endforeach
                <td><a href="#">{{$v_order->customer_name}}</a></td>
                <td>{{$v_order->mobile_number}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <!-- /page length options -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Shipping Details</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>
        <table class="table datatable-show-all">
            <thead>
            <tr>
                <th>First Name</th>
                <th>Last Name</th>
                <th>Email Address</th>
                <th>Address</th>
                <th>City</th>
                <th>Mobile</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                @foreach($order_view_by_id as $v_order)
                @endforeach
                <td><a href="#">{{$v_order->shipping_first_name}}</a></td>
                <td><a href="#">{{$v_order->shipping_last_name}}</a></td>
                <td>{{$v_order->shipping_email}}</td>
                <td>{{$v_order->shipping_address}}</td>
                <td>{{$v_order->shipping_city}}</td>
                <td>{{$v_order->shipping_mobile_number}}</td>
            </tr>
            </tbody>
        </table>
    </div>

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Order Details</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>
        <table class="table datatable-dom-position">
            <thead>
            <tr>
                <th>Order Id</th>
                <th>Product Image</th>
                <th>Product Name</th>
                <th>Payment Method</th>
                <th>Product Sales Quantity</th>
                <th>Product Price</th>
                <th>Price Sub Total </th>
            </tr>
            </thead>
            <tbody>
            @foreach($order_view_by_id as $v_order)
            <tr>
                <td>{{$v_order->order_id}}</td>
                <td><img src="{{ URL::to($v_order->product_image) }}" height="120px" width="100px"></td>
                <td><a href="#">{{$v_order->product_name}}</a></td>
                <td>{{$v_order->payment_method}}</td>
                <td>{{$v_order->product_sales_quantity}}</td>
                <td>{{$v_order->product_price}}</td>
                <td><span class="label label-success"><?php $a = $v_order->product_price; $b = $v_order->product_sales_quantity; $total=$a*$b; echo $total;?></span></td>
            </tr>
            @endforeach
            </tbody>
            <tfoot>
            <tr>
                <td colspan="5"><b><a href="#">Total Balance with Tex:</a></b></td>
                <td><strong> = {{$v_order->order_total}} BDT</strong></td>
            </tr>
            </tfoot>
        </table>
    </div>
    <a href="{{URL::to('/manage-order')}}" class="btn btn-warning pull-right">Back , All Order Page</a>
</div>
@endif
@endsection