@extends('admin_layout')
@section('title','Message View')
@section('admin_content')
@section('class_message','active')
@section('page_name','Message View')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
<div class="content">

    <!-- Page length options -->
    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Customer Details</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>
        <table class="table datatable-show-all">
            <thead>
            <tr>
                <th>Customer Name</th>
                <th>Customer Email</th>
            </tr>
            </thead>
            <tbody>
            <tr>
                @foreach($view_messages_info as $v_message)
                @endforeach
                <td><a href="#">{{$v_message->customer_name}}</a></td>
                <td>{{$v_message->customer_email}}</td>
            </tr>
            </tbody>
        </table>
    </div>
    <!-- /page length options -->

    <div class="panel panel-flat">
        <div class="panel-heading">
            <h5 class="panel-title">Message</h5>
            <div class="heading-elements">
                <ul class="icons-list">
                    <li><a data-action="collapse"></a></li>
                    <li><a data-action="reload"></a></li>
                    <li><a data-action="close"></a></li>
                </ul>
            </div>
        </div>
        <table class="table datatable-dom-position">
            <thead>
            <tr>
                <th>Subject & Message</th>
            </tr>
            </thead>
            <tbody>
            @foreach($view_messages_info as $v_message)
                <tr>
                    <td><a href="#">{{$v_message->message_subject}}</a></td>
                </tr>
                <tr>
                    <td>{{$v_message->customer_message}}</td>
                </tr>
            @endforeach
            </tbody>
        </table>
    </div>
    {{--<a href="{{ URL::to('/write-message/'.$v_message->message_id) }}" class="btn btn-success">Write , Message</a>--}}
</div>
<div class="container">
    <div class="row">
        <div class="col-md-4  col-md-offset-3">
            <form action="{{url('/send-email')}}" method="post">
                {{ csrf_field() }}
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Write Message</h6>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label>To</label>

                            <input type="email" name="to" class="form-control" placeholder="To" required>
                        </div>
                        <div class="form-group">
                            <label>Subject</label>
                            <input type="text" name="subject" class="form-control" placeholder="Subject" required>
                        </div>

                        <div class="form-group">
                            <label>Message</label>
                            <textarea rows="5" cols="5" class="form-control" name="message" placeholder="Message" required></textarea>
                        </div>
                        <div>
                            <button type="submit" class="btn bg-teal-400">Send</button>
                            <a href="{{URL::to('/all-messages')}}" class="btn btn-warning">Back</a>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
@endsection