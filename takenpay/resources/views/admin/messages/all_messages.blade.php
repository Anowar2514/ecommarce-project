@extends('admin_layout')
@section('title','Customer Messages')
@section('admin_content')
@section('class_message','active')
@section('page_name','Customer Messages')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">All Customer Messages</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <table class="table table-togglable table-hover">
        <thead>
        <tr>
            <th data-toggle="true">Serial No</th>
            <th data-hide="phone">Customer Name</th>
            <th data-hide="phone">Email</th>
            <th data-hide="phone">Subject</th>
            <th data-hide="phone">Message</th>
            <th data-hide="phone" data-ignore="true">Status</th>
            <th data-hide="phone,tablet" class="text-center">Action</th>
        </tr>
        </thead>
        <?php $sl=1; ?>
        @foreach($all_messages_info as $v_message)
            <tbody>
            <tr>
                <td>{{ $sl++ }}</td>
                <td><a href="#">{{ $v_message->customer_name }}</a></td>
                <td><a href="#">{{ $v_message->customer_email }}</a></td>
                <td><a href="#">{{ $v_message->message_subject }}</a></td>
                <td><a href="#">{{ str_limit($v_message->customer_message,30) }}</a></td>
                <td>
                    @if($v_message->publication_status ==1)
                        <span class="label label-success">
                    Pendding
                </span>
                    @else
                        <span class="label label-danger">
                        Reviewed
                    </span>
                    @endif
                </td>
                <td class="text-center">
                    @if($v_message->publication_status==1)
                        <a class="btn btn-warning" href="{{URL::to('/inactive-message/'.$v_message->message_id)}}">
                            <i class=" icon-thumbs-down3"></i>
                        </a>
                    @else
                        <a class="btn btn-info" href="{{URL::to('/active-message/'.$v_message->message_id)}}">
                            <i class=" icon-thumbs-up3"></i>
                        </a>
                    @endif
                        <a class="btn btn-success" href="{{URL::to('/view-message/'.$v_message->message_id)}}">
                            <i class="icon-earth"></i>
                        </a>
                    <a class="btn btn-danger" href="{{URL::to('/delete-message/'.$v_message->message_id)}}" onclick="return confirm('Are You Sure To Delete This Message !!!');" id="delete">
                        <i class="icon-trash"></i>
                    </a>

                </td>
            </tr>
            </tbody>
        @endforeach
    </table>
</div>
{{ $all_messages_info->links() }}
<br><br>
<!-- /column names -->
@endif
@endsection
