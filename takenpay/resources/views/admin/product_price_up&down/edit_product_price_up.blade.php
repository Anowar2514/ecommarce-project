@extends('admin_layout')
@section('title','Edit Product Price Up')
@section('admin_content')
@section('class_price','active')
@section('page_name','Edit Product Price Up')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
<div class="container">
    <div class="row">
        <div class="col-md-4  col-md-offset-3">
            <form action="{{url('/update-price/'.$product_info->price_up_id)}}" method="post">
                {{ csrf_field() }}
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Edit Product</h6>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label>Category Name</label>
                            <input type="text" name="category_id" class="form-control" value="{{ $product_info->category_id }}">
                        </div>
                        <div class="form-group">
                            <label>Brand Name</label>
                            <input type="text" name="brand_id" class="form-control" value="{{ $product_info->brand_id }}">
                        </div>
                        {{--<div class="form-group">--}}
                            {{--<label>Product Name</label>--}}
                            {{--<input type="text" name="product_name" class="form-control" value="{{ $product_info->product_name }}">--}}
                        {{--</div>--}}

                        <div class="form-group">
                            <label>Product Short Description</label>
                            <textarea rows="5" cols="5" class="form-control" name="product_short_description">{{$product_info->product_short_description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Product Long Description</label>
                            <textarea rows="5" cols="5" class="form-control" name="product_long_description">{{$product_info->product_long_description}}</textarea>
                        </div>
                        <div class="form-group">
                            <label>Product Price</label>
                            <input type="number" name="product_price" class="form-control" value="{{ $product_info->product_price }}">
                        </div>
                        {{--<div class="form-group">--}}
                            {{--<label>Product Image</label>--}}
                            {{--<input type="file" name="product_name" class="file-input" value="{{ $product_info->product_image }}">--}}
                        {{--</div>--}}
                        <div class="form-group">
                            <label>Product Size</label>
                            <input type="text" name="product_size" class="form-control" value="{{ $product_info->product_size }}">
                        </div>
                        <div class="form-group">
                            <label>Product Color</label>
                            <input type="text" name="product_color" class="form-control" value="{{ $product_info->product_color }}">
                        </div>
                        <div>
                            <button type="submit" class="btn bg-teal-400">Update Product</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
<!-- /form actions -->
@endsection