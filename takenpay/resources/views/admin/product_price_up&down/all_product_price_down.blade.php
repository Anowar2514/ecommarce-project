@extends('admin_layout')
@section('title','All Product Down Price')
@section('admin_content')
  @section('class_price','active')
  @section('page_name','All Product Down Price')
  <?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">All Down Price Products</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <table class="table table-togglable table-hover">
        <thead>
        <tr>
            <th data-toggle="true">Product ID</th>
            <th data-hide="phone">Product Image</th>
            <th data-hide="phone">Product Name</th>
            <th data-hide="phone">Product Description</th>
            <th data-hide="phone">Product Price</th>
            <th data-hide="phone">Product Size</th>
            <th data-hide="phone">Product Category</th>
            <th data-hide="phone">Product Brand</th>
            <th data-hide="phone" data-ignore="true">Status</th>
            <th data-hide="phone,tablet" class="text-center">Action</th>
        </tr>
        </thead>
        <?php $sl=1; ?>
        @foreach($all_product_info as $v_product)
        <tbody>
        <tr>
            <td>{{ $sl++ }}</td>
            <td><a href="#"><img src="{{ URL::to($v_product->product_image) }}" height="120px" width="100px"></a></td>
            <td><a href="#">{{ $v_product->product_name }}</a></td>
            <td><a href="#">{{ $v_product->product_short_description }}</a></td>
            <td><a href="#">{{ $v_product->product_price }}</a></td>
            <td><a href="#">{{ $v_product->product_size }}</a></td>
            <td><a href="#">{{ $v_product->category_name }}</a></td>
            <td><a href="#">{{ $v_product->brand_name }}</a></td>
            <td>
                @if($v_product->publication_status ==1)
                <span class="label label-info">
                    Active
                </span>
                    @else
                    <span class="label label-danger">
                        Inactive
                    </span>
                @endif
            </td>
            <td class="text-center">
                @if($v_product->publication_status==1)
                    <a class="btn btn-warning" href="{{URL::to('/inactive-price-down/'.$v_product->price_down_id)}}">
                        <i class=" icon-thumbs-down3"></i>
                    </a>
                @else
                    <a class="btn btn-success" href="{{URL::to('/active-price-down/'.$v_product->price_down_id)}}">
                        <i class=" icon-thumbs-up3"></i>
                    </a>
                @endif
                <a class="btn btn-info" href="{{URL::to('/edit-price-down/'.$v_product->price_down_id)}}">
                    <i class="icon-pencil"></i>
                </a>
                <a class="btn btn-danger" href="{{URL::to('/delete-price-down/'.$v_product->price_down_id)}}" onclick="return confirm('Are You Sure To Delete This Product !!!');" id="delete">
                    <i class="icon-trash"></i>
                </a>

            </td>
        </tr>
        </tbody>
            @endforeach
    </table>
</div>
{{ $all_product_info->links() }}
<br><br>
@endif
<!-- /column names -->
@endsection