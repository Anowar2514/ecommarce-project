@extends('admin_layout')
@section('title','Edit Category')
@section('admin_content')
@section('class_cat','active')
@section('page_name','Edit Category')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
<div class="container">
    <div class="row">
        <div class="col-md-4  col-md-offset-3">
            <form action="{{url('/update-category',$category_info->category_id)}}" method="post">
                {{ csrf_field() }}
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Edit Category</h6>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label>Category Name</label>
                            <input type="text" name="category_name" class="form-control" value="{{ $category_info->category_name }}">
                        </div>

                        <div class="form-group">
                            <label>Category Description</label>
                            <textarea rows="5" cols="5" class="form-control" name="category_description">{{$category_info->category_description}}</textarea>
                        </div>
                        <div>
                            <button type="submit" class="btn bg-teal-400">Update Category</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
<!-- /form actions -->
@endsection