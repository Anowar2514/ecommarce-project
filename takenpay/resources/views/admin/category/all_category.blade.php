@extends('admin_layout')
@section('title','All Category')
@section('admin_content')
  @section('class_cat','active')
  @section('page_name','All Category')
  <?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">All Categories</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <table class="table table-togglable table-hover">
        <thead>
        <tr>
            <th data-toggle="true">Category ID</th>
            <th data-hide="phone">Category Name</th>
            <th data-hide="phone">Category Description</th>
            <th data-hide="phone" data-ignore="true">Status</th>
            <th data-hide="phone,tablet" class="text-center">Action</th>
        </tr>
        </thead>
        <?php $sl = 1;?>
        @foreach($all_category_info as $v_category)
        <tbody>
        <tr>
            <td>{{ $sl++ }}</td>
            <td><a href="#">{{ $v_category->category_name }}</a></td>
            <td><a href="#">{{ $v_category->category_description }}</a></td>
            <td>
                @if($v_category->publication_status ==1)
                <span class="label label-success">
                    Active
                </span>
                    @else
                    <span class="label label-danger">
                        Inactive
                    </span>
                @endif
            </td>
            <td class="text-center">
                @if($v_category->publication_status ==1)
                <a class="btn btn-warning" href="{{URL::to('/inactive_category/'.$v_category->category_id)}}">
                    <i class=" icon-thumbs-down3"></i>
                </a>
                 @else
                    <a class="btn btn-success" href="{{URL::to('/active_category/'.$v_category->category_id)}}">
                        <i class=" icon-thumbs-up3"></i>
                    </a>
                    @endif
                <a class="btn btn-info" href="{{URL::to('/edit-category/'.$v_category->category_id)}}">
                    <i class="icon-pencil"></i>
                </a>
                <a class="btn btn-danger" href="{{URL::to('/delete-category/'.$v_category->category_id)}}" onclick="return confirm('Are You Sure To Delete This Category !!!');" id="delete">
                    <i class="icon-trash"></i>
                </a>
            </td>
        </tr>
        </tbody>
            @endforeach
    </table>
</div>
{{ $all_category_info->links() }}
<br><br>
<!-- /column names -->
@endif
@endsection