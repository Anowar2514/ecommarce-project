@extends('admin_layout')
@section('title','Add Slider')
@section('admin_content')
@section('class_slider','active')
@section('page_name','Add Slider')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
<div class="container">
    <div class="row">
        <div class="col-md-4  col-md-offset-3">
            <form action="{{url('/save-slider')}}" method="post" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Add Slider</h6>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label>Slider Name</label>
                            <input type="text" name="slider_name" class="form-control" placeholder="Add Slider Name" required>
                        </div>
                        <div class="form-group">
                            <label>Slider Price</label>
                            <input type="number" name="slider_price" class="form-control" placeholder="Add Slider Price" required>
                        </div>
                        <div class="form-group">
                            <label>Slider Image</label>
                            <input type="file" name="slider_image" class="file-input" required>
                        </div>
                        <div class="form-group">
                            <label>Publication Status</label>
                            <select class="form-control" name="publication_status">
                                <option>---------- Select Status ----------</option>
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                        <div>
                            <button type="submit" class="btn bg-teal-400">Submit Slider</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
<!-- /form actions -->
@endsection