@extends('admin_layout')
@section('title','Edit Slider')
@section('admin_content')
@section('class_slider','active')
@section('page_name','Edit Slider')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
<div class="container">
    <div class="row">
        <div class="col-md-4  col-md-offset-3">
            <form action="{{url('/update-slider',$slider_info->slider_id)}}" method="post">
                {{ csrf_field() }}
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Edit Slider</h6>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label>Slider Name</label>
                            <input type="text" name="slider_name" class="form-control" value="{{ $slider_info->slider_name }}">
                        </div>
                        <div class="form-group">
                            <label>Slider Image</label>
                            <input type="file" name="slider_name" class="file-input" value="{{ $slider_info->slider_image }}">
                        </div>
                        <div>
                            <button type="submit" class="btn bg-teal-400">Update Slider</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
<!-- /form actions -->
@endsection