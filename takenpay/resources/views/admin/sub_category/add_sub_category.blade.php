@extends('admin_layout')
@section('title','Add Sub Category')
@section('admin_content')
@section('class_sub_cat','active')
@section('page_name','Add Sub Category')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
<div class="container">
    <div class="row">
        <div class="col-md-4  col-md-offset-3">
            <form action="{{url('/save-sub-category')}}" method="post">
                {{ csrf_field() }}
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Add Sub Category</h6>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label>Sub Category Name</label>
                            <input type="text" name="sub_category_name" class="form-control" placeholder="Add Sub Category Name" required>
                        </div>
                        <div class="form-group">
                            <label>Category Name</label>
                            <select class="form-control" name="category_id">
                                <option>---------- Select Status ----------</option>
                                <?php
                                $all_published_category = DB::table('categories')
                                    ->where('publication_status',1)
                                    ->get();
                                foreach($all_published_category as $v_category){
                                ?>
                                <option value="{{ $v_category->category_id }}">{{ $v_category->category_name }}</option>
                                <?php }?>
                            </select>
                        </div>

                        <div class="form-group">
                            <label>Sub Category Description</label>
                            <textarea rows="5" cols="5" class="form-control" name="sub_category_description" placeholder="Enter sub category description here" required></textarea>
                        </div>
                        <div class="form-group">
                            <label>Publication Status</label>
                            <select class="form-control" name="publication_status">
                                <option>---------- Select Status ----------</option>
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                        <div>
                            <button type="submit" class="btn bg-teal-400">Submit Sub Category</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
    <!-- /form actions -->
@endsection