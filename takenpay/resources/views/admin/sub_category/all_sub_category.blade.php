@extends('admin_layout')
@section('title','All Sub Category')
@section('admin_content')
@section('class_sub_cat','active')
@section('page_name','All Sub Category')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)

<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">All Sub Category</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <table class="table table-togglable table-hover">
        <thead>
        <tr>
            <th data-toggle="true">Serial No</th>
            <th data-hide="phone">Sub Category Name</th>
            <th data-hide="phone">Category Name</th>
            <th data-hide="phone">Description</th>
            {{--<th data-hide="phone" data-ignore="true">Status</th>--}}
            <th data-hide="phone,tablet" class="text-center">Action</th>
        </tr>
        </thead>
        <?php $sl=1; ?>
        @foreach($all_sub_category_info as $v_subcategory)
            <tbody>
            <tr>
                <td>{{ $sl++ }}</td>
                <td><a href="#">{{ $v_subcategory->sub_category_name }}</a></td>
                <td><a href="#">{{ $v_subcategory->category_name }}</a></td>
                <td><a href="#">{{ $v_subcategory->sub_category_description }}</a></td>
                {{--<td>--}}
                    {{--@if($v_subcategory->publication_status ==1)--}}
                        {{--<span class="label label-success">--}}
                    {{--Active--}}
                {{--</span>--}}
                    {{--@else--}}
                        {{--<span class="label label-danger">--}}
                        {{--Inactive--}}
                    {{--</span>--}}
                    {{--@endif--}}
                {{--</td>--}}
                <td class="text-center">
                    {{--@if($v_subcategory->publication_status==1)--}}
                        {{--<a class="btn btn-warning" href="{{URL::to('/inactive-sub-category/'.$v_subcategory->sub_category_id)}}">--}}
                            {{--<i class=" icon-thumbs-down3"></i>--}}
                        {{--</a>--}}
                    {{--@else--}}
                        {{--<a class="btn btn-success" href="{{URL::to('/active-sub-category/'.$v_subcategory->sub_category_id)}}">--}}
                            {{--<i class=" icon-thumbs-up3"></i>--}}
                        {{--</a>--}}
                    {{--@endif--}}
                    <a class="btn btn-info" href="{{URL::to('/edit-sub-category/'.$v_subcategory->sub_category_id)}}">
                        <i class="icon-pencil"></i>
                    </a>
                    <a class="btn btn-danger" href="{{URL::to('/delete-sub-category/'.$v_subcategory->sub_category_id)}}" onclick="return confirm('Are You Sure To Delete This Category !!!');" id="delete">
                        <i class="icon-trash"></i>
                    </a>

                </td>
            </tr>
            </tbody>
        @endforeach
    </table>
</div>
{{ $all_sub_category_info->links() }}
<br><br>
@endif
<!-- /column names -->
@endsection