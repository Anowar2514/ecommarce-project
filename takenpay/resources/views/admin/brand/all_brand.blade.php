@extends('admin_layout')
@section('title','All Brand')
@section('admin_content')
  @section('class_brand','active')
  @section('page_name','All Brand')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
<div class="panel panel-flat">
    <div class="panel-heading">
        <h5 class="panel-title">All Brands</h5>
        <div class="heading-elements">
            <ul class="icons-list">
                <li><a data-action="collapse"></a></li>
                <li><a data-action="reload"></a></li>
                <li><a data-action="close"></a></li>
            </ul>
        </div>
    </div>

    <table class="table table-togglable table-hover">
        <thead>
        <tr>
            <th data-toggle="true">Brand ID</th>
            <th data-hide="phone">Brand Name</th>
            <th data-hide="phone">Brand Description</th>
            <th data-hide="phone" data-ignore="true">Status</th>
            <th data-hide="phone,tablet" class="text-center">Action</th>
        </tr>
        </thead>
        <?php $sl = 1;?>
        @foreach($all_brand_info as $v_brand)
        <tbody>
        <tr>
            <td>{{ $sl++ }}</td>
            <td><a href="#">{{ $v_brand->brand_name }}</a></td>
            <td><a href="#">{{ $v_brand->brand_description }}</a></td>
            <td>
                @if($v_brand->publication_status ==1)
                <span class="label label-success">
                    Active
                </span>
                    @else
                    <span class="label label-danger">
                        Inactive
                    </span>
                @endif
            </td>
            <td class="text-center">
                @if($v_brand->publication_status ==1)
                <a class="btn btn-warning" href="{{URL::to('/inactive_brand/'.$v_brand->brand_id)}}">
                    <i class=" icon-thumbs-down3"></i>
                </a>
                 @else
                    <a class="btn btn-success" href="{{URL::to('/active_brand/'.$v_brand->brand_id)}}">
                        <i class=" icon-thumbs-up3"></i>
                    </a>
                    @endif
                <a class="btn btn-info" href="{{URL::to('/edit-brand/'.$v_brand->brand_id)}}">
                    <i class="icon-pencil"></i>
                </a>
                <a class="btn btn-danger" href="{{URL::to('/delete-brand/'.$v_brand->brand_id)}}" onclick="return confirm('Are You Sure To Delete This Brand !!!');" id="delete">
                    <i class="icon-trash"></i>
                </a>

            </td>
        </tr>
        </tbody>
            @endforeach
    </table>
</div>
{{ $all_brand_info->links() }}
<br><br>
<!-- /column names -->
@endif
@endsection