@extends('admin_layout')
@section('title','Edit Brand')
@section('admin_content')
@section('class_brand','active')
@section('page_name','Edit Brand')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
<div class="container">
    <div class="row">
        <div class="col-md-4  col-md-offset-3">
            <form action="{{url('/update-brand',$brand_info->brand_id)}}" method="post">
                {{ csrf_field() }}
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Edit Brand</h6>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label>Brand Name</label>
                            <input type="text" name="brand_name" class="form-control" value="{{ $brand_info->brand_name }}">
                        </div>

                        <div class="form-group">
                            <label>Brand Description</label>
                            <textarea rows="5" cols="5" class="form-control" name="brand_description">{{$brand_info->brand_description}}</textarea>
                        </div>
                        <div>
                            <button type="submit" class="btn bg-teal-400">Update Brand</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endif
<!-- /form actions -->
@endsection