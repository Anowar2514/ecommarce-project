@extends('admin_layout')
@section('title','Add Brand')
@section('admin_content')
@section('class_brand','active')
@section('page_name','Add Brand')
<?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
<div class="container">
    <div class="row">
        <div class="col-md-4  col-md-offset-3">
            <form action="{{url('/save-brand')}}" method="post">
                {{ csrf_field() }}
                <div class="panel panel-flat">
                    <div class="panel-heading">
                        <h6 class="panel-title">Add Brand</h6>
                        <div class="heading-elements">
                            <ul class="icons-list">
                                <li><a data-action="collapse"></a></li>
                                <li><a data-action="reload"></a></li>
                                <li><a data-action="close"></a></li>
                            </ul>
                        </div>
                    </div>

                    <div class="panel-body">
                        <div class="form-group">
                            <label>Brand Name</label>
                            <input type="text" name="brand_name" class="form-control" placeholder="Add Brand Name" required>
                        </div>

                        <div class="form-group">
                            <label>Brand Description</label>
                            <textarea rows="5" cols="5" class="form-control" name="brand_description" placeholder="Enter brand description here" required></textarea>
                        </div>
                        <div class="form-group">
                            <label>Publication Status</label>
                            <select class="form-control" name="publication_status">
                                <option>---------- Select Status ----------</option>
                                <option value="1">Published</option>
                                <option value="0">Unpublished</option>
                            </select>
                        </div>
                        <div>
                            <button type="submit" class="btn bg-teal-400">Submit Brand</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
    <!-- /form actions -->
    @endif
@endsection