@extends('layout')
@section('title','Products Sub Category')
@section('content')
    <h2 class="title text-center">Sub Category Wise Products</h2>
    <?php
    foreach ($product_by_sub_category as $v_product_category){
    ?>
    <div class="col-sm-4">
        <div class="product-image-wrapper">
            <div class="single-products">
                <div class="productinfo text-center">
                    <img src="{{ URL::to($v_product_category->product_image)}}" height="280px" width="60px" alt="" />
                    <h2>BDT-{{ $v_product_category->product_price }}</h2>
                    <a href="{{URL::to('/view_product/'.$v_product_category->product_id)}}"><p>{{ $v_product_category->product_name }}</p></a>
                    <a href="{{URL::to('/view_product/'.$v_product_category->product_id)}}" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Buy Now</a><br><br>
                </div>
                <div class="product-overlay">
                    <div class="overlay-content">
                        <h2>BDT-{{$v_product_category->product_price }}</h2>
                        <a href="{{URL::to('/view_product/'.$v_product_category->product_id)}}"><p>{{ $v_product_category->product_name }}</p></a>
                        <a href="{{URL::to('/view_product/'.$v_product_category->product_id)}}" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Buy Now</a><br><br>
                    </div>
                </div>
            </div>
            <div class="choose">
                <ul class="nav nav-pills nav-justified">
                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                    <li><a href="{{URL::to('/view_product/'.$v_product_category->product_id)}}"><i class="fa fa-plus-square"></i>View Product</a></li>
                </ul>
            </div>
        </div>
    </div>
    <?php }
    ?>
    </div><!--features_items-->
@endsection