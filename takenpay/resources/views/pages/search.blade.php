@extends('layout')
@section('title','Search Result')
@section('content')
                <h1>Search Result</h1>
                <h4>{{ $products->total() }} result(s) for ' <b style="color: green">{{ request()->input('search') }}</b> '</h4>
                <br><br><hr>
             @foreach($products as $product)
                    <div class="col-sm-4">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    <img src="{{ URL::to($product->product_image)}}" height="280px" width="60px" alt="" />
                                    <h2>BDT-{{ $product->product_price }}</h2>
                                    <a href="{{URL::to('/view_product/'.$product->product_id)}}"><p>{{ $product->product_name }}</p></a>
                                    <a href="{{URL::to('/view_product/'.$product->product_id)}}" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Buy Now</a><br><br>
                                </div>
                                <div class="product-overlay">
                                    <div class="overlay-content">
                                        <h2>BDT-{{ $product->product_price }}</h2>
                                        <a href="{{URL::to('/view_product/'.$product->product_id)}}"><p>{{ $product->product_name }}</p></a>
                                        <a href="{{URL::to('/view_product/'.$product->product_id)}}" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Buy Now</a><br><br>
                                    </div>
                                </div>
                            </div>
                            <div class="choose">
                                <ul class="nav nav-pills nav-justified">
                                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                                    <li><a href="{{URL::to('/view_product/'.$product->product_id)}}"><i class="fa fa-plus-square"></i>View Product</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                 <div class="spacer"></div>
                 {{ $products->appends(request()->input())->links() }}
                 @endforeach
@endsection