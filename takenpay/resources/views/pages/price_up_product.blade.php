@extends('layout')
@section('title','Price Up')
@section('content')
    <hr>
    <h2 style="color:orange;"><b style="text-align: center;">Price Up Products</b></h2>
    <hr>
    <br><br>
    <?php
    $products = DB::table('price_ups')
        ->where('publication_status',1)
        ->get();
    foreach ($products->chunk(3) as $chunk){
        ?>
        <div class="row">
            <?php foreach ($chunk as $v_price){
                ?>
                <div class="col-xs-4">
                    <div class="productinfo text-center">
                        <img src="{{URL::to($v_price->product_image)}}" height="280px" width="60px" alt="" />
                        <h2>BDT-{{$v_price->product_price}}</h2>
                        <p>{{$v_price->product_name}}</p>
                        <a href="{{URL::to('/view_price/'.$v_price->price_up_id)}}" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Buy Now</a><br><br>
                    </div>
                </div>
            <?php }?>
        </div>
    <?php }?>
    @endsection