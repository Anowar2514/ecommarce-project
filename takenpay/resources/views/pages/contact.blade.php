@extends('layout')
@section('title','Contact')
@section('content')
    <div id="contact-page" class="container">
        <div class="bg">
            <div class="row">
                <div class="col-sm-8">
                    <h2 class="title text-center">Contact <strong>Us</strong></h2>
                    <br><br>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6">
                    <div class="contact-form">
                        <h2 class="title text-center">Send Message</h2>
                        <div class="status alert alert-success" style="display: none"></div>
                        <form id="main-contact-form" class="contact-form row" name="contact-form" action="{{url('/send-message')}}"  method="post">
                            {{ csrf_field() }}
                            <div class="form-group col-md-6">
                                <input type="text" name="customer_name" class="form-control" required="required" placeholder="Name">
                                <input type="hidden" name="publication_status" value="1">
                            </div>
                            <div class="form-group col-md-6">
                                <input type="email" name="customer_email" class="form-control" required="required" placeholder="Email">
                            </div>
                            <div class="form-group col-md-12">
                                <input type="text" name="message_subject" class="form-control" required="required" placeholder="Subject">
                            </div>
                            <div class="form-group col-md-12">
                                <textarea name="customer_message" id="message" required="required" class="form-control" rows="8" placeholder="Your Message Here"></textarea>
                            </div>
                            <div class="form-group col-md-12">
                                <input type="submit" name="send_email" class="btn btn-primary pull-right" value="Send Your Message">
                            </div>
                        </form>
                    </div>
                </div>
                <div class="col-sm-4">
                    <div class="contact-info">
                        <h2 class="title">Contact Info</h2>
                        <address>
                            <p style="color: orange">Take<span style="color: blue"> N</span> Pay</p>
                            <p>Bosila,Mohammadpur</p>
                            <p>Dhaka-1207,Bangladesh</p>
                            <p>Mobile: +8801612150312</p>
                            <p>Email: takenpay24@gmail.com</p>
                        </address>
                        <div class="social-networks">
                            <h4 style="color: orange"><b>SOCIAL NETWORK</b></h4>
                            <ul>
                                <?php
                                $all_published_social_link = DB::table('tbl_sociallinks')
                                    ->where('social_status','active')
                                    ->limit(4)
                                    ->get();
                                foreach($all_published_social_link as $v_social_link){
                                    ?>
                                <li>
                                    <a href="{{URL::to($v_social_link->social_link)}}"><i class="{{$v_social_link->social_logo}}"></i></a>
                                </li>
                                    <?php }?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div><!--/#contact-page-->
@endsection