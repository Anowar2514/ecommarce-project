@extends('layout')
@section('title','Price Down')
@section('content')
    <hr>
    <h2 style="color:orange;"><b style="text-align: center;">Price Down Products</b></h2>
    <hr>
    <br><br>
    <?php
    $products = DB::table('price_downs')
        ->where('publication_status',1)
        ->get();
    foreach ($products->chunk(3) as $chunk){
        ?>
        <div class="row">
            <?php foreach ($chunk as $v_down_price){
                ?>
                <div class="col-xs-4">
                    <div class="productinfo text-center">
                        <img src="{{URL::to($v_down_price->product_image)}}" height="280px" width="60px" alt="" />
                        <h2>BDT-{{$v_down_price->product_price}}</h2>
                        <p>{{$v_down_price->product_name}}</p>
                        <a href="{{URL::to('/view-down/'.$v_down_price->price_down_id)}}" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Buy Now</a><br><br>
                    </div>
                </div>
            <?php }?>
        </div>
    <?php }?>
    @endsection