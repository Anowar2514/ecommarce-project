@extends('layout')
@section('title','Price Range Result')
@section('content')
                <h1>Search Result</h1>
                <h4>{{ $all_published_product->total() }} result(s) for ' <b style="color: green">{{ request()->input('price_range') }}</b> '</h4>
                <br><br><hr>
             @foreach($all_published_product as $product)
                    <div class="col-sm-4">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    <img src="{{ URL::to($product->product_image)}}" height="280px" width="60px" alt="" />
                                    <h2>BDT-{{ $product->product_price }}</h2>
                                    <a href="{{URL::to('/view_product/'.$product->product_id)}}"><p>{{ $product->product_name }}</p></a>
                                    <a href="{{URL::to('/view_product/'.$product->product_id)}}" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Buy Now</a><br><br>
                                </div>
                                <div class="product-overlay">
                                    <div class="overlay-content">
                                        <h2>BDT-{{ $product->product_price }}</h2>
                                        <a href="{{URL::to('/view_product/'.$product->product_id)}}"><p>{{ $product->product_name }}</p></a>
                                        <a href="{{URL::to('/view_product/'.$product->product_id)}}" class="btn btn-success "><i class="fa fa-shopping-cart"></i> Buy Now</a><br><br>
                                    </div>
                                </div>
                            </div>
                            <div class="choose">
                                <ul class="nav nav-pills nav-justified">
                                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                                    <li><a href="{{URL::to('/view_product/'.$product->product_id)}}"><i class="fa fa-plus-square"></i>View Product</a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                 <div class="spacer"></div>
                 @endforeach
                {{ $all_published_product->appends(request()->input())->links() }}
@endsection