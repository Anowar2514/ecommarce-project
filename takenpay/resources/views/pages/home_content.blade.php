@extends('layout')
@section('title','Home')
@include('slider')
@section('content')
    <h2 class="title text-center"> All Products</h2>
    <?php
    foreach ($all_published_product as $v_product){
    ?>
    <div class="col-sm-4">
        <div class="product-image-wrapper">
            <div class="single-products">
                <div class="productinfo text-center">
                    <img src="{{ URL::to($v_product->product_image)}}" height="280px" width="60px" alt="" />
                    <h2>BDT-{{ $v_product->product_price }}</h2>
                    <a href="{{URL::to('/view_product/'.$v_product->product_id)}}"><p>{{ $v_product->product_name }}</p></a>
                    <a href="{{URL::to('/view_product/'.$v_product->product_id)}}" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Buy Now</a><br><br>
                </div>
                <div class="product-overlay">
                    <div class="overlay-content">
                        <h2>BDT-{{ $v_product->product_price }}</h2>
                        <a href="{{URL::to('/view_product/'.$v_product->product_id)}}"><p>{{ $v_product->product_name }}</p></a>
                        <a href="{{URL::to('/view_product/'.$v_product->product_id)}}" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Buy Now</a><br><br>
                    </div>
                </div>
            </div>
            <div class="choose">
                <ul class="nav nav-pills nav-justified">
                    <li><a href="#"><i class="fa fa-plus-square"></i>Add to wishlist</a></li>
                    <li><a href="{{URL::to('/view_product/'.$v_product->product_id)}}"><i class="fa fa-plus-square"></i>View Product</a></li>
                </ul>
            </div>
        </div>
    </div>
    <?php }
    ?>
    </div>
    {{ $all_published_product->links() }}
    <!--features_items-->
    <hr>

    <div class="recommended_items"><!--price up items-->
        <h2 class="title text-center">Price Down Product</h2>

        <div id="recommended-item-carousel" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                <?php
                $all_published_price_down = DB::table('price_downs')
                    ->where('publication_status',1)
                    ->get();
                $i=1;
                foreach ($all_published_price_down as $v_price){
                if ($i==1){
                ?>
                <div class="item active">
                    <?php }else{?>
                        <div class="item">
                            <?php }?>
                    <div class="col-sm-4">
                        <div class="product-image-wrapper">
                            <div class="single-products">
                                <div class="productinfo text-center">
                                    <img src="{{URL::to($v_price->product_image)}}" height="280px" width="60px" alt="" />
                                    <h2>BDT-{{$v_price->product_price}}</h2>
                                    <p>{{$v_price->product_name}}</p>
                                    <a href="{{URL::to('/view-down/'.$v_price->price_down_id)}}" class="btn btn-success"><i class="fa fa-shopping-cart"></i> Buy Now</a><br><br>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <?php $i++; } ?>
            </div>
            </div>
            <a class="left recommended-item-control" href="#recommended-item-carousel" data-slide="prev">
                <i class="fa fa-angle-left"></i>
            </a>
            <a class="right recommended-item-control" href="#recommended-item-carousel" data-slide="next">
                <i class="fa fa-angle-right"></i>
            </a>
        </div>
    </div><!--/price up items-->
    <hr>
@endsection