@extends('layout')
@section('title','Add To Cart')
@section('content')
    <section id="cart_items">
        <div class="container col-sm-12">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="#">Home</a></li>
                    <li class="active">Shopping Cart</li>
                </ol>
            </div>
            <div class="table-responsive cart_info">
                <?php
                    $contents = Cart::content();
                    //dd($contents);
                ?>
                <table class="table table-condensed">
                    <thead>
                    <tr class="cart_menu">
                        <td class="image">Image</td>
                        <td class="description">Name</td>
                        <td class="price">Price</td>
                        <td class="quantity">Quantity</td>
                        <td class="total">Total</td>
                        <td>Action</td>
                    </tr>
                    </thead>
                    <tbody>
                   <?php
                   foreach($contents as $v_contents)
                   {
                   ?>
                    <tr>
                        <td class="cart_product">
                            <a href=""><img src="{{URL::to($v_contents->options->image)}}" height="80px" width="60px" alt="" style="alignment: center"></a>
                        </td>
                        <td class="cart_description">
                            <h4 style="text-align: center"><a href="">{{$v_contents->name}}</a></h4>
                            <p style="text-align: center"><a href="">Product ID: {{$v_contents->id}}</a></p>
                        </td>
                        <td class="cart_price">
                            <p>BDT-{{$v_contents->price}}</p>
                        </td>
                        <td class="cart_quantity">
                            <div class="cart_quantity_button">
                                <form action="{{url('/update-cart')}}" method="post">
                                    {{ csrf_field() }}
                                <input class="cart_quantity_input" type="number" name="qty" value="{{$v_contents->qty}}" autocomplete="off" size="2">
                                <input type="hidden" name="rowId" value="{{$v_contents->rowId}}">
                                <input type="submit" name="update" value="update" class="btn btn-sm btn-info">
                                </form>
                            </div>
                        </td>
                        <td class="cart_total">
                            <h4>BDT-{{$v_contents->total}}</h4>
                        </td>
                        <td class="cart_delete">
                            <a class="cart_quantity_delete" href="{{URL::to('/delete-to-cart/'.$v_contents->rowId)}}"><i class="fa fa-times"></i></a>
                        </td>
                    </tr>
                    <?php }?>
                    </tbody>
                </table>
            </div>
        </div>
    </section> <!--/#cart_items-->

    <section id="do_action">
        <div class="container">
            <div class="heading">
                <h3>What would you like to do next?</h3>
                <p>Choose if you have a discount code or reward points you want to use or would like to estimate your delivery cost.</p>
            </div>
            <div class="row">
                <div class="col-sm-8">
                    <div class="total_area">
                        <ul>
                            <li>Cart Sub Total <span>BDT-{{Cart::subtotal()}}</span></li>
                            <li>Eco Tax <span>BDT-{{Cart::tax()}}</span></li>
                            <li>Shipping Cost <span>Free</span></li>
                            <li>Total <span>BDT-{{Cart::total()}}</span></li>
                        </ul>
                        <?php
                        $customer_id = Session::get('customer_id');
                        ?>
                        @if($customer_id != NULL)
                        <a class="btn btn-default update" href="{{URL::to('/checkout')}}">Check Out</a>
                        @elseif($customer_id != NULL && $shipping_id !=NULL)
                        <a class="btn btn-default update" href="{{URL::to('/payment')}}">Check Out</a>
                        @else
                        <a class="btn btn-default update" href="{{URL::to('/login-check')}}">Check Out</a>
                            @endif
                    </div>
                </div>
            </div>
        </div>
    </section><!--/#do_action-->
    @endsection