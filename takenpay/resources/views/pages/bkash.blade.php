@extends('layout')
@section('title','Bkash Payment')
@section('content')
    <section id="form">
        <div class="container">
            <div class="row">
                <div class="col-sm-3 col-sm-offset-1">
                    <div class="login-form">
                        <h1>Thanks for Order........</h1>
                        <h3>Send Money By Bkash Here: +8801612150312</h3>
                    </div>
                </div>
                <div class="col-sm-1">
                    <h2 class="or">OR</h2>
                </div>
                <div class="col-sm-4">
                    <div class="signup-form">
                        <h2><b>Contact Me By This Number:</b><br> <a href="#" onclick="return confirm('Call Here For Order: +8801612150312');">+8801612150312</a></h2>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection