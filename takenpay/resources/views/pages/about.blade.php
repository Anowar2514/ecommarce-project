@extends('layout')
@section('title','About')
@section('content')
    <section>
        <div class="container">
            <div class="row">
                <div class="col-sm-8">
                    <div class="blog-post-area">
                        <h2 class="title text-center"><span style="color: green">About The <b style="color: orange">Take<span style="color: blue"> N</span> Pay</b></span></h2>
                        <div class="single-blog-post">
                            <h3 style="color: green">We Have a large collection of modern dresses</h3>
                            <hr>
                            <div class="post-meta">
                                <span>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star"></i>
									<i class="fa fa-star-half-o"></i>
								</span>
                            </div>
                            <p style="color: green"><b>
                                    Welcome to<span style="color: orange"> Take <span style="color: blue">N</span> Pay</span> !
                                </b></p>
                            <p>
                                We aim to offer our customers a variety of the latest all types of products. We’ve come a long way,
                            </p>
                            so we know exactly which direction to take when supplying you with high quality yet budget friendly products.
                            <p>
                            We offer all of this while providing excellent customer service and friendly support.
                            </p>
                            <p>
                            We always keep an eye on the latest trends in all types of product and put our customers’ wishes first.
                            </p>
                            <p>
                            That is why we have satisfied customers all over the world, and are thrilled to be a part of the all types of products industry.
                            </p>
                            <p>
                                The interests of our customers are always the top priority for us, so we hope you will enjoy our products as much as we enjoy
                                making them available to you.
                            </p>
                        </div>
                    </div><!--/blog-post-area-->
                </div>
            </div>
        </div>
    </section>
@endsection