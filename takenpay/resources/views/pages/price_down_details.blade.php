@extends('layout')
@section('title','Price Down Details')
@section('content')
<div class="col-sm-9 padding-right">
    <div class="product-details"><!--product-details-->
        <div class="col-sm-5">
            <div class="view-product">
                <img src="{{URL::to($price_down_by_details->product_image)}}" alt="" />
                <h3>ZOOM</h3>
            </div>
        </div>
        <div class="col-sm-7">
            <div class="product-information"><!--/product-information-->
                <img src="{{URL::to('front_end/images/product-details/new.jpg')}}" class="newarrival" alt="" />
                <h2>{{$price_down_by_details->product_name}}</h2>
                <p>Product Color: {{$price_down_by_details->product_color}}</p>
                <img src="{{URL::to('front_end/images/product-details/rating.png')}}" alt="" />
                <span>
									<span>BDT-{{$price_down_by_details->product_price}}</span>
                                <form action="{{url('/price-down-add-to-cart')}}" method="post">
                                    {{ csrf_field()}}
									<label>Quantity:</label>
									<input name="qty" type="number" value="1"/>
									<input name="price_down_id" type="hidden" value="{{$price_down_by_details->price_down_id}}" />
									<button type="submit" class="btn btn-fefault cart">
										<i class="fa fa-shopping-cart"></i>
										Buy Now
									</button>
                                </form>
								</span>
                <p><b>Availability:</b> In Stock</p>
                <p><b>Condition:</b> New</p>
                <p><b>Brand:</b>{{$price_down_by_details->brand_name}}</p>
                <p><b>Category:</b>{{$price_down_by_details->category_name}}</p>
                <p><b>Size:</b>{{$price_down_by_details->product_size}}</p>
                <p><b>In Stock:</b>{{$price_down_by_details->product_quantity}} Items</p>
                <a href=""><img src="{{URL::to('front_end/images/product-details/share.png')}}" class="share img-responsive"  alt="" /></a>
            </div><!--/product-information-->
        </div>
    </div><!--/product-details-->

    <div class="category-tab shop-details-tab"><!--category-tab-->
        <div class="col-sm-12">
            <ul class="nav nav-tabs">
                <li><a href="#details" data-toggle="tab">Details</a></li>
                <li><a href="#companyprofile" data-toggle="tab">Company Profile</a></li>
                <li><a href="#tag" data-toggle="tab">Tag</a></li>
                <li class="active"><a href="#reviews" data-toggle="tab">Reviews (5)</a></li>
            </ul>
        </div>
        <div class="tab-content">
            <div class="tab-pane fade" id="details" >
                    {{$price_down_by_details->product_long_description}}
            </div>
            <div class="tab-pane fade active in" id="reviews" >
                <div class="col-sm-12">
                    <ul>
                        <li><a href=""><i class="fa fa-user"></i>Md.Anowar Hossain</a></li>
                        <li><a href=""><i class="fa fa-clock-o"></i>12:41 AM</a></li>
                        <li><a href=""><i class="fa fa-calendar-o"></i>1 JUNE 2018</a></li>
                    </ul>
                    <p>{{$price_down_by_details->product_long_description}}</p>

                    <form action="#">
										<span>
											<input type="text" placeholder="Your Name"/>
											<input type="email" placeholder="Email Address"/>
										</span>
                        <textarea name="" ></textarea>
                        <b>Rating: </b> <img src="{{URL::to('front_end/images/product-details/rating.png')}}" alt="" />
                        <button type="button" class="btn btn-default pull-right">
                            Submit
                        </button>
                    </form>
                </div>
            </div>

        </div>
    </div><!--/category-tab-->
</div>
    @endsection
