<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Take N Pay | @yield('title')</title>

    <!-- Global stylesheets -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,300,100,500,700,900" rel="stylesheet" type="text/css">
    <link href="{{asset('back_end/css/icons/icomoon/styles.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('back_end/css/minified/bootstrap.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('back_end/css/minified/core.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('back_end/css/minified/components.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('back_end/css/minified/colors.min.css')}}" rel="stylesheet" type="text/css">
    <!-- /global stylesheets -->

    <!-- Core JS files -->
    <script type="text/javascript" src="{{asset('back_end/js/plugins/loaders/pace.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('back_end/js/core/libraries/jquery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('back_end/js/core/libraries/bootstrap.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('back_end/js/plugins/loaders/blockui.min.js')}}"></script>
    <!-- /core JS files -->

    <!-- Theme JS files -->
    <script type="text/javascript" src="{{asset('back_end/js/plugins/visualization/d3/d3.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('back_end/js/plugins/visualization/d3/d3_tooltip.js')}}"></script>
    <script type="text/javascript" src="{{asset('back_end/js/plugins/forms/styling/switchery.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('back_end/js/plugins/forms/styling/uniform.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('back_end/js/plugins/forms/selects/bootstrap_multiselect.js')}}"></script>
    <script type="text/javascript" src="{{asset('back_end/js/plugins/ui/moment/moment.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('back_end/js/plugins/pickers/daterangepicker.js')}}"></script>

    <script type="text/javascript" src="{{asset('back_end/js/core/app.js')}}"></script>
    <script type="text/javascript" src="{{asset('back_end/js/pages/dashboard.js')}}"></script>
    <!-- /theme JS files -->

</head>

<body>
    <?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
<h5 style="color: #17a2b8"><b>Take N Pay</b></h5>
<!-- Main navbar -->
<div class="navbar navbar-inverse">
    <div class="navbar-collapse collapse" id="navbar-mobile">
        <ul class="nav navbar-nav">
            <li><a class="sidebar-control sidebar-main-toggle hidden-xs"><i class="icon-paragraph-justify3"></i></a></li>
            <li class="dropdown">
                <div class="dropdown-menu dropdown-content">
                    <div class="dropdown-content-heading">
                        <ul class="icons-list">
                            <li><a href="#"><i class="icon-sync"></i></a></li>
                        </ul>
                    </div>
                    <div class="dropdown-content-footer">
                        <a href="#" data-popup="tooltip" title="All activity"><i class="icon-menu display-block"></i></a>
                    </div>
                </div>
            </li>
        </ul>
        <?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
        <ul class="nav navbar-nav navbar-right">
            <li class="dropdown dropdown-user">
                <a class="dropdown-toggle" data-toggle="dropdown">
                    <img src="assets/images/placeholder.jpg" alt="">
                    <span>{{Session::get('admin_name')}}</span>
                    <i class="caret"></i>
                </a>

                <ul class="dropdown-menu dropdown-menu-right">
                    <li><a href="#"><i class="icon-user-plus"></i> My profile</a></li>
                    <li><a href="#"><i class="icon-coins"></i> My balance</a></li>
                    <li class="divider"></li>
                    <li><a href="{{URL::to('/logout')}}"><i class="icon-switch2"></i> Logout</a></li>
                </ul>
            </li>
        </ul>
            @endif
    </div>
</div>
<!-- /main navbar -->


<!-- Page container -->
<div class="page-container">

    <!-- Page content -->
    <div class="page-content">

        <!-- Main sidebar -->
        <div class="sidebar sidebar-main">
            <div class="sidebar-content">

                <!-- User menu -->
                  <div class="sidebar-user">
                    <div class="category-content">
                        <div class="media">
                            <a href="#" class="media-left"><img src="assets/images/placeholder.jpg" class="img-circle img-sm" alt=""></a>
                            <?php
                            $admin_id = Session::get('admin_id');
                            $useradmin_id = Session::get('useradmin_id');
                            ?>
                            @if($admin_id !=NULL || $useradmin_id !=NULL)
                            <div class="media-body">
                                <span class="media-heading text-semibold">{{Session::get('admin_name')}}</span>
                                <div class="text-size-mini text-muted">
                                    <i class="icon-pin text-size-small"></i> &nbsp;Dhaka, Bangladesh
                                </div>
                            </div>
                            @endif

                            <div class="media-right media-middle">
                                <ul class="icons-list">
                                    <li>
                                        <a href="#"><i class="icon-cog3"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                  </div>
                <hr>
                <!-- /user menu -->
                <!-- Main navigation -->
                <div class="sidebar-category sidebar-category-visible">
                    <div class="category-content no-padding">
                        <ul class="navigation navigation-main navigation-accordion">
                            <?php
                            $admin_id = Session::get('admin_id');
                            $useradmin_id = Session::get('useradmin_id');
                            ?>
                            @if($admin_id !=NULL || $useradmin_id !=NULL)
                            <li class="@yield('class_dash')"><a href="{{URL::to('/dashboard')}}"><i class="icon-home4"></i> <span>Dashboard</span></a></li>
                            @endif
                            <?php
                            $admin_id = Session::get('admin_id');
                            $useradmin_id = Session::get('useradmin_id');
                            ?>
                            @if($admin_id !=NULL || $useradmin_id !=NULL)
                            <li class="@yield('class_cat')" >
                                <a href="#"><i class="icon-stack2"></i> <span>Category</span></a>
                                <ul>
                                    <li><a href="{{URL::to('/all-category')}}">All Categories</a></li>
                                    <li><a href="{{URL::to('/add-category')}}">Add Category</a></li>
                                </ul>
                            </li>
                            @endif
                            <?php
                            $admin_id = Session::get('admin_id');
                            $useradmin_id = Session::get('useradmin_id');
                            ?>
                            @if($admin_id !=NULL || $useradmin_id !=NULL)
                            <li class="@yield('class_sub_cat')" >
                                <a href="#"><i class="icon-file-check"></i> <span>Sub Category</span></a>
                                <ul>
                                    <li><a href="{{URL::to('/all-sub-category')}}">All Sub Categories</a></li>
                                    <li><a href="{{URL::to('/add-sub-category')}}">Add Sub Category</a></li>
                                </ul>
                            </li>
                            @endif
                            <?php
                            $admin_id = Session::get('admin_id');
                            $useradmin_id = Session::get('useradmin_id');
                            ?>
                            @if($admin_id !=NULL || $useradmin_id !=NULL)
                            <li class="@yield('class_brand')">
                                <a href="#"><i class="icon-copy"></i> <span>Brands</span></a>
                                <ul>
                                    <li><a href="{{URL::to('/all-brand')}}" id="layout1">All Brands <span class="label bg-warning-400">Current</span></a></li>
                                    <li><a href="{{URL::to('/add-brand')}}" id="layout2">Add Brand</a></li>
                                </ul>
                            </li>
                            @endif
                            <?php
                            $admin_id = Session::get('admin_id');
                            $useradmin_id = Session::get('useradmin_id');
                            ?>
                            @if($admin_id !=NULL || $useradmin_id !=NULL)
                            <li class="@yield('class_product')">
                                <a href="#"><i class="icon-gift"></i> <span>Products</span></a>
                                <ul>
                                    <li><a href="{{URL::to('/all-product')}}">All Products</a></li>
                                    <li><a href="{{URL::to('/add-product')}}">Add Product<span class="label bg-warning-400">New</span></a></li>
                                </ul>
                            </li>
                            @endif
                            <?php
                            $admin_id = Session::get('admin_id');
                            $useradmin_id = Session::get('useradmin_id');
                            ?>
                            @if($admin_id !=NULL || $useradmin_id !=NULL)
                            <li class="@yield('class_price')">
                                <a href="#"><i class="icon-cash4"></i> <span>Product Price Up's & Down </span></a>
                                <ul>
                                    <li><a href="{{URL::to('/all-price-up')}}">All Price Up Products</a></li>
                                    <li><a href="{{URL::to('/add-price-up')}}">Add Price Up Products</a></li>
                                    <hr>
                                    <li><a href="{{URL::to('/all-price-down')}}">All Price Down Products</a></li>
                                    <li><a href="{{URL::to('/add-price-down')}}">Add Price Down Products</a></li>
                                </ul>
                            </li>
                            @endif
                            <?php
                            $admin_id = Session::get('admin_id');
                            $useradmin_id = Session::get('useradmin_id');
                            ?>
                            @if($admin_id !=NULL || $useradmin_id !=NULL)
                            <li class="@yield('class_slider')">
                                <a href="#"><i class="icon-images2"></i> <span>Slider</span></a>
                                <ul>
                                    <li><a href="{{URL::to('/all-slider')}}">All Sliders</a></li>
                                    <li><a href="{{URL::to('/add-slider')}}">Add Slider<span class="label bg-warning-400">Front Images</span></a></li>
                                </ul>
                            </li>
                            @endif
                            <?php
                            $admin_id = Session::get('admin_id');
                            $useradmin_id = Session::get('useradmin_id');
                            ?>
                            @if($admin_id !=NULL || $useradmin_id !=NULL)
                            <li class="@yield('class_social')">
                                <a href="#"><i class="icon-facebook2"></i> <span>Social Link</span></a>
                                <ul>
                                    <li><a href="{{URL::to('/all-social-link')}}">All Social Link</a></li>
                                    <li><a href="{{URL::to('/add-social-link')}}">Add Social Link</a></li>
                                </ul>
                            </li>
                            @endif
                            <?php
                            $admin_id = Session::get('admin_id');
                            $useradmin_id = Session::get('useradmin_id');
                            ?>
                            @if($admin_id !=NULL || $useradmin_id !=NULL)
                            <li class="@yield('class_order')">
                                <a href="#"><i class="icon-list-ordered"></i> <span>Order</span></a>
                                <ul>
                                    <li><a href="{{URL::to('/manage-order')}}" id="layout1">All Orders <span class="label bg-warning-400">Current</span></a></li>
                                </ul>
                            </li>
                            @endif
                            <?php
                            $admin_id = Session::get('admin_id');
                            $useradmin_id = Session::get('useradmin_id');
                            ?>
                            @if($admin_id !=NULL || $useradmin_id !=NULL)
                            <li class="@yield('class_message')">
                                <a href="#"><i class="icon-inbox"></i> <span>Customer Messages</span></a>
                                <ul>
                                    <li><a href="{{URL::to('/all-messages')}}">Customer All Messages</a></li>
                                </ul>
                            </li>
                            @endif
                            <?php
                            $admin_id = Session::get('admin_id');
                            $useradmin_id = Session::get('useradmin_id');
                            ?>
                            @if($admin_id !=NULL || $useradmin_id !=NULL)
                            <li class="@yield('class_delivery')">
                                <a href="#"><i class="icon-user-plus"></i> <span>Delivery Man</span></a>
                                <ul>
                                    <li><a href="{{URL::to('/all-delivery-man')}}">All Delivery Man</a></li>
                                    <li><a href="{{URL::to('/add-delivery-man')}}">Add Delivery Man</a></li>
                                </ul>
                            </li>
                            @endif
                            <?php
                            $admin_id = Session::get('admin_id');
                            ?>
                            @if($admin_id !=NULL)
                            <li class="@yield('class_admin')">
                                <a href="#"><i class="icon-users"></i> <span>Admins</span></a>
                                <ul>
                                    <li><a href="{{URL::to('/all-admins')}}">All Admins</a></li>
                                    <li><a href="{{URL::to('/add-admins')}}">Add Admins</a></li>
                                </ul>
                            </li>
                                @endif
                                <?php
                                $admin_id = Session::get('admin_id');
                                $useradmin_id = Session::get('useradmin_id');
                                ?>
                                @if($admin_id !=NULL || $useradmin_id !=NULL)
                                    <li class="@yield('class_dash')"><a href="http://mlmsoftware.takenpay.org/" target="_blank"><i class="icon-home4"></i> <span>MlmSoftware</span></a></li>
                                @endif
                        </ul>
                    </div>
                </div>
                <!-- /main navigation -->

            </div>
        </div>
        <!-- /main sidebar -->


        <!-- Main content -->

        <div class="content-wrapper">
        <?php
        $admin_id = Session::get('admin_id');
        $useradmin_id = Session::get('useradmin_id');
        ?>
        @if($admin_id !=NULL || $useradmin_id !=NULL)
            <!-- Page header -->
            <div class="page-header">
                <div class="breadcrumb-line">
                    <ul class="breadcrumb">
                        <li><a href="{{URL::to('/dashboard')}}"><i class="icon-home2 position-left"></i> Home</a></li>
                        <li class="active">@yield('page_name')</li>
                    </ul>
                </div>
            </div>
            @endif
            <!-- /page header -->


            <!-- Content area -->
            <div class="content">
                @yield('admin_content')
                </div>
                <!-- /dashboard content -->


                <!-- Footer -->
                <div class="footer text-muted">
                    &copy; 2018. <a href="#">Take N Pay</a> by <a href="http://www.takenpay.org" target="_blank">Md.Anowar Hossain</a>
                </div>
                <!-- /footer -->

            </div>
            <!-- /content area -->

        </div>
        <!-- /main content -->

    </div>
    <!-- /page content -->

</div>
<!-- /page container -->
@endif
</body>
</html>
