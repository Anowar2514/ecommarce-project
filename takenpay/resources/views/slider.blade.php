@section('slider')
    <section id="slider"><!--slider-->
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <div id="slider-carousel" class="carousel slide" data-ride="carousel">
                        <ol class="carousel-indicators">
                            <li data-target="#slider-carousel" data-slide-to="0" class="active"></li>
                            <li data-target="#slider-carousel" data-slide-to="1"></li>
                            <li data-target="#slider-carousel" data-slide-to="2"></li>
                            <li data-target="#slider-carousel" data-slide-to="3"></li>
                            <li data-target="#slider-carousel" data-slide-to="4"></li>
                            <li data-target="#slider-carousel" data-slide-to="5"></li>
                        </ol>

                        <div class="carousel-inner">
                            <?php
                            $all_published_slider = DB::table('sliders')
                                ->where('publication_status',1)
                                ->get();
                            $i=1;
                            foreach ($all_published_slider as $v_slider){
                            if($i==1){
                            ?>
                            <div class="item active">
                                <?php }else{?>
                                <div class="item">
                                    <?php }?>
                                    <div class="col-sm-4">
                                        <h1 style="color: blue"><img src="{{asset('front_end/images/home/head01.jpg')}}" alt="" hight="60px" width="60px" /><b><span>  Take </span>N<span style="color: orange"> Pay</span></b></h1>
                                        <h2 style="color: green">{{$v_slider->slider_name}}</h2>
                                        {{--<h4 style="color: blue"><b>BDT-{{$v_slider->slider_price}}</b></h4>--}}
                                        <button type="button" class="btn btn-default get">Get it now</button>
                                    </div>
                                    <div class="col-sm-8">
                                        <img src="{{URL::to($v_slider->slider_image)}}" class="girl img-responsive" alt="" style="height: 400px; width: 600px;" />
                                    </div>
                                </div>
                                <?php  $i++; }?>
                            </div>

                            <a href="#slider-carousel" class="left control-carousel hidden-xs" data-slide="prev">
                                <i class="fa fa-angle-left"></i>
                            </a>
                            <a href="#slider-carousel" class="right control-carousel hidden-xs" data-slide="next">
                                <i class="fa fa-angle-right"></i>
                            </a>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section><!--/slider-->
    @endsection